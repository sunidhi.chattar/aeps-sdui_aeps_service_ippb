package com.aeps.aeps_ippb.presentation.ministatement_screen

import android.Manifest
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.RemoteException
import android.util.Log
import android.widget.Toast
import androidx.activity.compose.BackHandler
import androidx.activity.compose.ManagedActivityResultLauncher
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.aeps.aeps_ippb.MainActivity
import com.aeps.aeps_ippb.common.GetPosConnectedPrinter
import com.aeps.aeps_ippb.common.SdkConstants
import com.aeps.aeps_ippb.domain.model.response.TransferResponseDomain
import com.aeps.aeps_ippb.presentation.viewmodel.UnifiedAepsViewModel
import com.aeps.aeps_ippb.ui.theme.msTextColor
import com.aeps.aeps_ippb.ui.theme.txnBkglightblue
import com.aeps.aeps_ippb.ui.theme.txnTextColor
import com.isu.printer_library.vriddhi.AEMPrinter
import wangpos.sdk4.libbasebinder.Printer
import java.io.IOException
import com.aeps.aeps_ippb.R

@Composable
fun MiniStatementScreen(
    viewModel: UnifiedAepsViewModel,
) {
    val printClicked = remember {
        mutableStateOf(false)
    }
    val context = LocalContext.current
    var bluetoothManager: BluetoothManager? = null
    bluetoothManager = context.getSystemService(
        BluetoothManager::class.java
    )
    val bluetoothAdapter: BluetoothAdapter? = bluetoothManager!!.adapter
    var mAemprinter: AEMPrinter?
    val activityResultLauncher =
        rememberLauncherForActivityResult(contract = ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                Toast.makeText(context, "Bluetooth Enabled", Toast.LENGTH_SHORT).show()
            }
        }

    BackHandler {
        (context as Activity).finish()
    }
    val location =
        if (SdkConstants.location != "" || SdkConstants.location.isNotEmpty() && SdkConstants.location != "null") {
            SdkConstants.location
        } else {
            "N/A"
        }
    val agent =
        if (SdkConstants.agent != "" || SdkConstants.agent.isNotEmpty() || SdkConstants.agent != "null") {
            SdkConstants.agent
        } else {
            "N/A"
        }

    val apiResponse = viewModel.apiResponse.value
    apiResponse?.let {
        if (viewModel.isStartPrinting.value) {
            val aepsSdkMainActivity = MainActivity()
            aepsSdkMainActivity.ShowBluetoothDevices(context, viewModel, printClicked) {
                mAemprinter = GetPosConnectedPrinter.aemPrinter
                callBluetoothFunctionForMiniStatement(
                    it.txId ?: "N/A",
                    it.originIdentifier ?: "N/A",
                    mAemprinter,
                    it.balance ?: "N/A",
                    it.ministatement,
                    date = it.createdDate ?: "N/A",
                    authCode = it.authCode ?: "N/A",
                    uidRef = it.uidRefId ?: "N/A",
                    agent = agent,
                    location = location,
                    terminalId = viewModel.terminalId.value ?: "N/A",
                    bankName = it.bankName ?: "N/A",
                    rrn = it.apiTid ?: "N/A",
                    context
                )
                printClicked.value = false

            }
        }
        Column(
            modifier = Modifier
                .fillMaxSize(),
            verticalArrangement = Arrangement.SpaceBetween
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
            ) {
                Text(
                    text = "Transaction ID :",
                    modifier = Modifier.padding(start = 10.dp, top = 2.dp, end = 20.dp),
                    color = msTextColor
                )
                Text(
                    text = it.txId ?: "N/A",
                    modifier = Modifier.padding(start = 11.dp, top = 4.dp, end = 13.dp),
                    color = txnTextColor
                )
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(start = 8.dp, end = 13.dp),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Column {
                        Text(
                            text = "Available Balance",
                            modifier = Modifier.padding(start = 10.dp, top = 2.dp, end = 20.dp),
                            color = msTextColor
                        )
                        Text(
                            text = it.balance ?: "",
                            modifier = Modifier.padding(start = 11.dp, top = 4.dp, end = 13.dp),
                            color = txnTextColor,
                            fontSize = 20.sp
                        )
                    }
                    Column {
                        Text(
                            text = "Aadhaar Number",
                            modifier = Modifier.padding(start = 10.dp, top = 2.dp, end = 20.dp),
                            color = msTextColor
                        )
                        Text(
                            text = it.originIdentifier ?: "",
                            modifier = Modifier.padding(start = 11.dp, top = 4.dp, end = 13.dp),
                            color = txnTextColor,
                            fontSize = 20.sp
                        )
                    }
                }
                Spacer(modifier = Modifier.height(7.dp))
                Text(
                    text = "Statement",
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(color = txnBkglightblue)
                        .padding(7.dp),
                    textAlign = TextAlign.Center,
                    fontSize = 17.sp,
                    color = Color.White
                )
                LazyColumn(verticalArrangement = Arrangement.spacedBy(4.dp)) {
                    it.ministatement?.let { msList ->
                        items(items = msList) { msItem ->
                            msItem?.let {
                                MiniStatementList(
                                    date = it.date ?: "",
                                    remarks = it.txnDesc ?: "",
                                    amount = it.amount ?: "",
                                    creditDebit = it.txntype ?: ""
                                )
                            }
                        }
                    }
                }
            }
            Column(modifier = Modifier.fillMaxWidth()) {
                Button(
                    modifier = Modifier
                        .fillMaxWidth()
                        .defaultMinSize(minHeight = 54.dp)
                        .border(1.dp, txnBkglightblue),
                    onClick = {
                        startPrintFunctionMs(
                            context,
                            viewModel,
                            SdkConstants.permissions12,
                            bluetoothAdapter,
                            activityResultLauncher,
                            printClicked
                        )
                    },
                    colors = ButtonDefaults.buttonColors(
                        backgroundColor = Color.White
                    )
                ) {
                    Text("PRINT", color = Color.Black, fontSize = 20.sp)
                }
                Button(
                    modifier = Modifier
                        .fillMaxWidth()
                        .defaultMinSize(minHeight = 54.dp),
                    onClick = {
                        (context as Activity).finish()
                    },
                    colors = ButtonDefaults.buttonColors(
                        backgroundColor = txnBkglightblue
                    )
                ) {
                    Text("CLOSE", color = Color.White, fontSize = 20.sp)
                }
            }
        }
    }
    if (apiResponse == null) {
        Column(
            modifier = Modifier.fillMaxSize(), verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(text = "No Data Found")
        }
    }
}

private fun callBluetoothFunctionForMiniStatement(
    txId: String,
    originIdentifier: String,
    mAemPrinter: AEMPrinter?,
    balance: String,
    miniStatementsList: List<TransferResponseDomain.MiniStatementDomain?>?,
    date: String,
    authCode: String,
    uidRef: String,
    agent: String,
    location: String,
    terminalId: String,
    bankName: String,
    rrn: String,
    context: Context
) {
    val balanceAmount = if (balance.isNotEmpty() && balance != "N/A") {
        "Rs. $balance"
    } else {
        balance.ifEmpty { "N/A" }
    }
    try {
        mAemPrinter?.let {
            mAemPrinter.POS__FontThreeInchDOUBLEHIEGHT()
            mAemPrinter.POS_FontThreeInchCENTER()
            val d1: Drawable? = ContextCompat.getDrawable(context, R.drawable.ippb_logo)
            val bitDw1 = d1 as BitmapDrawable
            val bmp1 = bitDw1.bitmap
            mAemPrinter.printImage(bmp1)
            mAemPrinter.POS_FontThreeInchCENTER()
            mAemPrinter.POS__FontThreeInchNORMAL()
            mAemPrinter.print("Aadhaar Enabled Payment System")
            mAemPrinter.print("\n")
            mAemPrinter.POS_FontThreeInchCENTER()
            mAemPrinter.print("India Post Payments Bank")
            mAemPrinter.print("\n")
            mAemPrinter.print("\n")
            mAemPrinter.POS_FontThreeInchCENTER()
            mAemPrinter.POS__FontThreeInchDOUBLEHIEGHT()
            mAemPrinter.print("MINI STATEMENT")
            mAemPrinter.print("\n")
            mAemPrinter.print("\n")
            mAemPrinter.POS__FontThreeInchNORMAL()
            mAemPrinter.print("DATE TIME : ${date.ifEmpty { "N/A" }}")
            mAemPrinter.print("\n")
            mAemPrinter.print("AGENT ID: ${SdkConstants.userNameFromCoreApp.ifEmpty { "N/A" }}")
            mAemPrinter.print("\n")
            mAemPrinter.print("AGENT NAME : ${agent.ifEmpty { "N/A" }}")
            mAemPrinter.print("\n")
            mAemPrinter.print("LOCATION : ${location.ifEmpty { "N/A" }}")
            mAemPrinter.print("\n")
            mAemPrinter.print("TERMINAL ID : ${terminalId.ifEmpty { "N/A" }}")
            mAemPrinter.print("\n")
            mAemPrinter.print("CUST ID : ${originIdentifier.ifEmpty { "N/A" }}")
            mAemPrinter.print("\n")
            mAemPrinter.print("CUST BANK : ${bankName.ifEmpty { "N/A" }}")
            mAemPrinter.print("\n")
            mAemPrinter.print("TXN ID : ${txId.ifEmpty { "N/A" }}")
            mAemPrinter.print("\n")
            mAemPrinter.print("RRN : ${rrn.ifEmpty { "N/A" }}")
            mAemPrinter.print("\n")
            mAemPrinter.print("STAN : ${rrn.takeLast(6).ifEmpty { "N/A" }}")
            mAemPrinter.print("\n")
            mAemPrinter.print("UID REF : ${uidRef.ifEmpty { "N/A" }}")
            mAemPrinter.print("\n")
            mAemPrinter.print("AUTHORIZE CODE : ${authCode.ifEmpty { "N/A" }}")
            mAemPrinter.print("\n")
            mAemPrinter.print("TXN STATUS : SUCCESSFUL")
            mAemPrinter.print("\n")
            mAemPrinter.print("\n")
            if (miniStatementsList.isNullOrEmpty()) {
                mAemPrinter.print("No Data Found")
            } else {
                for (i in miniStatementsList.indices) {
                    val mSList = miniStatementsList[i]
                    val msType = when (mSList?.txntype) {
                        "DEBIT", "D" -> "Dr"
                        "CREDIT", "C" -> "Cr"
                        else -> mSList?.txntype
                    }
                    if (mSList?.date == null || mSList.txnDesc == null) {
                        mAemPrinter.print("""           $msType${mSList?.amount} """)
                    } else {
                        mAemPrinter.print(
                            mSList.date.trim() + "  " + msType + mSList.amount + "  " + mSList.txnDesc.trim()
                        )
                    }
                    mAemPrinter.print("\n")
                }
            }
            mAemPrinter.print("Available Balance : $balanceAmount")
            mAemPrinter.print("\n")
            mAemPrinter.print("\n")
            mAemPrinter.POS_FontThreeInchCENTER()
            mAemPrinter.print("***Customer Copy***")
            mAemPrinter.print("\n")
            mAemPrinter.POS_FontThreeInchCENTER()
            mAemPrinter.print("IPPB")
            mAemPrinter.print("\n")
            mAemPrinter.print("\n")
            mAemPrinter.print("\n")
            mAemPrinter.print("\n")
            mAemPrinter.print("\n")
            mAemPrinter.print(" ")
        }
    } catch (e: IOException) {
        e.printStackTrace()
        try {
            GetPosConnectedPrinter.aemPrinter = null
        } catch (exception: Exception) {
            exception.printStackTrace()
        }
    }
}


@Composable
fun MiniStatementList(
    date: String,
    remarks: String,
    amount: String,
    creditDebit: String
) {
    val creditDebitText = when (creditDebit) {
        "DEBIT", "D" -> "Dr"
        "CREDIT", "C" -> "Cr"
        else -> creditDebit
    }
    val creditDebitTextColor = when (creditDebit) {
        "DEBIT", "D" -> Color.Red
        "CREDIT", "C" -> Color.Green
        else -> Color.Black
    }
    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Column(
            modifier = Modifier
                .padding(start = 20.dp)
                .weight(2f),
            horizontalAlignment = Alignment.Start,
            verticalArrangement = Arrangement.Center
        ) {
            Text(text = date, fontSize = 15.sp, color = Color.Black)
            Text(text = remarks, fontSize = 12.sp, color = Color.Black)
        }

        Row(
            modifier = Modifier
                .padding(end = 33.dp)
                .weight(1f),
            horizontalArrangement = Arrangement.End,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(text = amount, fontSize = 15.sp, color = Color.Black)
            Spacer(modifier = Modifier.width(11.dp))
            Text(text = creditDebitText, fontSize = 15.sp, color = creditDebitTextColor)
        }
    }
}

fun startPrintFunctionMs(
    context: Context,
    viewModel: UnifiedAepsViewModel,
    permissions12: Array<String>,
    bluetoothAdapter: BluetoothAdapter?,
    activityResultLauncher: ManagedActivityResultLauncher<Intent, ActivityResult>,
    printClicked: MutableState<Boolean>,
) {
    val deviceModel = Build.MODEL
    if (deviceModel.equals("WPOS-3", ignoreCase = true)) {
        //start printing with wiseasy internal printer
        UnifiedMSPrintReceiptThread(context, viewModel).start()

    } else {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.R) {
            // Device is running Android 10 or below
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.BLUETOOTH)
                != PackageManager.PERMISSION_GRANTED
            ) {
                // Permission is not granted, request it
                ActivityCompat.requestPermissions(
                    context as Activity, permissions12, 1
                )
                printClicked.value = false
                return
            } else {
                if (bluetoothAdapter!!.isEnabled) {
                    viewModel.isStartPrinting.value = true
                } else {
                    GetPosConnectedPrinter.aemPrinter = null
                    val turnOn = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                    activityResultLauncher.launch(turnOn)
                    printClicked.value = false
                    viewModel.isStartPrinting.value = false
                }
            }
        } else {
            if (ActivityCompat.checkSelfPermission(
                    context, Manifest.permission.BLUETOOTH_CONNECT
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    context as Activity, permissions12, 1
                )
                printClicked.value = false
                return
            } else {
                if (bluetoothAdapter!!.isEnabled) {
                    viewModel.isStartPrinting.value = true
                } else {
                    GetPosConnectedPrinter.aemPrinter = null
                    val turnOn = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                    activityResultLauncher.launch(turnOn)
                    printClicked.value = false
                    viewModel.isStartPrinting.value = false
                }
            }
        }
    }
}

//    WPOS-3 PRINT
private class UnifiedMSPrintReceiptThread(
    private val context: Context,
    private val viewModel: UnifiedAepsViewModel
) : Thread() {
    private var mPrinter: Printer? = null
    override fun run() {
        mPrinter = Printer(context)
        mPrinter?.let {
            try {
                it.setPrintType(0) //Printer type 0 means it's an internal printer
            } catch (e: RemoteException) {
                e.printStackTrace()
            }
            checkPrinterStatus(mPrinter = it, context, viewModel)
        }
    }
}

private fun checkPrinterStatus(
    mPrinter: Printer,
    context: Context,
    viewModel: UnifiedAepsViewModel
) {
    try {
        val status = IntArray(1)
        mPrinter.getPrinterStatus(status)
        Log.e("TAG", "Printer Status is " + status[0])
        var msg = ""
        when (status[0]) {
            0x00 -> {
                msg = "Printer status OK"
                Log.e("TAG", "check printer status: $msg")
                startPrinting(mPrinter, context, viewModel)
            }

            0x01 -> {
                msg = "Parameter error"
                showLog(msg, context)
            }

            0x8A -> {
                msg = "Out of Paper"
                showLog(msg, context)
            }

            0x8B -> {
                msg = "Overheat"
                showLog(msg, context)
            }

            else -> {
                msg = "Printer Error"
                showLog(msg, context)
            }
        }
    } catch (e: RemoteException) {
        e.printStackTrace()
    }
}

private fun showLog(msg: String, context: Context) {
    (context as Activity).runOnUiThread(Runnable {
        Toast.makeText(
            context,
            msg,
            Toast.LENGTH_SHORT
        ).show()
    })
    Log.e(
        "TAG",
        "Printer status: $msg"
    )
}

/**
 * First initialize the printer and clear if any catch data is present
 *
 * After initialization of printer then start printing
 */
private fun startPrinting(mPrinter: Printer, context: Context, viewModel: UnifiedAepsViewModel) {
    var result = -1
    try {
        result = mPrinter.printInit()
        Log.e(
            "TAG",
            "startPrinting: Printer init result $result"
        )
        mPrinter.clearPrintDataCache()
        if (result == 0) {
            printReceipt(mPrinter, context, viewModel)
        } else {
            Toast.makeText(context, "Printer initialization failed", Toast.LENGTH_SHORT).show()
        }
    } catch (e: java.lang.Exception) {
        e.printStackTrace()
    }
}

private fun printReceipt(mPrinter: Printer, context: Context, viewModel: UnifiedAepsViewModel) {
    val location =
        if (SdkConstants.location != "" || SdkConstants.location.isNotEmpty() && SdkConstants.location != "null") {
            SdkConstants.location
        } else {
            "N/A"
        }
    val agent =
        if (SdkConstants.agent != "" || SdkConstants.agent.isNotEmpty() || SdkConstants.agent != "null") {
            SdkConstants.agent
        } else {
            "N/A"
        }
    val msList = viewModel.apiResponse.value?.ministatement ?: emptyList()
    var result = -1
    try {
        Log.e(
            "TAG",
            "printReceipt: set density low 3"
        )
        mPrinter.setGrayLevel(3)
        result = mPrinter.printStringExt(
            "IPPB",
            0,
            0f,
            2.0f,
            Printer.Font.SANS_SERIF,
            25,
            Printer.Align.CENTER,
            true,
            false,
            true
        )
        result = mPrinter.printString(
            "Aadhaar Enabled Payment System",
            20,
            Printer.Align.CENTER,
            true,
            false
        )
        result = mPrinter.printString(
            "India Post Payments Bank",
            20,
            Printer.Align.CENTER,
            true,
            false
        )
        result = mPrinter.printString("MINI STATEMENT", 23, Printer.Align.CENTER, true, false)
        result = mPrinter.printString(
            "DATE TIME : ${viewModel.apiResponse.value?.createdDate ?: "N/A".ifEmpty { "N/A" }}",
            20,
            Printer.Align.LEFT,
            false,
            false
        )
        result = mPrinter.printString(
            "AGENT ID : ${SdkConstants.userNameFromCoreApp.ifEmpty { "N/A" }}",
            20,
            Printer.Align.LEFT,
            false,
            false
        )
        result = mPrinter.printString(
            "AGENT NAME: ${agent.ifEmpty { "N/A" }}",
            20,
            Printer.Align.LEFT,
            false,
            false
        )
        result = mPrinter.printString(
            "LOCATION : ${location.ifEmpty { "N/A" }}",
            20,
            Printer.Align.LEFT,
            false,
            false
        )
        result = mPrinter.printString(
            "TERMINAL ID : ${viewModel.terminalId.value ?: "N/A".ifEmpty { "N/A" }}",
            20,
            Printer.Align.LEFT,
            false,
            false
        )
        result = mPrinter.printString(
            "CUST ID : ${viewModel.apiResponse.value?.originIdentifier ?: "N/A".ifEmpty { "N/A" }}",
            20,
            Printer.Align.LEFT,
            false,
            false
        )
        result = mPrinter.printString(
            "CUST BANK : ${viewModel.apiResponse.value?.bankName ?: "N/A".ifEmpty { "N/A" }}",
            20,
            Printer.Align.LEFT,
            false,
            false
        )

        result = mPrinter.printString(
            "TXN Id : ${viewModel.apiResponse.value?.txId ?: "N/A".ifEmpty { "N/A" }}",
            20,
            Printer.Align.LEFT,
            false,
            false
        )

        result = mPrinter.printString(
            "RRN : ${viewModel.apiResponse.value?.apiTid ?: "N/A".ifEmpty { "N/A" }}",
            20,
            Printer.Align.LEFT,
            false,
            false
        )

        result = mPrinter.printString(
            "STAN : ${viewModel.apiResponse.value?.apiTid?.takeLast(6) ?: "N/A".ifEmpty { "N/A" }}",
            20,
            Printer.Align.LEFT,
            false,
            false
        )
        result = mPrinter.printString(
            "UID REF : ${viewModel.apiResponse.value?.uidRefId ?: "N/A".ifEmpty { "N/A" }}",
            20,
            Printer.Align.LEFT,
            false,
            false
        )
        result = mPrinter.printString(
            "AUTHORIZE CODE : ${viewModel.apiResponse.value?.authCode ?: "N/A".ifEmpty { "N/A" }}",
            20,
            Printer.Align.LEFT,
            false,
            false
        )
        result = mPrinter.printString(
            "TXN STATUS : SUCCESSFUL",
            20,
            Printer.Align.LEFT,
            false,
            false
        )
        if (msList.isEmpty()) {
            mPrinter.printString("No Data Found", 20, Printer.Align.LEFT, false, false)
        } else {
            for (i in msList.indices) {
                val mSList = msList[i]
                val msType = when (mSList?.txntype) {
                    "DEBIT", "D" -> "Dr"
                    "CREDIT", "C" -> "Cr"
                    else -> mSList?.txntype
                }
                if (mSList?.date == null || mSList.txnDesc == null) {
                    result = mPrinter.printString(
                        """           $msType${mSList?.amount} """,
                        20,
                        Printer.Align.LEFT,
                        false,
                        false
                    )
                } else {
                    result = mPrinter.printString(
                        mSList.date.trim() + "                              " + msType + mSList.amount,
                        20,
                        Printer.Align.LEFT,
                        false,
                        false
                    )
                    result =
                        mPrinter.printString(
                            mSList.txntype?.trim(),
                            20,
                            Printer.Align.LEFT,
                            false,
                            false
                        )
                }
            }
        }
        result = mPrinter.printString(
            "Balance :${viewModel.apiResponse.value?.balance ?: "N/A".ifEmpty { "N/A" }}",
            20,
            Printer.Align.LEFT,
            false,
            false
        )
        result = mPrinter.printStringExt(
            "***Customer Copy***",
            0,
            0f,
            2.0f,
            Printer.Font.SANS_SERIF,
            20,
            Printer.Align.CENTER,
            true,
            true,
            false
        )
        result = mPrinter.printStringExt(
            "IPPB",
            0,
            0f,
            2.0f,
            Printer.Font.SANS_SERIF,
            20,
            Printer.Align.CENTER,
            true,
            true,
            false
        )
        result = mPrinter.printString(" ", 25, Printer.Align.CENTER, false, false)
        result = mPrinter.printString(" ", 25, Printer.Align.CENTER, false, false)
        Log.e(
            "TAG",
            "printReceipt: print thank you result $result"
        )
        result = mPrinter.printPaper(30)
        Log.e(
            "TAG",
            "printReceipt: print step result $result"
        )
        showPrinterStatus(result, context)
        result = mPrinter.printFinish()
        Log.e(
            "TAG",
            "printReceipt: printer finish result $result"
        )
    } catch (e: java.lang.Exception) {
        e.printStackTrace()
    }
}


private fun showPrinterStatus(result: Int, context: Context) {
    var msg = ""
    when (result) {
        0x00 -> {
            msg = "Print Finish"
            showLog(msg, context)
        }

        0x01 -> {
            msg = "Parameter error"
            showLog(msg, context)
        }

        0x8A -> {
            msg = "Out of Paper"
            showLog(msg, context)
        }

        0x8B -> {
            msg = "Overheat"
            showLog(msg, context)
        }

        else -> {
            msg = "Printer Error"
            showLog(msg, context)
        }
    }
}


@Preview(showBackground = true, showSystemUi = true)
@Composable
fun PreviewMiniStatementItem() {
    MiniStatementList(
        date = " 12/12/12",
        remarks = "CREDIT BY ANANDESWAR SAHU AND RECEIVED",
        amount = "20001",
        creditDebit = "CR"
    )
}
