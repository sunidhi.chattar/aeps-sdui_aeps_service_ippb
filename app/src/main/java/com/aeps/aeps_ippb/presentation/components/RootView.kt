package com.aeps.aeps_ippb.presentation.components

import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.ui.graphics.Color
import com.aeps.aeps_ippb.ui.theme.AEPS_JCTheme
import com.google.accompanist.systemuicontroller.rememberSystemUiController

@Composable
fun RootView(content: @Composable () -> Unit) {
    AEPS_JCTheme(darkTheme = false) {
        val systemUiController = rememberSystemUiController()
        val darkIcons = MaterialTheme.colors.isLight
        SideEffect { systemUiController.setSystemBarsColor(Color.Transparent, darkIcons) }
        Surface { content.invoke() }
    }
}