package com.aeps.aeps_ippb.presentation.navgraph

object Destinations {
    const val TRANSACTION_STATUS_ROUTE="transactionStatusRoute"
    const val MINISTATEMENT_ROUTE="miniStatementRoute"
    const val FINGERPRINT_CAPTURE_ROUTE="fingerPrintCaptureRoute"
    const val BIO_AUTH_SCREEN="bioAuthScreenRoute"
}