package com.aeps.aeps_ippb.presentation.transaction_status

import android.Manifest
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.RemoteException
import android.util.Log
import android.widget.Toast
import androidx.activity.compose.BackHandler
import androidx.activity.compose.ManagedActivityResultLauncher
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.OutlinedButton
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.app.ActivityCompat
import androidx.navigation.NavHostController
import coil.compose.AsyncImagePainter
import com.aeps.aeps_ippb.MainActivity
import com.aeps.aeps_ippb.R
import com.aeps.aeps_ippb.common.GetPosConnectedPrinter
import com.aeps.aeps_ippb.common.SdkConstants
import com.aeps.aeps_ippb.common.createPdf
import com.aeps.aeps_ippb.common.getImagePainter
import com.aeps.aeps_ippb.presentation.viewmodel.UnifiedAepsViewModel
import com.aeps.aeps_ippb.ui.theme.*
import com.isu.printer_library.vriddhi.AEMPrinter
import wangpos.sdk4.libbasebinder.Printer
import java.io.IOException

@Composable
fun TransactionStatusScreen(
    navHostController: NavHostController,
    viewModel: UnifiedAepsViewModel,
) {
    val context = LocalContext.current
    BackHandler(true) {
        (context as Activity).finish()
    }
    var mAemprinter: AEMPrinter?
    var bluetoothManager: BluetoothManager? = null
    bluetoothManager = context.getSystemService(
        BluetoothManager::class.java
    )
    val bluetoothAdapter: BluetoothAdapter? = bluetoothManager?.adapter
    val activityResultLauncher =
        rememberLauncherForActivityResult(contract = ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                Toast.makeText(context, "Bluetooth Enabled", Toast.LENGTH_SHORT).show()
            }
        }

    var transactionStatus: String? by remember {
        mutableStateOf(null)
    }
    var aadhaarNo: String? by remember {
        mutableStateOf(null)
    }
    var createdDate: String? by remember {
        mutableStateOf(null)
    }
    var transactionID: String? by remember {
        mutableStateOf(null)
    }
    var bankName: String? by remember {
        mutableStateOf(null)
    }
    var referenceNo: String? by remember {
        mutableStateOf(null)
    }
    var balanceAmount: String? = ""
    var transactionType: String? by remember {
        mutableStateOf(null)
    }
    var isRetriable: Boolean? by remember {
        mutableStateOf(null)
    }
    var apiComment: String? by remember {
        mutableStateOf(null)
    }
    var statusTxt: String? by remember {
        mutableStateOf(null)
    }
    var bkgColor = redColor

    var balanceText by remember {
        mutableStateOf("")
    }
    var balance = "N/A"
    var cardText by remember {
        mutableStateOf("N/A")
    }
    var shouldShowRetryBtn by remember {
        mutableStateOf(false)
    }
    val printClicked = remember {
        mutableStateOf(false)
    }
    var isClicked by remember {
        mutableStateOf(false)
    }
    var authCode: String? by remember {
        mutableStateOf(null)
    }
    var uidRefId: String? by remember {
        mutableStateOf(null)
    }
    var depositId: String? by remember {
        mutableStateOf(null)
    }
    val successIcon = getImagePainter(
        context = context,
        imageUrl = viewModel.uiResponse.value?.data?.uiJson?.screens?.transactionScreen?.column?.column?.statusIcon?.image?.successIconURL?:""
    )
    val failedIcon = getImagePainter(
        context = context,
        imageUrl = viewModel.uiResponse.value?.data?.uiJson?.screens?.transactionScreen?.column?.column?.statusIcon?.image?.failedIconURL?:""
    )
    val brandLogo = getImagePainter(
        context = context,
        imageUrl = viewModel.uiResponse.value?.data?.uiJson?.screens?.transactionScreen?.column?.column?.statusIcon?.image?.brandLogoURL?:""
    )
    var statusIcon = successIcon

    val transactionData = viewModel.apiResponse
    transactionData.value?.let {
        transactionStatus = it.status
        aadhaarNo = it.originIdentifier
        createdDate = it.createdDate
        transactionID = it.txId
        bankName = it.bankName
        referenceNo = it.apiTid
        balanceAmount = it.balance
        transactionType = it.transactionMode
        isRetriable = it.isRetriable ?: false
        apiComment = it.apiComment
        authCode = it.authCode
        uidRefId = it.uidRefId
        depositId = it.depositId
    }

    if (transactionStatus != null) {
        aadhaarNo = if (aadhaarNo == "" || aadhaarNo == null) {
            "N/A"
        } else {
            val buf = StringBuffer(aadhaarNo!!)
            buf.replace(0, 10, "XXXX-XXXX-")
            buf.toString()
        }
        if (balanceAmount != "" && !balanceAmount.equals("N/A", ignoreCase = true)) {
            balance = "Rs. $balanceAmount"
        }
        cardText = if (transactionType.equals(
                "AEPS_CASH_WITHDRAWAL",
                ignoreCase = true
            ) || SdkConstants.transactionType == SdkConstants.cashWithdrawal
            || SdkConstants.transactionType == SdkConstants.aadhaarPay
        ) {
            "Txn Amount: Rs.  ${viewModel.amount.value}"
        } else {
            "Balance Amount: $balance"
        }
        if (transactionStatus.equals("SUCCESS", ignoreCase = true)) {
            statusTxt = "Transaction Success"
            bkgColor = greenColor
        } else if (transactionStatus.equals("REFUNDED", ignoreCase = true)) {
            statusTxt = transactionStatus
            bkgColor = yellowColor
            statusIcon = failedIcon
            balanceText = apiComment.toString()
        } else if (transactionStatus.equals("FAILED", true)) {
            if (isRetriable == true) {
                shouldShowRetryBtn = true
                balanceText = apiComment.toString()
                statusIcon = failedIcon
                statusTxt = "Transaction Failed"
                bkgColor = redColor
            } else {
                balanceText = apiComment.toString()
                statusIcon = failedIcon
                statusTxt = "Transaction Failed"
                bkgColor = redColor
            }
        } else {
            balanceText = apiComment.toString()
            statusIcon = successIcon
            statusTxt = transactionStatus
            bkgColor = yellowColor
        }
    } else {
        balanceText = apiComment.toString()
        statusIcon = failedIcon
        statusTxt = "Failed"
        bkgColor = redColor
    }
    val transactionTypeName = when (SdkConstants.transactionType) {
        "0" -> {
            "AEPS_BALANCE_ENQUIRY"
        }

        "1" -> {
            "AEPS_CASH_WITHDRAWAL"
        }

        "2" -> {
            "AEPS_MINI_STATEMENT"
        }

        "4" -> {
            "AEPS_CASH_DEPOSIT"
        }

        else -> {
            "AadhaarPay"
        }
    }

    if (printClicked.value) {
        if (viewModel.isStartPrinting.value) {
            val aepsSdkMainActivity = MainActivity()
            aepsSdkMainActivity.ShowBluetoothDevices(context, viewModel, printClicked) {
                mAemprinter = GetPosConnectedPrinter.aemPrinter
                callBluetoothFunction(
                    transactionID?:"N/A",
                    aadhaarNo?:"N/A",
                    createdDate?:"N/A",
                    bankName?:"N/A",
                    referenceNo?:"N/A",
                    transactionType?:transactionTypeName,
                    mAemprinter,
                    transactionStatus?:"N/A",
                    balanceAmount?:"N/A",
                    viewModel.amount.value,
                )
                printClicked.value = false
            }
        }
    }

    val location =
        if (SdkConstants.location != "" || SdkConstants.location.isNotEmpty() && SdkConstants.location != "null") {
            SdkConstants.location
        } else {
            "N/A"
        }
    TransactionReceiptScreen(
        brandLogo = brandLogo,
        statusIcon = statusIcon,
        status = statusTxt ?: "N/A",
        failureStatus = balanceText,
        transactionType = transactionType ?: transactionTypeName,
        date = createdDate ?: "N/A",
        txnId = transactionID ?: "N/A",
        rrn = referenceNo ?: "N/A",
        aadhaarNo = aadhaarNo ?: "N/A",
        bankName = bankName ?: "N/A",
        txnAmount = viewModel.amount.value ?: "N/A",
        balance = balance,
        bkgColor = bkgColor,
        context = context,
        agent = SdkConstants.agent.ifEmpty { "N/A" },
        location = location,
        terminalId = viewModel.terminalId.value ?: "N/A",
        uidRef = uidRefId ?: "N/A",
        authCode = authCode ?: "N/A",
        depositID = depositId ?: "N/A",
        downloadPDF = {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.TIRAMISU) {
                if ((ActivityCompat.checkSelfPermission(
                        context, Manifest.permission.READ_EXTERNAL_STORAGE
                    ) != PackageManager.PERMISSION_GRANTED) && (ActivityCompat.checkSelfPermission(
                        context, Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ) != PackageManager.PERMISSION_GRANTED)
                ) {
                    ActivityCompat.requestPermissions(
                        context as Activity, SdkConstants.permissionsList, 1
                    )
                    return@TransactionReceiptScreen
                }
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                if ((ActivityCompat.checkSelfPermission(
                        context, Manifest.permission.READ_MEDIA_IMAGES
                    ) != PackageManager.PERMISSION_GRANTED) && (ActivityCompat.checkSelfPermission(
                        context, Manifest.permission.READ_MEDIA_VIDEO
                    ) != PackageManager.PERMISSION_GRANTED)
                    && (ActivityCompat.checkSelfPermission(
                        context, Manifest.permission.READ_MEDIA_AUDIO
                    ) != PackageManager.PERMISSION_GRANTED)
                ) {
                    ActivityCompat.requestPermissions(
                        context as Activity, SdkConstants.permissionsList1, 1
                    )
                    return@TransactionReceiptScreen
                }
            }
            createPdf(
                context,
                viewModel.apiResponse.value?.ministatement,
                false,
                transactionAmount = viewModel.amount.value,
                status = transactionStatus ?: "FAILED",
                createdDate = createdDate ?: "N/A",
                transactionMode = transactionType ?: transactionTypeName,
                transactionID = transactionID ?: "N/A",
                aadhaarNo = aadhaarNo ?: "N/A",
                bankName = bankName ?: "N/A",
                referenceNo = referenceNo ?: "N/A",
                balanceAmount = balanceAmount ?: "N/A"
            )
        },
        printData = {
            printClicked.value = !printClicked.value
            val printData = PrintData(
                status = transactionData.value?.status ?: "FAILED",
                txId = transactionData.value?.txId ?: "N/A",
                aadhaarNo = transactionData.value?.originIdentifier ?: "N/A",
                createdDate = transactionData.value?.createdDate ?: "N/A",
                bankName = transactionData.value?.bankName ?: "N/A",
                apiTid = transactionData.value?.apiTid ?: "N/A",
                balance = transactionData.value?.balance ?: "N/A",
                transactionMode = transactionData.value?.transactionMode
                    ?: transactionTypeName,
                amount = viewModel.amount.value
            )
            startPrintFunction(
                context,
                viewModel,
                SdkConstants.permissions12,
                bluetoothAdapter,
                activityResultLauncher,
                printClicked,
                printData
            )
        }
    )
}

fun callBluetoothFunction(
    txId: String,
    originIdentifier: String,
    createdDate: String,
    bankName: String,
    apiTid: String,
    transactionMode: String,
    mAemprinter: AEMPrinter?,
    status: String,
    balance: String,
    amount: String,
) {
   val balanceAmount = if(balance.isNotEmpty() && balance!= "N/A"){
       "Rs. $balance"
   }else{
       balance.ifEmpty { "N/A" }
   }
    try {
        mAemprinter?.let {
            mAemprinter.setFontType(AEMPrinter.DOUBLE_HEIGHT)
            mAemprinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER)
            mAemprinter.setFontType(AEMPrinter.FONT_NORMAL)
            mAemprinter.setFontType(AEMPrinter.FONT_002)
            mAemprinter.POS_FontThreeInchCENTER()
            mAemprinter.print(SdkConstants.SHOP_NAME)
            mAemprinter.print("\n")
            mAemprinter.setFontType(AEMPrinter.DOUBLE_HEIGHT)
            mAemprinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER)
            mAemprinter.print("-----Transaction Report-----\n")
            mAemprinter.POS_FontThreeInchCENTER()
            mAemprinter.print(status)
            mAemprinter.print("\n\n")
            mAemprinter.print("Transaction ID: ${txId.ifEmpty { "N/A" }}")
            mAemprinter.print("\n")
            mAemprinter.print("Aadhaar Number: ${originIdentifier.ifEmpty { "N/A" }}")
            mAemprinter.print("\n")
            mAemprinter.print("Date/Time: ${createdDate.ifEmpty { "N/A" }}")
            mAemprinter.print("\n")
            mAemprinter.print("Bank Name: ${bankName.ifEmpty { "N/A" }}")
            mAemprinter.print("\n")
            mAemprinter.print("RRN: ${apiTid.ifEmpty { "N/A" }}")
            mAemprinter.print("\n")
            if(SdkConstants.transactionType != "4"){
                mAemprinter.print("Balance Amount:$balanceAmount")
                mAemprinter.print("\n")
            }
            if (SdkConstants.transactionType == "1" || SdkConstants.transactionType == "3"|| SdkConstants.transactionType == "4") {
                mAemprinter.print("Transaction Amount: Rs. $amount")
                mAemprinter.print("\n")
            }
            mAemprinter.print("\n")
            mAemprinter.print("Transaction Type: ${transactionMode.ifEmpty { "N/A" }}")
            mAemprinter.print("\n\n")
            mAemprinter.setFontType(AEMPrinter.FONT_002)
            mAemprinter.POS_FontThreeInchRIGHT()
            mAemprinter.print("Thank You \n")
            mAemprinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER)
            mAemprinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_RIGHT)
            mAemprinter.POS_FontThreeInchRIGHT()
            mAemprinter.print(SdkConstants.BRAND_NAME)
            mAemprinter.setCarriageReturn()
            mAemprinter.setCarriageReturn()
            mAemprinter.setCarriageReturn()
            mAemprinter.setCarriageReturn()
            val data = printerStatus(mAemprinter)
            mAemprinter.print(data)
            mAemprinter.print("\n")

        }
    } catch (e: IOException) {
        e.printStackTrace()
    }
}

@Throws(IOException::class)
fun printerStatus(mAemprinter: AEMPrinter): String {
    val printerStatus = charArrayOf(
        0x1B.toChar(),
        0x7E.toChar(),
        0x42.toChar(),
        0x50.toChar(),
        0x7C.toChar(),
        0x47.toChar(),
        0x45.toChar(),
        0x54.toChar(),
        0x7C.toChar(),
        0x50.toChar(),
        0x52.toChar(),
        0x4E.toChar(),
        0x5F.toChar(),
        0x53.toChar(),
        0x54.toChar(),
        0x5E.toChar()
    )
    val data = String(printerStatus)
    mAemprinter.print(data)
    return data
}

fun startPrintFunction(
    context: Context,
    viewModel: UnifiedAepsViewModel,
    permissions12: Array<String>,
    bluetoothAdapter: BluetoothAdapter?,
    activityResultLauncher: ManagedActivityResultLauncher<Intent, ActivityResult>,
    printClicked: MutableState<Boolean>,
    printData: PrintData?
) {
    val deviceModel = Build.MODEL
    if (deviceModel.equals("WPOS-3", ignoreCase = true)) {
        //start printing with wiseasy internal printer

        if (printData != null) {
            UnifiedPrintReceiptThread(context, viewModel, printData).start()
        }
    } else {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.R) {
            // Device is running Android 10 or below
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.BLUETOOTH)
                != PackageManager.PERMISSION_GRANTED
            ) {
                // Permission is not granted, request it
                ActivityCompat.requestPermissions(
                    context as Activity, permissions12, 1
                )
                printClicked.value = false
                return
            } else {
                if (bluetoothAdapter != null) {
                    if (bluetoothAdapter.isEnabled) {
                        viewModel.isStartPrinting.value = true
                    } else {
                        GetPosConnectedPrinter.aemPrinter = null
                        val turnOn = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                        activityResultLauncher.launch(turnOn)
                        printClicked.value = false
                        viewModel.isStartPrinting.value = false
                    }
                }
            }
        } else {
            if (ActivityCompat.checkSelfPermission(
                    context, Manifest.permission.BLUETOOTH_CONNECT
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    context as Activity, permissions12, 1
                )
                printClicked.value = false
                return
            } else {
                if (bluetoothAdapter != null) {
                    if (bluetoothAdapter.isEnabled) {
                        viewModel.isStartPrinting.value = true
                    } else {
                        GetPosConnectedPrinter.aemPrinter = null
                        val turnOn = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                        activityResultLauncher.launch(turnOn)
                        printClicked.value = false
                        viewModel.isStartPrinting.value = false
                    }
                }
            }
        }
    }
}

class UnifiedPrintReceiptThread(
    private val context: Context,
    private val viewModel: UnifiedAepsViewModel,
    private val printData: PrintData
) : Thread() {
    private var mPrinter: Printer? = null
    override fun run() {
        mPrinter = Printer(context)
        try {
            mPrinter!!.setPrintType(0) //Printer type 0 means it's an internal printer
        } catch (e: RemoteException) {
            e.printStackTrace()
        }
        checkPrinterStatus(printData)
    }

    private fun checkPrinterStatus(printData: PrintData) {
        try {
            val status = IntArray(1)
            mPrinter?.getPrinterStatus(status)
            Log.e("TAG", "Printer Status is " + status[0])
            val msg: String
            when (status[0]) {
                0x00 -> {
                    msg = "Printer status OK"
                    Log.e("TAG", "check printer status: $msg")
                    startPrinting(printData)
                }

                0x01 -> {
                    msg = "Parameter error"
                    showLog(msg)
                }

                0x8A -> {
                    msg = "Out of Paper"
                    showLog(msg)
                }

                0x8B -> {
                    msg = "Overheat"
                    showLog(msg)
                }

                else -> {
                    msg = "Printer Error"
                    showLog(msg)
                }
            }
        } catch (e: RemoteException) {
            e.printStackTrace()
        }
    }

    private fun startPrinting(printData: PrintData) {
        val result: Int
        try {
            result = mPrinter!!.printInit()
            Log.e("TAG", "startPrinting: Printer init result $result")
            mPrinter!!.clearPrintDataCache()
            if (result == 0) {
                printReceipt(printData)
            } else {
                Toast.makeText(
                    context, "Printer initialization failed", Toast.LENGTH_SHORT
                ).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun printReceipt(printData: PrintData) {
        var result: Int
        try {
            Log.e("TAG", "printReceipt: set density low 3")
            mPrinter!!.setGrayLevel(3)
            result = mPrinter!!.printStringExt(
                SdkConstants.SHOP_NAME,
                0,
                0f,
                2.0f,
                Printer.Font.SANS_SERIF,
                25,
                Printer.Align.CENTER,
                true,
                false,
                true
            )
            result =
                mPrinter!!.printString(
                    "Transaction Report",
                    25,
                    Printer.Align.CENTER,
                    true,
                    false
                )
            result = mPrinter!!.printString(printData.status.ifEmpty { "N/A" }, 25, Printer.Align.CENTER, true, false)

            result = mPrinter!!.printString(
                "------------------------------------------",
                30,
                Printer.Align.CENTER,
                true,
                false
            )
            result = mPrinter!!.printString(
                "Transaction Id :${printData.txId.ifEmpty { "N/A" }}", 18, Printer.Align.LEFT, true, false
            )
            result = mPrinter!!.printString(
                "Aadhaar Number :${printData.aadhaarNo.ifEmpty { "N/A" }}",
                18,
                Printer.Align.LEFT,
                true,
                false
            )
            result = mPrinter!!.printString(
                "Date/Time : " + printData.createdDate.ifEmpty { "N/A" },
                18,
                Printer.Align.LEFT,
                false,
                false
            )
            result = mPrinter!!.printString(
                "Bank Name : ${printData.bankName.ifEmpty { "N/A" }}", 18, Printer.Align.LEFT, true, false
            )
            result = mPrinter!!.printString(
                "RRN : ${printData.apiTid.ifEmpty { "N/A" }}", 18, Printer.Align.LEFT, true, false
            )
            if(SdkConstants.transactionType != "4"){
                result = mPrinter!!.printString(
                    "Balance Amount :${printData.balance.ifEmpty { "N/A" }}", 18, Printer.Align.LEFT, true, false
                )
            }
            if (SdkConstants.transactionType == "1" || SdkConstants.transactionType == "3" || SdkConstants.transactionType == "4") {
                result = mPrinter!!.printString(
                    "Transaction Amount : Rs ." + viewModel.amount.value,
                    18,
                    Printer.Align.LEFT,
                    true,
                    false
                )
            }
            result = mPrinter!!.printString(
                "Transaction Type : ${printData.transactionMode.ifEmpty { "N/A" }}",
                18,
                Printer.Align.LEFT,
                true,
                false
            )
            result = mPrinter!!.printStringExt(
                "Thank You !",
                0,
                0f,
                2.0f,
                Printer.Font.SANS_SERIF,
                18,
                Printer.Align.RIGHT,
                true,
                true,
                false
            )
            result = mPrinter!!.printStringExt(
                SdkConstants.BRAND_NAME,
                0,
                0f,
                2.0f,
                Printer.Font.SANS_SERIF,
                18,
                Printer.Align.RIGHT,
                true,
                true,
                false
            )
            result = mPrinter!!.printString(" ", 25, Printer.Align.CENTER, false, false)
            result = mPrinter!!.printString(" ", 25, Printer.Align.CENTER, false, false)
            Log.e(
                "TAG", "printReceipt: print thank You result $result"
            )
            result = mPrinter!!.printPaper(30)
            Log.e(
                "TAG", "printReceipt: print step result $result"
            )
            showPrinterStatus(result)
            result = mPrinter!!.printFinish()
            Log.e(
                "TAG", "printReceipt: printer finish result $result"
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun showPrinterStatus(result: Int) {
        val msg: String
        when (result) {
            0x00 -> {
                msg = "Print Finish"
                showLog(msg)
            }

            0x01 -> {
                msg = "Parameter error"
                showLog(msg)
            }

            0x8A -> {
                msg = "Out of Paper"
                showLog(msg)
            }

            0x8B -> {
                msg = "Overheat"
                showLog(msg)
            }

            else -> {
                msg = "Printer Error"
                showLog(msg)
            }
        }
    }


    private fun showLog(msg: String) {
        (context as Activity).runOnUiThread {
            Toast.makeText(
                context, msg, Toast.LENGTH_SHORT
            ).show()
        }
        Log.e("TAG", "Printer status: $msg")
    }

}

@Composable
fun TransactionReceiptScreen(
    brandLogo: AsyncImagePainter,
    statusIcon: AsyncImagePainter,
    status: String,
    failureStatus: String,
    transactionType: String,
    date: String,
    txnId: String,
    rrn: String,
    aadhaarNo: String,
    bankName: String,
    txnAmount: String,
    balance: String,
    bkgColor: Color,
    context: Context,
    agent: String,
    location: String,
    terminalId: String,
    uidRef: String,
    authCode: String,
    depositID: String,
    downloadPDF: () -> Unit,
    printData: () -> Unit
) {
    val transactionAmount = if (txnAmount.equals("N/A", true)) {
        txnAmount
    } else {
        "Rs. $txnAmount"
    }
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.White)
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .verticalScroll(
                    rememberScrollState()
                )
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .background(bkgColor)
                    .wrapContentHeight(),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Spacer(modifier = Modifier.height(20.dp))
                Row(
                    modifier =
                    Modifier
                        .align(Alignment.End)
                        .wrapContentWidth()
                ) {
                    Image(
                        painter = painterResource(
                            id = R.drawable.close
                        ), contentDescription = "close",
                        modifier = Modifier
                            .size(24.dp)
                            .clickable {
                                (context as Activity).finish()
                            },
                        colorFilter = ColorFilter.tint(Color.White)
                    )
                    Spacer(modifier = Modifier.width(20.dp))
                }
                Image(
                    painter = statusIcon,
                    contentDescription = "status",
                    modifier = Modifier.size(60.dp)
                )
                Spacer(modifier = Modifier.height(20.dp))
                Text(
                    text =
                    status, fontSize = 24.sp, color = Color.White
                )
                if (!status.equals("SUCCESS", true)) {
                    Text(
                        text =
                        failureStatus,
                        fontSize = 12.sp,
                        color = Color.White
                    )
                }
                Spacer(modifier = Modifier.height(20.dp))
            }
            Column(
                modifier =
                Modifier.fillMaxWidth(), horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Image(
                    painter = brandLogo,
                    contentDescription = "",
                    modifier = Modifier.size(80.dp)
                )
                Spacer(modifier = Modifier.height(5.dp))
                Text(
                    text =
                    "Aadhaar Enabled Payment System",
                    fontSize = 14.sp,
                    fontWeight = FontWeight.Bold,
                    color = Color.Black
                )
                Spacer(modifier = Modifier.height(5.dp))
                Text(
                    text =
                    "India Post Payments Bank", fontSize = 15.sp, color = Color.Black
                )
                Spacer(modifier = Modifier.height(15.dp))
                Text(
                    text = transactionType.ifEmpty { "N/A" },
                    fontSize = 20.sp,
                    color = Color.Black
                )
            }
            Spacer(modifier = Modifier.height(15.dp))
            Column(
                modifier =
                Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 15.dp)
            ) {
                Text(
                    text = "DATE / TIME : ${date.ifEmpty { "N/A" }}",
                    fontSize = 14.sp,
                    color = Color.Black
                )
                Spacer(modifier = Modifier.height(5.dp))
                Text(
                    text = "AGENT ID : ${SdkConstants.userNameFromCoreApp.ifEmpty { "N/A" }}",
                    fontSize = 14.sp,
                    color = Color.Black
                )
                Spacer(modifier = Modifier.height(5.dp))
                Text(
                    text = "AGENT NAME: ${agent.ifEmpty { "N/A" }}",
                    fontSize = 14.sp,
                    color = Color.Black
                )
                Spacer(modifier = Modifier.height(5.dp))
                Text(
                    text = "LOCATION : ${location.ifEmpty { "N/A" }}",
                    fontSize = 14.sp,
                    color = Color.Black
                )
                Spacer(modifier = Modifier.height(5.dp))
                Text(
                    text = "TERMINAL ID : ${terminalId.ifEmpty { "N/A" }}",
                    fontSize = 14.sp,
                    color = Color.Black
                )
                Spacer(modifier = Modifier.height(5.dp))
                Text(
                    text = "STAN : ${rrn.takeLast(6).ifEmpty { "N/A" }}",
                    fontSize = 14.sp,
                    color = Color.Black
                )
                Spacer(modifier = Modifier.height(5.dp))
                Text(
                    text =
                    "TXN ID : $txnId", fontSize = 14.sp, color = Color.Black
                )
                Spacer(modifier = Modifier.height(5.dp))
                Text(text = "RRN : $rrn", fontSize = 14.sp, color = Color.Black)
                Spacer(modifier = Modifier.height(5.dp))
                Text(
                    text = "CUSTOMER ID : ${aadhaarNo.ifEmpty { "N/A" }}",
                    fontSize = 14.sp,
                    color = Color.Black
                )
                Spacer(modifier = Modifier.height(5.dp))
                Text(
                    text = "UID REF : ${uidRef.ifEmpty { "N/A" }}",
                    fontSize = 14.sp,
                    color = Color.Black
                )
                Spacer(modifier = Modifier.height(5.dp))

                Text(
                    text = "AUTHORIZE CODE : ${authCode.ifEmpty { "N/A" }}",
                    fontSize = 14.sp,
                    color = Color.Black
                )
                Spacer(modifier = Modifier.height(5.dp))
                if(SdkConstants.transactionType=="4") {
                    Text(
                        text = "DEPOSIT ID : ${depositID.ifEmpty { "N/A" }}",
                        fontSize = 14.sp,
                        color = Color.Black
                    )
                    Spacer(modifier = Modifier.height(5.dp))
                }
                Text(
                    text = "STATUS : ${
                        if (status.equals(
                                "Transaction Failed",
                                true
                            )
                        ) "FAILED" else if(status.equals("Transaction Success",true)){
                            "SUCCESS"
                        }else{
                            status
                        }
                    }", fontSize = 14.sp, color = Color.Black
                )
                Spacer(modifier = Modifier.height(5.dp))
                Text(
                    text = "CUSTOMER BANK : ${bankName.ifEmpty { "N/A" }}",
                    fontSize = 14.sp,
                    color = Color.Black
                )
                Spacer(modifier = Modifier.height(5.dp))
                if (SdkConstants.transactionType == "1" || SdkConstants.transactionType == "3"|| SdkConstants.transactionType == "4") {
                    Text(text = "TXN AMOUNT : $transactionAmount", fontSize = 14.sp, color = Color.Black)
                    Spacer(modifier = Modifier.height(5.dp))
                }
                if(SdkConstants.transactionType != "4"){
                    Text(text = "BALANCE : $balance", fontSize = 14.sp, color = Color.Black)
                    Spacer(modifier = Modifier.height(5.dp))
                }
            }
            Spacer(modifier = Modifier.height(15.dp))
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentHeight()
                    .padding(horizontal = 10.dp)
            ) {
                OutlinedButton(
                    onClick = { downloadPDF.invoke() },
                    modifier = Modifier.weight(1f),
                    border = BorderStroke(1.dp, Color.Gray),
                    colors = ButtonDefaults.buttonColors(backgroundColor = Color.White),
                    shape = RoundedCornerShape(10.dp)
                ) {
                    Text(text = "Download")
                }
                Spacer(modifier = Modifier.width(10.dp))
                OutlinedButton(
                    onClick = {
                        printData.invoke()
                    },
                    modifier = Modifier.weight(1f),
                    border = BorderStroke(1.dp, Color.Gray),
                    colors = ButtonDefaults.buttonColors(backgroundColor = Color.White),
                    shape = RoundedCornerShape(10.dp)
                ) {
                    Text(
                        text = "Print"
                    )
                }
            }
            Spacer(modifier = Modifier.height(20.dp))
        }
    }
}