package com.aeps.aeps_ippb.presentation.transaction_status

data class PrintData(
    var status: String,
    var txId:String,
    var aadhaarNo:String,
    var createdDate:String,
    var bankName:String,
    var apiTid:String,
    var balance:String,
    var transactionMode:String,
    var amount:String
)
