package com.aeps.aeps_ippb.presentation.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import com.aeps.aeps_ippb.R
import com.aeps.aeps_ippb.presentation.viewmodel.UnifiedAepsViewModel

@Composable
fun BluetoothDeviceDialog(
    viewModel: UnifiedAepsViewModel,
    printerName: ArrayList<String>,
    pairPrinter: (String) -> Unit
) {
    Dialog(
        onDismissRequest = {
        },
        properties = DialogProperties(
            dismissOnBackPress = false,
            dismissOnClickOutside = false
        )
    ) {
        Card(
            elevation = 5.dp,
            shape = RoundedCornerShape(4.dp),
            modifier = Modifier
                .fillMaxWidth()
                .defaultMinSize(minWidth = 318.dp)
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
            ) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = 14.dp, horizontal = 21.dp),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    TransactionDialogText(
                        text = "Select Printer to connect",
                        fontSize = 16.sp
                    )
                    Icon(
                        painter = painterResource(id = R.drawable.close),
                        contentDescription = "close icon",
                        modifier = Modifier
                            .size(ButtonDefaults.IconSize)
                            .clickable(onClick = {
                                viewModel.isStartPrinting.value = !viewModel.isStartPrinting.value
                            }),
                        Color.Black
                    )
                }
                Divider()
                Spacer(modifier = Modifier.height(16.dp))
                LazyColumn {
                    items(printerName.size) {
                        Text(
                            text = printerName[it],
                            modifier = Modifier
                                .clickable {
                                    pairPrinter.invoke(printerName[it])
                                }
                                .fillMaxWidth(),
                            textAlign = TextAlign.Center
                        )
                        Divider(modifier = Modifier.padding(5.dp))
                    }
                }
            }
        }
    }
}

@Composable
fun TransactionDialogText(
    text: String,
    modifier: Modifier = Modifier,
    fontSize: TextUnit = 14.sp,
    fontColor: Color = Color.Black
) {
    Text(
        text = text,
        modifier = modifier,
        color = fontColor,
        fontSize = fontSize
    )
}
