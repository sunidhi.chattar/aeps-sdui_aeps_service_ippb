package com.aeps.aeps_ippb.presentation.unified_bioauth

import android.Manifest
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.widget.Toast
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.AlertDialog
import androidx.compose.material.BottomSheetScaffold
import androidx.compose.material.BottomSheetValue
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.material.rememberBottomSheetScaffoldState
import androidx.compose.material.rememberBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.DialogProperties
import androidx.core.app.ActivityCompat
import androidx.navigation.NavHostController
import com.aeps.aeps_ippb.R
import com.aeps.aeps_ippb.bluetooth.BluetoothConnectionActivity
import com.aeps.aeps_ippb.common.SdkConstants
import com.aeps.aeps_ippb.common.SdkConstants.Companion.permissions12
import com.aeps.aeps_ippb.driver_data.DriverActivity
import com.aeps.aeps_ippb.presentation.components.CustomLoader
import com.aeps.aeps_ippb.presentation.components.TopActionBar
import com.aeps.aeps_ippb.presentation.navgraph.Destinations
import com.aeps.aeps_ippb.presentation.viewmodel.UnifiedAepsViewModel
import com.aeps.aeps_ippb.ui.theme.HeadingColor
import com.aeps.aeps_ippb.ui.theme.fingerGrey
import com.aeps.aeps_ippb.ui.theme.msTextColor
import com.aeps.aeps_ippb.ui.theme.primaryColor
import com.aeps.aeps_ippb.ui.theme.txnBkgbkgblue
import com.aeps.aeps_ippb.ui.theme.txnBkglightblue
import com.aeps.aeps_ippb.ui.theme.txnBkgtextColor
import com.aeps.aeps_ippb.ui.theme.txnTextColor
import kotlinx.coroutines.launch

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun BioAuthFingerCaptureScreen(
    navHostController: NavHostController,
    viewModel: UnifiedAepsViewModel
) {
    val context = LocalContext.current
    val sheetState = rememberBottomSheetState(initialValue = BottomSheetValue.Collapsed)
    val scaffoldState = rememberBottomSheetScaffoldState(bottomSheetState = sheetState)
    val scope = rememberCoroutineScope()
    val isCaptured = remember {
        mutableStateOf(false)
    }
    val bioAuthFingerEnabled = remember {
        mutableStateOf(true)
    }
    isCaptured.value = sheetState.isExpanded
    bioAuthFingerEnabled.value = sheetState.isCollapsed
    val isPlaying = remember {
        mutableStateOf(false)
    }

    LaunchedEffect(key1 = true) {
        viewModel.fetchUiData(navHostController)
    }
    val uiData = viewModel.uiResponse
    if (viewModel.isFingerClicked.value) {
        if (viewModel.scoreStr.value != "0") {
            if (viewModel.scoreStr.value.contains(",")) {
                Toast.makeText(
                    context,
                    "Invalid Fingerprint Data",
                    Toast.LENGTH_SHORT
                )
                    .show()
            } else {
                isPlaying.value = true
                LaunchedEffect(key1 = !bioAuthFingerEnabled.value) {
                    if (sheetState.isCollapsed) {
                        sheetState.expand()
                    }
                }
            }
        } else if (viewModel.isFingerCaptureFailed.value) {
            isPlaying.value = false
            LaunchedEffect(key1 = !bioAuthFingerEnabled.value) {
                sheetState.collapse()
            }
        }
    }
    if (viewModel.showDialog.value) {
        AlertDialog(
            properties = DialogProperties(
                dismissOnBackPress = false,
                dismissOnClickOutside = false
            ),
            onDismissRequest = { viewModel.showDialog.value = false },
            confirmButton = {
                Text(text = "Ok",
                    modifier = Modifier
                        .clickable {
                            viewModel.showDialog.value = false
                            if (!viewModel.bioAuthVisible.value) {
                                (context as Activity).finish()
                            }
                        }
                        .padding(10.dp)
                )
            },
            title = {
                Text(text = viewModel.errorMessage.value ?: "")
            }
        )
    }
    BottomSheetScaffold(
        topBar = {
            TopActionBar(context)
        },
        scaffoldState = scaffoldState,
        sheetContent = {
            BioAuthSheetContent(
                viewModel = viewModel,
                isFingerCaptureFailed = viewModel.isFingerCaptureFailed,
                isPlaying = isPlaying,
                progressScore = viewModel.scoreStr.value.toFloat(),
                submitBioAuth = {
                    viewModel.onSubmitBioAuth(context) { res ->
                        val status = res.status
                        if (status == 1) {
                            Toast.makeText(context, "Bioauth "+res.statusDesc, Toast.LENGTH_SHORT).show()
                            viewModel.scoreStr.value = "0"
                            viewModel.isFingerClicked.value = false
                            navHostController.navigate(Destinations.FINGERPRINT_CAPTURE_ROUTE) {
                                this.popUpTo(Destinations.BIO_AUTH_SCREEN) {
                                    inclusive = true
                                }
                            }
                        } else {
                            Toast.makeText(
                                context,
                                res.statusDesc.toString(),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }
            ) {
                scope.launch {
                    sheetState.collapse()
                    viewModel.scoreStr.value = "0"
                    isCaptured.value = true
                    isPlaying.value = false
                    try {
                        viewModel.isLoading.value = true
                        val intent =
                            Intent(
                                context,
                                DriverActivity::class.java
                            )
                        context.startActivity(intent)
                        viewModel.isFingerClicked.value = true
                    } catch (e: Exception) {
                        Toast
                            .makeText(
                                context,
                                e.localizedMessage?.toString()
                                    ?: "Activity Not Found",
                                Toast.LENGTH_SHORT
                            )
                            .show()
                    }
                    bioAuthFingerEnabled.value = false
                }
            }
        },
        sheetShape = RoundedCornerShape(topStart = 40.dp, topEnd = 40.dp),
        sheetBackgroundColor = Color.White,
        sheetPeekHeight = 0.dp,
        sheetElevation = 20.dp,
        sheetGesturesEnabled = false
    ) {
        if (uiData.value == null) {
            CustomLoader()
        } else {
            uiData.value?.data?.uiJson?.appColor?.let {
                primaryColor = Color(android.graphics.Color.parseColor(it.primaryColor))
                txnBkgtextColor = Color(android.graphics.Color.parseColor(it.txnBkgtextColor))
                txnBkglightblue = Color(android.graphics.Color.parseColor(it.txnBkglightblue))
                txnBkgbkgblue = Color(android.graphics.Color.parseColor(it.txnBkgbkgblue))
                msTextColor = Color(android.graphics.Color.parseColor(it.msTextColor))
                txnTextColor = Color(android.graphics.Color.parseColor(it.txnTextColor))
                fingerGrey = Color(android.graphics.Color.parseColor(it.fingerGrey))
            }
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .background(
                        color = if (isCaptured.value) Color.Gray else Color.Transparent
                    )
            ) {
                if (viewModel.isLoading.value && !viewModel.showLoader.value) {
                    CustomLoader()
                }
                if (viewModel.bioAuthVisible.value) {
                    if (viewModel.showDeviceTypeDialogBio.value) {
                        ShowDialogToChooseDeviceType(viewModel.showDeviceTypeDialogBio, context)
                    }
                    Column(
                        modifier = Modifier
                            .padding(horizontal = 33.dp)
                            .verticalScroll(rememberScrollState()),
                        horizontalAlignment = Alignment.Start
                    ) {
                        Spacer(modifier = Modifier.height(it.calculateTopPadding() + 45.dp))
                        Text(
                            modifier = Modifier.fillMaxWidth(),
                            textAlign = TextAlign.Center,
                            text = "Verify your Identity!",
                            color = HeadingColor,
                            fontSize = 20.sp,
                            fontFamily = FontFamily(Font(R.font.poppins_semibold))
                        )
                        Spacer(modifier = Modifier.height(8.dp))
                        Text(
                            fontFamily = FontFamily(Font(R.font.poppins_regular)),
                            text = "Biometric Authentication",
                            modifier = Modifier
                                .padding(top = 10.dp)
                                .fillMaxWidth(),
                            fontSize = 12.sp,
                            textAlign = TextAlign.Center
                        )

                        Spacer(modifier = Modifier.height(54.dp))
                        Image(
                            modifier = Modifier
                                .size(100.dp)
                                .align(Alignment.CenterHorizontally),
                            painter = painterResource(id = R.drawable.fingerprint_scanner1),
                            contentDescription = "",
                            colorFilter = if (bioAuthFingerEnabled.value) ColorFilter.tint(
                                primaryColor
                            ) else ColorFilter.tint(
                                fingerGrey
                            )
                        )
                        Spacer(modifier = Modifier.height(23.dp))
                        if (!isCaptured.value) {
                            Button(
                                onClick = {
                                    isCaptured.value = true
                                    if (bioAuthFingerEnabled.value) {
                                        try {
                                            viewModel.isLoading.value = true
                                            val intent =
                                                Intent(
                                                    context,
                                                    DriverActivity::class.java
                                                )
                                            context.startActivity(intent)
                                            viewModel.isFingerClicked.value = true
                                        } catch (e: Exception) {
                                            Toast
                                                .makeText(
                                                    context,
                                                    e.localizedMessage?.toString()
                                                        ?: "Activity Not Found",
                                                    Toast.LENGTH_SHORT
                                                )
                                                .show()
                                        }
                                    }
                                    bioAuthFingerEnabled.value = false
                                },
                                modifier = Modifier.fillMaxWidth(),
                                colors = ButtonDefaults.buttonColors(
                                    backgroundColor = primaryColor
                                ),
                                shape = RoundedCornerShape(10.dp),
                                enabled = bioAuthFingerEnabled.value
                            ) {
                                Text(
                                    text = "CAPTURE",
                                    fontFamily = FontFamily(Font(R.font.poppins_semibold)),
                                    fontSize = 16.sp,
                                    color = Color.White
                                )
                            }
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun ShowDialogToChooseDeviceType(
    showDeviceTypeDialog: MutableState<Boolean>,
    context: Context
) {
    val bluetoothManager: BluetoothManager? = context.getSystemService(
        BluetoothManager::class.java
    )
    val bluetoothAdapter: BluetoothAdapter? = bluetoothManager!!.adapter
    val activityResultLauncher =
        rememberLauncherForActivityResult(contract = ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                Toast.makeText(context, "Bluetooth Enabled", Toast.LENGTH_SHORT).show()
            }
        }

    AlertDialog(
        modifier = Modifier.border(
            width = 2.dp,
            shape = RoundedCornerShape(20.dp),
            color = primaryColor
        ),
        properties = DialogProperties(
            dismissOnBackPress = true,
            dismissOnClickOutside = false
        ),
        onDismissRequest = {
            (context as Activity).finish()
            showDeviceTypeDialog.value = false
        },
        confirmButton = {
            TextButton(
                modifier = Modifier.padding(horizontal = 10.dp, vertical = 5.dp),
                colors = ButtonDefaults.textButtonColors(
                    backgroundColor = primaryColor
                ),
                elevation = ButtonDefaults.elevation(10.dp),
                onClick = {
                    SdkConstants.isMantraIris = false

                    if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.R) {
                        // Device is running Android 10 or below
                        if (ActivityCompat.checkSelfPermission(
                                context,
                                Manifest.permission.BLUETOOTH
                            )
                            != PackageManager.PERMISSION_GRANTED
                        ) {
                            // Permission is not granted, request it
                            ActivityCompat.requestPermissions(
                                context as Activity, permissions12, 1
                            )
                            return@TextButton
                        } else {
                            if (bluetoothAdapter != null) {
                                if (bluetoothAdapter.isEnabled) {
                                    showDeviceTypeDialog.value = false
                                    if (SdkConstants.Bluetoothname == "") {
                                        val intent =
                                            Intent(
                                                context,
                                                BluetoothConnectionActivity::class.java
                                            )
                                        (context as Activity).startActivityForResult(intent,201)
                                    }
                                } else {
                                    SdkConstants.Bluetoothname = ""
                                    val turnOn = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                                    activityResultLauncher.launch(turnOn)
                                }
                            }
                        }
                    } else {
                        if (ActivityCompat.checkSelfPermission(
                                context, Manifest.permission.BLUETOOTH_CONNECT
                            ) != PackageManager.PERMISSION_GRANTED
                        ) {
                            ActivityCompat.requestPermissions(
                                context as Activity, permissions12, 1
                            )
                            return@TextButton
                        } else {
                            if (bluetoothAdapter != null) {
                                if (bluetoothAdapter.isEnabled) {
                                    showDeviceTypeDialog.value = false
                                    if (SdkConstants.Bluetoothname == "") {
                                        val intent =
                                            Intent(
                                                context,
                                                BluetoothConnectionActivity::class.java
                                            )
                                        (context as Activity).startActivityForResult(intent,201)
                        //                                    context.finish()
                                    }
                                } else {
                                    SdkConstants.Bluetoothname = ""
                                    val turnOn = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                                    activityResultLauncher.launch(turnOn)
                                }
                            }
                        }
                    }
                }
            ) {
                Text(
                    fontSize = 12.sp,
                    text = "YES",
                    color = Color.White,
                    fontFamily = FontFamily(Font(R.font.poppins_regular))
                )
            }
        },
        dismissButton = {
            TextButton(
                modifier = Modifier.padding(start = 10.dp, top = 5.dp, bottom = 5.dp),
                colors = ButtonDefaults.textButtonColors(
                    backgroundColor = primaryColor
                ),
                elevation = ButtonDefaults.elevation(10.dp),
                onClick = {
                    SdkConstants.isMantraIris = false
                    SdkConstants.bluetoothConnector = false
                    SdkConstants.Bluetoothname = ""
                    showDeviceTypeDialog.value = false
                }
            ) {
                Text(
                    fontSize = 12.sp,
                    text = "USB DEVICE",
                    color = Color.White,
                    fontFamily = FontFamily(Font(R.font.poppins_regular))
                )
            }
        },
        title = {
            Text(modifier=Modifier.padding(top=20.dp, bottom = 15.dp),
                text = "Do you want to connect with bluetooth device?",
                color = Color.Black,
                fontFamily = FontFamily(
                    Font(R.font.poppins_regular)
                )
            )
        },
        backgroundColor = Color.White,
        shape = RoundedCornerShape(20.dp),
        contentColor = Color.White
    )
}

