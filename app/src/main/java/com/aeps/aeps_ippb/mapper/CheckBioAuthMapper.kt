package com.aeps.aeps_ippb.mapper

import com.aeps.aeps_ippb.common.Mapper
import com.aeps.aeps_ippb.data.remote.dto.request.SubmitBioAuthReqDto
import com.aeps.aeps_ippb.data.remote.dto.response.CheckBioAuthDto
import com.aeps.aeps_ippb.domain.model.request.SubmitBioAuthReqDomain
import com.aeps.aeps_ippb.domain.model.response.CheckBioAuthDomain

class CheckBioAuthMapper : Mapper<CheckBioAuthDto, CheckBioAuthDomain> {
    override fun mapToDto(domain: CheckBioAuthDomain): CheckBioAuthDto {
        return CheckBioAuthDto(
            statusDesc = domain.statusDesc,
            txnId = domain.txnId,
            status = domain.status
        )
    }

    override fun mapToDomain(dto: CheckBioAuthDto): CheckBioAuthDomain {
        return CheckBioAuthDomain(
            statusDesc = dto.statusDesc,
            txnId = dto.txnId,
            status = dto.status
        )
    }
}

class SubmitBioAuthReqMapper : Mapper<SubmitBioAuthReqDto, SubmitBioAuthReqDomain> {
    override fun mapToDto(domain: SubmitBioAuthReqDomain): SubmitBioAuthReqDto {
        return SubmitBioAuthReqDto(
            latLong = domain.latLong,
            pidData = domain.pidData
        )
    }

    override fun mapToDomain(dto: SubmitBioAuthReqDto): SubmitBioAuthReqDomain {
        return SubmitBioAuthReqDomain(
            latLong = dto.latLong,
            pidData = dto.pidData
        )
    }
}