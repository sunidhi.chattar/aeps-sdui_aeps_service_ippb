/*
 * MIT License
 * <p>
 * Copyright (c) 2017 Donato Rimenti
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.aeps.aeps_ippb.bluetooth

import android.Manifest
import android.app.ProgressDialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.ISUbiometric.biometricsdk.ListInteractionListener
import com.aeps.aeps_ippb.R
import com.aeps.aeps_ippb.common.SdkConstants
import com.aeps.aeps_ippb.driver_data.DriverActivity
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import java.util.Objects

/**
 * Main Activity of this application.
 *
 * @author Donato Rimenti
 */
class BluetoothConnectionActivity : AppCompatActivity(),
    ListInteractionListener<BluetoothDevice?> {
    /**
     * The controller for Bluetooth functionalities.
     */
    private lateinit var bluetooth: IsuBluetoothController

    /**
     * The Bluetooth discovery button.
     */
    private var fab: FloatingActionButton? = null

    /**
     * Progress dialog shown during the pairing process.
     */
    private var bondingProgressDialog: ProgressDialog? = null

    /**
     * Adapter for the recycler view.
     */
    lateinit var recyclerViewAdapter: DeviceRecyclerViewAdapter
    lateinit var recyclerView: RecyclerViewEmptySupport
    var device_toolbar: Toolbar? = null

    @RequiresApi(api = Build.VERSION_CODES.S)
    private val requiredPermissions = arrayOf(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.BLUETOOTH_SCAN,
        Manifest.permission.BLUETOOTH_CONNECT
    )

    /**
     * {@inheritDoc}
     */
    override fun onCreate(savedInstanceState: Bundle?) {

        // Changes the theme back from the splashscreen. It's very important that this is called
        // BEFORE onCreate.
        SystemClock.sleep(1500)
        /*setTheme(R.style.AppTheme_NoActionBar);*/super.onCreate(savedInstanceState)
        setContentView(R.layout.ekyc_device_bluetooth)
        device_toolbar = findViewById<View>(R.id.device_toolbar) as Toolbar
        setSupportActionBar(device_toolbar)
        setToolbar()

        // Sets up the RecyclerView.
        recyclerViewAdapter = DeviceRecyclerViewAdapter(this)
        recyclerView = findViewById<View>(R.id.device_list) as RecyclerViewEmptySupport
        recyclerView.layoutManager = LinearLayoutManager(this)

        // Sets the view to show when the dataset is empty. IMPORTANT : this method must be called
        // before recyclerView.setAdapter().
        val emptyView = findViewById<View>(R.id.device_empty_list)
        recyclerView.setEmptyView(emptyView)

        // Sets the view to show during progress.
        val progressBar = findViewById<View>(R.id.progressBar) as ProgressBar
        recyclerView.setProgressView(progressBar)
        recyclerView.adapter = recyclerViewAdapter


        // [#11] Ensures that the Bluetooth is available on this device before proceeding.
        val hasBluetooth = packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH)
        if (!hasBluetooth) {
            val dialog = AlertDialog.Builder(this@BluetoothConnectionActivity).create()
            dialog.setTitle(getString(R.string.bluetooth_not_available_title))
            dialog.setMessage(getString(R.string.bluetooth_not_available_message))
            dialog.setButton(
                AlertDialog.BUTTON_NEUTRAL, "OK"
            ) { dialog, which -> // Closes the dialog and terminates the activity.
                dialog.dismiss()
                finish()
            }
            dialog.setCancelable(false)
            dialog.show()
        }

        // Sets up the bluetooth controller.
        bluetooth =
            IsuBluetoothController(this, BluetoothAdapter.getDefaultAdapter(),
                recyclerViewAdapter!!
            )
        fab = findViewById<View>(R.id.fab) as FloatingActionButton
        fab!!.setOnClickListener { view ->
            // If the bluetooth is not enabled, turns it on.
            if (!bluetooth.isBluetoothEnabled) {
                Snackbar.make(
                    view,
                    R.string.enabling_bluetooth,
                    Snackbar.LENGTH_SHORT
                ).show()
                bluetooth.turnOnBluetoothAndScheduleDiscovery()
            } else {
                //Prevents the user from spamming the button and thus glitching the UI.
                if (!bluetooth.isDiscovering) {
                    // Starts the discovery.
                    Snackbar.make(
                        view,
                        R.string.device_discovery_started,
                        Snackbar.LENGTH_SHORT
                    ).show()
                    bluetooth.startDiscovery()
                } else {
                    Snackbar.make(
                        view,
                        R.string.device_discovery_stopped,
                        Snackbar.LENGTH_SHORT
                    ).show()
                    bluetooth.cancelDiscovery()
                }
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.BLUETOOTH_CONNECT
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                if (!bluetooth.isBluetoothEnabled) {
                    bluetooth.turnOnBluetoothAndScheduleDiscovery()
                } else {
                    //Prevents the user from spamming the button and thus glitching the UI.
                    if (!bluetooth.isDiscovering) {
                        // Starts the discovery.
                        bluetooth.startDiscovery()
                    } else {
                        bluetooth.cancelDiscovery()
                    }
                }
            } else {
                ActivityCompat.requestPermissions(this, requiredPermissions, 100)
            }
        } else {
            if (!bluetooth.isBluetoothEnabled) {
                bluetooth.turnOnBluetoothAndScheduleDiscovery()
            } else {
                //Prevents the user from spamming the button and thus glitching the UI.
                if (!bluetooth.isDiscovering) {
                    // Starts the discovery.
                    bluetooth.startDiscovery()
                } else {
                    bluetooth.cancelDiscovery()
                }
            }
        }
    }

    private fun setToolbar() {
        device_toolbar!!.title = "Select Device"
        device_toolbar!!.inflateMenu(R.menu.list_menu)
        device_toolbar!!.setOnMenuItemClickListener { item ->
            if (item.itemId == R.id.action_close) {
                // finish ();
                onBackPressed()
            }
            false
        }
    }




    override fun onItemClick(device: BluetoothDevice?) {
        try {
            device?.let {
                Log.v("Connector", "Item clicked : " + IsuBluetoothController.deviceToString(device))
                var name = Objects.requireNonNull(device.name)
                if (name != null) {
                    if (name.length > 9) {
                        name = name.substring(0, 9)
                    }
                    if (name.contains("BTprinter")) {
                        if (bluetooth?.isAlreadyPaired(device) == true) {
                            Log.d("Connector", "Device already paired!")
                            SdkConstants.selected_btdevice = device
                            SdkConstants.Bluetoothname = "BLUPRINT"
                            SdkConstants.bluetoothConnector = true
                            Toast.makeText(this, R.string.device_already_paired, Toast.LENGTH_SHORT)
                                .show()
                            finish()
                        } else {
                            Log.d("Connector", "Device not paired. Pairing.")
                            val outcome: Boolean? = bluetooth.pair(device)
                            // Prints a message to the user.
                            val deviceName: String? = IsuBluetoothController.getDeviceName(device)
                            if (outcome == true) {
                                // The pairing has started, shows a progress dialog.
                                Log.d("Connector", "Showing pairing dialog")
                                bondingProgressDialog = ProgressDialog.show(
                                    this, "",
                                    "Pairing with device $deviceName...", true, false
                                )
                            } else {
                                Log.d(
                                    "Connector",
                                    "Error while pairing with device $deviceName!"
                                )
                                Toast.makeText(
                                    this,
                                    "Error while pairing with device $deviceName!", Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    } else if (name.contains("ESI")) {
                        if (bluetooth.isAlreadyPaired(device)) {
                            Log.d("Connector", "Device already paired!")
                            SdkConstants.Bluetoothname = "EVOLUTE"
                            SdkConstants.selected_fingerPrint = device
                            SdkConstants.bluetoothConnector = true
                            Toast.makeText(this, R.string.device_already_paired, Toast.LENGTH_SHORT)
                                .show()
                            finish()
                        } else {
                            Log.d("Connector", "Device not paired. Pairing.")
                            val outcome: Boolean? = bluetooth?.pair(device)
                            // Prints a message to the user.
                            val deviceName: String? = IsuBluetoothController.getDeviceName(device)
                            if (outcome==true) {
                                // The pairing has started, shows a progress dialog.
                                Log.d("Connector", "Showing pairing dialog")
                                SdkConstants.Bluetoothname = "EVOLUTE"
                                SdkConstants.selected_fingerPrint = device
                                SdkConstants.bluetoothConnector = true
                                bondingProgressDialog = ProgressDialog.show(
                                    this, "",
                                    "Pairing with device $deviceName...", true, false
                                )
                            } else {
                                SdkConstants.Bluetoothname = ""
                                SdkConstants.selected_fingerPrint = device
                                SdkConstants.bluetoothConnector = false
                                Log.d(
                                    "Connector",
                                    "Error while pairing with device $deviceName!"
                                )
                                Toast.makeText(
                                    this,
                                    "Error while pairing with device $deviceName!", Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    } else if (name.contains("BPFS")) {
                        if (bluetooth.isAlreadyPaired(device)) {
                            Log.d("Connector", "Device already paired!")
                            SdkConstants.selected_fingerPrint = device
                            SdkConstants.Bluetoothname = "BLUPRINT"
                            SdkConstants.bluetoothConnector = true
                            Toast.makeText(this, R.string.device_already_paired, Toast.LENGTH_SHORT)
                                .show()
                            val intent = Intent(
                                this@BluetoothConnectionActivity,
                                DriverActivity::class.java
                            )
                            startActivity(intent)
                            finish()
                        } else {
                            Log.d("Connector", "Device not paired. Pairing.")
                            val outcome: Boolean = bluetooth.pair(device)
                            // Prints a message to the user.
                            val deviceName: String? = IsuBluetoothController.getDeviceName(device)
                            if (outcome) {
                                // The pairing has started, shows a progress dialog.
                                Log.d("Connector", "Showing pairing dialog")
                                bondingProgressDialog = ProgressDialog.show(
                                    this, "",
                                    "Pairing with device $deviceName...", true, false
                                )
                            } else {
                                Log.d(
                                    "Connector",
                                    "Error while pairing with device $deviceName!"
                                )
                                Toast.makeText(
                                    this,
                                    "Error while pairing with device $deviceName!", Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    } else if (name.contains("BT")) {
                        if (bluetooth.isAlreadyPaired(device)) {
                            Log.d("Connector", "Device already paired!")
                            SdkConstants.selected_fingerPrint = device
                            SdkConstants.Bluetoothname = "BLUPRINT"
                            SdkConstants.bluetoothConnector = true
                            Toast.makeText(this, R.string.device_already_paired, Toast.LENGTH_SHORT)
                                .show()
                            finish()
                        } else {
                            Log.d("Connector", "Device not paired. Pairing.")
                            val outcome: Boolean = bluetooth.pair(device)
                            // Prints a message to the user.
                            val deviceName: String? = IsuBluetoothController.getDeviceName(device)
                            if (outcome) {
                                // The pairing has started, shows a progress dialog.
                                Log.d("Connector", "Showing pairing dialog")
                                bondingProgressDialog = ProgressDialog.show(
                                    this, "",
                                    "Pairing with device $deviceName...", true, false
                                )
                            } else {
                                Log.d(
                                    "Connector",
                                    "Error while pairing with device $deviceName!"
                                )
                                Toast.makeText(
                                    this,
                                    "Error while pairing with device $deviceName!", Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    } else {
                        Toast.makeText(
                            this,
                            "Please select the required Bluetooth Device",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } else {
                    Toast.makeText(this, "Selected device name not found !", Toast.LENGTH_SHORT).show()
                }
            }
        } catch (e: Exception) {
        }
    }

    override fun startLoading() {
        recyclerView.startLoading()

        // Changes the button icon.
        fab!!.setImageResource(R.drawable.ic_bluetooth_white_24dp)
    }

    /**
     * {@inheritDoc}
     */
    override fun endLoading(partialResults: Boolean) {
        recyclerView?.endLoading()

        // If discovery has ended, changes the button icon.
        if (!partialResults) {
            fab!!.setImageResource(R.drawable.ic_bluetooth_white_24dp)
        }
    }

    /**
     * {@inheritDoc}
     */
    override fun endLoadingWithDialog(error: Boolean, device: BluetoothDevice?) {
        if (bondingProgressDialog != null) {
            val view = findViewById<View>(R.id.main_content)
            val message: String
            val deviceName: String? = IsuBluetoothController.getDeviceName(device)

            // Gets the message to print.
            message = if (error) {
                "Failed pairing with device $deviceName!"
            } else {
                //                Constants.selected_btdevice = device;
                //                Constants.selected_fingerPrint = device;
                "Succesfully paired with device $deviceName!"
            }

            // Dismisses the progress dialog and prints a message to the user.
            bondingProgressDialog!!.dismiss()
            Snackbar.make(view, message, Snackbar.LENGTH_SHORT).show()

            // Cleans up state.
            bondingProgressDialog = null
            finish()
        }
    }

    /**
     * {@inheritDoc}
     */
    override fun onDestroy() {
        bluetooth.close()
        super.onDestroy()
    }

    /**
     * {@inheritDoc}
     */
    override fun onRestart() {
        super.onRestart()
        // Stops the discovery.
        bluetooth?.cancelDiscovery()
        // Cleans the view.
        recyclerViewAdapter?.cleanView()
    }

    /**
     * {@inheritDoc}
     */
    override fun onStop() {
        super.onStop()
        // Stoops the discovery.
        bluetooth?.cancelDiscovery()
    }

    companion object {
        var selected_btdevice: BluetoothDevice? = null
    }
}