package com.aeps.aeps_ippb.data.repository

import com.aeps.aeps_ippb.common.NetworkResource
import com.aeps.aeps_ippb.common.handleFlowResponse
import com.aeps.aeps_ippb.data.remote.ApiServices
import com.aeps.aeps_ippb.data.remote.PostApiService
import com.aeps.aeps_ippb.data.remote.dto.request.FetchUiDataReq
import com.aeps.aeps_ippb.data.remote.dto.response.ui_data.FetchUiData
import com.aeps.aeps_ippb.domain.model.Handler
import com.aeps.aeps_ippb.domain.model.request.SubmitBioAuthReqDomain
import com.aeps.aeps_ippb.domain.model.request.TransferRequestDomain
import com.aeps.aeps_ippb.domain.model.response.CheckBioAuthDomain
import com.aeps.aeps_ippb.domain.model.response.TransferResponseDomain
import com.aeps.aeps_ippb.domain.repository.Repository
import com.aeps.aeps_ippb.mapper.CheckBioAuthMapper
import com.aeps.aeps_ippb.mapper.FundTransferRequestMapper
import com.aeps.aeps_ippb.mapper.SubmitBioAuthReqMapper
import com.iserveu.inappdistribution.network.APIInterface
import kotlinx.coroutines.flow.Flow


class RepositoryImp constructor(
    private val apiInterface: ApiServices,
    private val ktorApiServices: PostApiService
) : Repository {


    override fun transfer(
        endPoint: String, token: String, body: TransferRequestDomain
    ): Flow<NetworkResource<TransferResponseDomain>> {
        return Handler.handleResponse {
            ktorApiServices.getTransfer(
                endPoint, token = token, FundTransferRequestMapper().mapToDto(body)
            )
        }
    }

    override fun fetchUIData(adminName: String): Flow<NetworkResource<FetchUiData>> {
        val fetchUiDataReq = FetchUiDataReq(userName = adminName)
        return Handler.handleResponse { ktorApiServices.getFetchuidata(fetchUiDataReq) }
    }

    override fun submitBioAuth(
        url: String, submitBioAuthReqDomain: SubmitBioAuthReqDomain, token: String
    ): Flow<NetworkResource<CheckBioAuthDomain>> {
        return Handler.handleResponse {
            ktorApiServices.getsubmitbioauthstatus(
                url, SubmitBioAuthReqMapper().mapToDto(submitBioAuthReqDomain), token = token
            )
        }
    }

    override fun checkBioAuthStatus(
        url: String, token: String
    ): Flow<NetworkResource<CheckBioAuthDomain>> {
        return Handler.handleResponse { ktorApiServices.getcheckbioauthstatus(url, token = token) }
    }
}

