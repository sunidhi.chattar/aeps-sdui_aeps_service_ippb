package com.aeps.aeps_ippb.data.remote.dto.request

import com.google.gson.annotations.SerializedName

data class SubmitBioAuthReqDto(

	@field:SerializedName("latLong")
	val latLong: String? = null,

	@field:SerializedName("pidData")
	val pidData: String? = null
)
