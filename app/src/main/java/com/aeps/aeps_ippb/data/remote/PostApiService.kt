package com.aeps.aeps_ippb.data.remote

import com.aeps.aeps_ippb.data.remote.dto.request.FetchUiDataReq
import com.aeps.aeps_ippb.data.remote.dto.request.SubmitBioAuthReqDto
import com.aeps.aeps_ippb.data.remote.dto.request.TransferRequestDto
import com.aeps.aeps_ippb.data.remote.dto.response.TransferResponseDto
import io.ktor.client.HttpClient
import io.ktor.client.engine.android.Android
import io.ktor.client.plugins.logging.LogLevel
import io.ktor.client.plugins.logging.Logging
import io.ktor.client.statement.HttpResponse

interface PostApiService {

    suspend fun getTransfer(
        endPoint: String,
        token: String,
        mapToDto: TransferRequestDto
    ): HttpResponse

    suspend fun getFetchuidata(
        req: FetchUiDataReq
    ): HttpResponse

    suspend fun getcheckbioauthstatus(
        url: String,
        token: String
    ): HttpResponse

    suspend fun getsubmitbioauthstatus(
        url: String,
        submitBioAuthReqDto: SubmitBioAuthReqDto,
        token: String
    ): HttpResponse

}