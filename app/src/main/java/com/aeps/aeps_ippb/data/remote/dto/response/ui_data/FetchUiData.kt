package com.aeps.aeps_ippb.data.remote.dto.response.ui_data

import com.google.gson.annotations.SerializedName

data class FetchUiData(

    @field:SerializedName("data")
    val data: Data? = null,

    @field:SerializedName("status")
    val status: Int? = null
)

data class TransactionScreen(

    @field:SerializedName("Column")
    val column: Column? = null
)
data class FingerCaptureScreen(

    @field:SerializedName("CheckBox")
    val checkBox: CheckBox? = null,
    @field:SerializedName("FingerPrint_Image")
    val fingerPrintImage: FingerPrintImage? = null,

)

data class AadharField(

    @field:SerializedName("Hints")
    val hints: String? = null,

    @field:SerializedName("Spacer")
    val spacer: Spacer? = null,

    @field:SerializedName("Icon")
    val icon: String? = null,

    @field:SerializedName("Error_Message2")
    val errorMessage2: String? = null,

    @field:SerializedName("Max_Digit")
    val maxDigit: Int? = null,

    @field:SerializedName("Error_Message1")
    val errorMessage1: String? = null
)

data class RetryText(

    @field:SerializedName("Text")
    val text: Text? = null
)


data class Surface(

    @field:SerializedName("Modifier")
    val modifier: Modifier? = null
)

data class TransactionIdText(

    @field:SerializedName("Text")
    val text: Text? = null
)

data class Data(

    @field:SerializedName("ui_json")
    val uiJson: UiJson? = null,

    @field:SerializedName("version")
    val version: String? = null
)

data class DownloadPdfText(

    @field:SerializedName("Text")
    val text: Text? = null
)
data class Box(

    @field:SerializedName("Column")
    val column: Column? = null
)
data class Screens(

    @field:SerializedName("Transaction_Screen")
    val transactionScreen: TransactionScreen? = null,

    @field:SerializedName("FingerCapture_Screen")
    val fingerCaptureScreen: FingerCaptureScreen? = null
)

data class DriverData(

    @field:SerializedName("fType")
    val fType: String? = null,

    @field:SerializedName("timeout")
    val timeout: String? = null,

    @field:SerializedName("env")
    val env: String? = null,

    @field:SerializedName("wadh")
    val wadh: String? = null

)

data class Text(

    @field:SerializedName("Value")
    val value: String? = null,

    @field:SerializedName("Font_Size")
    val fontSize: Int? = null,

    @field:SerializedName("Modifier")
    val modifier: Modifier? = null,

    @field:SerializedName("Spacer")
    val spacer: Spacer? = null,

    @field:SerializedName("Color")
    val color: String? = null,

)

data class CardText(

    @field:SerializedName("Text")
    val text: Text? = null
)

data class FingerPrintImage(

    @field:SerializedName("Spacer")
    val spacer: Spacer? = null,

    @field:SerializedName("Image")
    val image: Image? = null
)

data class Column(

    @field:SerializedName("Button_Column")
    val buttonColumn: ButtonColumn? = null,

    @field:SerializedName("Column")
    val column: Column? = null,

    @field:SerializedName("Spacer")
    val spacer: Spacer? = null,

    @field:SerializedName("Status_Icon")
    val statusIcon: StatusIcon? = null,

    @field:SerializedName("Card_Text")
    val cardText: CardText? = null,

    @field:SerializedName("Balance_Text")
    val balanceText: BalanceText? = null,

    @field:SerializedName("CreatedDate_Text")
    val createdDateText: CreatedDateText? = null,

    @field:SerializedName("TransactionId_Text")
    val transactionIdText: TransactionIdText? = null,

    @field:SerializedName("BankName_Text")
    val bankNameText: BankNameText? = null,

    @field:SerializedName("View_TxnDetails_Text")
    val viewTxnDetailsText: ViewTxnDetailsText? = null,

    @field:SerializedName("Modifier")
    val modifier: Modifier? = null,

    @field:SerializedName("Button")
    val button: Button? = null,

    @field:SerializedName("Surface")
    val surface: Surface? = null,

    @field:SerializedName("Text")
    val text: Text? = null,

    @field:SerializedName("Image")
    val image: Image? = null,

    @field:SerializedName("FingerPrint_Image")
    val fingerPrintImage: FingerPrintImage? = null,
    @field:SerializedName("CheckBox")
    val checkBox: CheckBox? = null
)

data class CheckBox(
    @field:SerializedName("Text")
    val text: Text? = null
)

data class Modifier(

    @field:SerializedName("Height")
    val height: Int? = null,

    @field:SerializedName("Padding")
    val padding: Padding? = null,

    @field:SerializedName("Width")
    val width: Int? = null,

    @field:SerializedName("Elevation")
    val elevation: Int? = null,

    @field:SerializedName("RoundedCornerShape")
    val roundedCornerShape: RoundedCornerShape? = null,

    @field:SerializedName("Text")
    val text: Text? = null
)

data class PrintText(

    @field:SerializedName("Text")
    val text: Text? = null
)

data class CreatedDateText(

    @field:SerializedName("Text")
    val text: Text? = null
)
data class RoundedCornerShape(

    @field:SerializedName("Bottom_Start")
    val bottomStart: Int? = null,

    @field:SerializedName("Top_End")
    val topEnd: Int? = null,

    @field:SerializedName("Bottom_End")
    val bottomEnd: Int? = null,

    @field:SerializedName("Top_Start")
    val topStart: Int? = null
)

data class BankNameText(

    @field:SerializedName("Text")
    val text: Text? = null
)

data class ButtonColumn(

    @field:SerializedName("Button")
    val button: Button? = null,

    @field:SerializedName("Spacer")
    val spacer: Spacer? = null
)

data class UiJson(

    @field:SerializedName("baseUrl")
    val baseUrl: BaseUrl? = null,

    @field:SerializedName("App_Color")
    val appColor: AppColor? = null,

    @field:SerializedName("Screens")
    val screens: Screens? = null,

    @field:SerializedName("Driver_Data")
    val driverDate: DriverData? = null
)

data class Button(

    @field:SerializedName("Download_Pdf_Text")
    val downloadPdfText: DownloadPdfText? = null,

    @field:SerializedName("Button_Spacer")
    val buttonSpacer: ButtonSpacer? = null,

    @field:SerializedName("Shape")
    val shape: Shape? = null,

    @field:SerializedName("Print_Text")
    val printText: PrintText? = null,

    @field:SerializedName("Retry_Text")
    val retryText: RetryText? = null,

    @field:SerializedName("Spacer")
    val spacer: Spacer? = null,

    @field:SerializedName("Modifier")
    val modifier: Modifier? = null,

    @field:SerializedName("Close_Button_Text")
    val closeButtonText: CloseButtonText? = null,

    @field:SerializedName("Text")
    val text: Text? = null
)

data class Image(

    @field:SerializedName("Failed_Icon_URL")
    val failedIconURL: String? = null,
    @field:SerializedName("brandLogoURL")
    val brandLogoURL: String? = null,

    @field:SerializedName("Spacer")
    val spacer: Spacer? = null,

    @field:SerializedName("Modifier")
    val modifier: Modifier? = null,

    @field:SerializedName("Success_Icon_URL")
    val successIconURL: String? = null,
    @field:SerializedName("Pending_Icon_URL")
    val pendingIconUrl: String? = null,
    @field:SerializedName("Content_Description")
    val contentDescription: String? = null,

    @field:SerializedName("Image_Url")
    val imageUrl: String? = null
)

data class StatusIcon(

    @field:SerializedName("Image")
    val image: Image? = null
)


data class ButtonSpacer(

    @field:SerializedName("Modifier")
    val modifier: Modifier? = null
)

data class Shape(

    @field:SerializedName("RoundedCornerShape")
    val roundedCornerShape: RoundedCornerShape? = null
)

data class Spacer(

    @field:SerializedName("Modifier")
    val modifier: Modifier? = null
)


data class BalanceText(

    @field:SerializedName("Text")
    val text: Text? = null
)

data class BaseUrl(

    @field:SerializedName("bioAuthBaseUrl")
    val bioAuthBaseUrl: String? = null,

    @field:SerializedName("baseurlSdkUser")
    val baseurlSdkUser: String? = null,

    @field:SerializedName("unifiedAepsBaseUrl")
    val unifiedAepsBaseUrl: String? = null,

    @field:SerializedName("addressBaseUrl")
    val addressBaseUrl: String? = null,

    @field:SerializedName("aadhaarPayBaseUrl")
    val aadhaarPayBaseUrl: String? = null,
    @field:SerializedName("transactionEncodedUrlCoreBE")
    val transactionEncodedUrlCoreBE: String? = null,
    @field:SerializedName("transactionEncodedUrlCoreMS")
    val transactionEncodedUrlCoreMS: String? = null,
    @field:SerializedName("transactionEncodedUrlCoreCW")
    val transactionEncodedUrlCoreCW: String? = null,
    @field:SerializedName("transactionEncodedUrlCoreAP")
    val transactionEncodedUrlCoreAP: String? = null,
    @field:SerializedName("transactionEncodedUrlCoreCD")
    val transactionEncodedUrlCoreCD: String? = null,
    @field:SerializedName("checkBioAuthUrl")
    val checkBioAuthUrl: String? = null,
    @field:SerializedName("submitBioAuthUrl")
    val submitBioAuthUrl: String? = null
)


data class AppColor(

    @field:SerializedName("Primary_Color")
    val primaryColor: String? = null,

    @field:SerializedName("FingerGrey")
    val fingerGrey: String? = null,

    @field:SerializedName("MsTextColor")
    val msTextColor: String? = null,

    @field:SerializedName("TxnTextColor")
    val txnTextColor: String? = null,

    @field:SerializedName("TxnBkgbkgblue")
    val txnBkgbkgblue: String? = null,

    @field:SerializedName("TxnBkglightblue")
    val txnBkglightblue: String? = null,

    @field:SerializedName("TxnBkgtextColor")
    val txnBkgtextColor: String? = null
)

data class Padding(

    @field:SerializedName("Start")
    val start: Int? = null,

    @field:SerializedName("End")
    val end: Int? = null,

    @field:SerializedName("Top")
    val top: Int? = null,

    @field:SerializedName("Bottom")
    val bottom: Int? = null
)

data class CloseButtonText(

    @field:SerializedName("Text")
    val text: Text? = null
)

data class ViewTxnDetailsText(

    @field:SerializedName("Text")
    val text: Text? = null
)
