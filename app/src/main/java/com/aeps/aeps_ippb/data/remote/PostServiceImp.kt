package com.aeps.aeps_ippb.data.remote

import com.aeps.aeps_ippb.data.remote.dto.request.FetchUiDataReq
import com.aeps.aeps_ippb.data.remote.dto.request.SubmitBioAuthReqDto
import com.aeps.aeps_ippb.data.remote.dto.request.TransferRequestDto
import com.aeps.aeps_ippb.data.remote.dto.response.CheckBioAuthDto
import com.aeps.aeps_ippb.data.remote.dto.response.TransferResponseDto
import io.ktor.client.HttpClient
import io.ktor.client.request.header
import io.ktor.client.request.post
import io.ktor.client.request.setBody
import io.ktor.client.statement.HttpResponse
import io.ktor.http.ContentType
import io.ktor.http.HttpHeaders
import io.ktor.http.contentType

class PostServiceImp(private val client: HttpClient) : PostApiService {

    override suspend fun getTransfer(
        endPoint: String,
        token: String,
        mapToDto: TransferRequestDto
    ): HttpResponse {
        return client.post(endPoint) {
            header(HttpHeaders.Authorization, token)
            setBody(mapToDto)
            contentType(ContentType.Application.Json)
        }
    }

    override suspend fun getFetchuidata(req: FetchUiDataReq): HttpResponse {
        return client.post(urlString = "https://sdui-module-prod.iserveu.tech/aeps/fetchData") {
            setBody(req)
            contentType(ContentType.Application.Json)
        }
    }

    override suspend fun getcheckbioauthstatus(url: String, token: String): HttpResponse {
        return client.post(url) {
            header(HttpHeaders.Authorization, token)
            contentType(ContentType.Application.Json)
        }
    }

    override suspend fun getsubmitbioauthstatus(
        url: String,
        submitBioAuthReqDto: SubmitBioAuthReqDto,
        token: String
    ): HttpResponse {
        return client.post(url) {
            setBody(submitBioAuthReqDto)
            header(HttpHeaders.Authorization, token)
            contentType(ContentType.Application.Json)
        }
    }
}