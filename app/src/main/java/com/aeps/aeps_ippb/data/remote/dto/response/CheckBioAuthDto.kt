package com.aeps.aeps_ippb.data.remote.dto.response

import com.google.gson.annotations.SerializedName

data class CheckBioAuthDto(

	@field:SerializedName("statusDesc")
	val statusDesc: String? = null,

	@field:SerializedName("txnId")
	val txnId: Any? = null,

	@field:SerializedName("status")
	val status: Int? = null
)
