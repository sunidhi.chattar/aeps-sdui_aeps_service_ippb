package com.aeps.aeps_ippb.data.remote

import com.aeps.aeps_ippb.data.remote.dto.request.*
import com.aeps.aeps_ippb.data.remote.dto.response.*
import com.aeps.aeps_ippb.data.remote.dto.response.ui_data.FetchUiData
import retrofit2.Response
import retrofit2.http.*

interface ApiServices {

    @POST
    suspend fun transfer(
        @Url url: String,
        @Header("Authorization") token: String,
        @Body dto: TransferRequestDto
    ): Response<TransferResponseDto>

    @POST
    suspend fun fetchUiData(
        @Url url: String = "https://sdui-module-prod.iserveu.tech/aeps/fetchData",
        @Body fetchUiDataReq: FetchUiDataReq = FetchUiDataReq()
    ): Response<FetchUiData>
    @GET()
    suspend fun checkBioAuthStatus(
        @Url url: String,
        @Header("Authorization") token: String
    ): Response<CheckBioAuthDto>

    @POST()
    suspend fun submitBioAuthStatus(
        @Url url: String,
        @Body submitBioAuthReqDto: SubmitBioAuthReqDto,
        @Header("Authorization") token: String
    ): Response<CheckBioAuthDto>

}