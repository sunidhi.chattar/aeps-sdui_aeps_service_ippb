package com.aeps.aeps_ippb.domain.usecase

import com.aeps.aeps_ippb.domain.usecase.check_bioauth_usecase.CheckBioAuthUseCase
import com.aeps.aeps_ippb.domain.usecase.fetchui_usecase.FetchUiUseCase
import com.aeps.aeps_ippb.domain.usecase.submit_bioauth_usecase.SubmitBioAuthUseCase
import com.aeps.aeps_ippb.domain.usecase.transfer.TransferUseCase

data class UseCases(
    val transferUseCase: TransferUseCase,
    val fetchUiUseCase: FetchUiUseCase,
    val submitBioAuthUseCase: SubmitBioAuthUseCase,
    val checkBioAuthUseCase: CheckBioAuthUseCase
)