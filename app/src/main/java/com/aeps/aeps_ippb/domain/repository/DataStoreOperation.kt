package com.aeps.aeps_ippb.domain.repository

import kotlinx.coroutines.flow.Flow

interface DataStoreOperation {

    suspend fun updateUiVersion(version:String)
    fun checkVersion():Flow<String?>
}