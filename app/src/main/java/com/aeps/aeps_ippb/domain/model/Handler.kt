package com.aeps.aeps_ippb.domain.model

import com.aeps.aeps_ippb.common.NetworkResource
import io.ktor.client.call.body
import io.ktor.client.network.sockets.ConnectTimeoutException
import io.ktor.client.network.sockets.SocketTimeoutException
import io.ktor.client.plugins.ClientRequestException
import io.ktor.client.plugins.RedirectResponseException
import io.ktor.client.plugins.ResponseException
import io.ktor.client.plugins.ServerResponseException
import io.ktor.client.statement.HttpResponse
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.io.IOException

object Handler{
    inline fun <reified T> handleResponse(crossinline call: suspend () -> HttpResponse): Flow<NetworkResource<T>> {
        return flow {
            emit(NetworkResource.Loading(isLoading = true))
            try {
                val response = call.invoke().body<T>()

            emit(NetworkResource.Success(response))
            emit(NetworkResource.Loading(isLoading = false))

        } catch (e: ClientRequestException) {

            emit(NetworkResource.Error(e.response.status.description))

        } catch (e: ServerResponseException) {


            emit(NetworkResource.Error(e.response.status.description))
        } catch (e: RedirectResponseException) {

            emit(NetworkResource.Error(e.response.status.description))
        } catch (e: ResponseException) {
            emit(NetworkResource.Error(e.response.status.description))
        } catch (e: ConnectTimeoutException) {

            emit(NetworkResource.Error("Connection Timeout"))
        } catch (e: SocketTimeoutException) {

            emit(NetworkResource.Error("Socket Timeout"))
        } catch (e: IOException) {

            emit(NetworkResource.Error(e.message ?: "Unknown IO Error"))
        } catch (e: Exception) {

            emit(NetworkResource.Error(e.message ?: "Unknown Error"))
        }
        emit(NetworkResource.Loading(isLoading = false))
    }
}
}