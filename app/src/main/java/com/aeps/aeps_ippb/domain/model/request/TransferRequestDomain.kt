package com.aeps.aeps_ippb.domain.model.request

data class TransferRequestDomain(
    val amount: Int? = null,
    val latLong: String? = null,
    val mobileNumber: String? = null,
    val bankName: String? = null,
    val aadharNo: String? = null,
    val pidData: String? = null,
    val apiUser: String? = null,
    val iin: String? = null,
    val ipAddress: String? = null,
    val shakey: String? = null,
    var paramA: String? = null,
    var paramB: String? = null,
    var paramC: String? = null,
    val retailer: String? = null,
    val gatewayPriority: Int? = null,
    val apiUserName: String? = null,
    val isSL: Boolean? = null
)
