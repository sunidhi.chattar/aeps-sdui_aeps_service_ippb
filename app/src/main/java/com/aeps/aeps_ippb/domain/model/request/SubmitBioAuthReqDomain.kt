package com.aeps.aeps_ippb.domain.model.request


data class SubmitBioAuthReqDomain(
	val latLong: String? = null,
	val pidData: String? = null
)
