package com.aeps.aeps_ippb.driver_data

interface ConnectionLostCallback {

    fun connectionLost()
}