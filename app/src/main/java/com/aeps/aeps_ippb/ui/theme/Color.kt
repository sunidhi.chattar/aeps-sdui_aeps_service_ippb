package com.aeps.aeps_ippb.ui.theme

import androidx.compose.ui.graphics.Color

var primaryColor = Color(0xFF1FB7DB)
var Red = Color(0xFFff000d)
var Grey=Color(0x80CCC9C7)
var txnBkgtextColor = Color(0xFF373636)
var txnBkglightblue = Color(0xFF00c3d7)
var txnBkgbkgblue = Color(0xFF84E5EF)
var msTextColor = Color(0xFF454545)
var txnTextColor = Color(0xFF373636)
var fingerGrey = Color(0xFF707070)
val very_light_grey = Color(0xFFD3D3D3)
val ProgressBarGreen = Color(0xFF07b553)
val redColor = Color(0xFFed1b24)
val greenColor = Color(0xFF2bc48a)
val yellowColor = Color(0xFFf2d516)

val HeadingColor = Color(0xFF242236)
val ConcentTextColor = Color(0xFF6F6F6F)
