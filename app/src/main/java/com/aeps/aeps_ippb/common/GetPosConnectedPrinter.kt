package com.aeps.aeps_ippb.common

import com.isu.printer_library.vriddhi.AEMPrinter


class GetPosConnectedPrinter(aemPrinter: AEMPrinter) {
    private var aemPrinter: AEMPrinter
        get() = Companion.aemPrinter!!

    companion object {
         var aemPrinter: AEMPrinter? = null
    }

    init {
        this.aemPrinter = aemPrinter
    }
}