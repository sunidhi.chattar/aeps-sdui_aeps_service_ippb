package com.aeps.aeps_ippb.common

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.provider.Settings
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.window.DialogProperties
import com.aeps.aeps_ippb.common.locationlocation.GpsTracker
import com.aeps.aeps_ippb.presentation.viewmodel.UnifiedAepsViewModel

@Composable
fun GetLocation(context: Context, viewModel: UnifiedAepsViewModel) {
    val gpsTracker = GpsTracker(context)
    if (gpsTracker.canGetLocation()) {
        val latitude = gpsTracker.getLatitude()
        val longitude = gpsTracker.getLongitude()
        viewModel.latLong.value = "$latitude,$longitude"
    } else {
        viewModel.showLocationDialog.value = true
        ShowAlertDialog(viewModel)
    }
    SaveLocation(viewModel)
}

@Composable
private fun SaveLocation(viewModel: UnifiedAepsViewModel) {
    val context = LocalContext.current
    var lastLatlong: String? = null
    val sharedPreferences: SharedPreferences =
        context.getSharedPreferences("LastLatlong", Context.MODE_PRIVATE)
    val editor = sharedPreferences.edit()
    if (viewModel.latLong.value.isNotEmpty()) {
        lastLatlong = viewModel.latLong.value
    }
    editor.putString(lastLatlong, viewModel.latLong.value)
    editor.apply()
}

@Composable
fun ShowAlertDialog(viewModel: UnifiedAepsViewModel) {
    val context = LocalContext.current
    val showDialog = viewModel.showLocationDialog.value
    if (showDialog) {
        AlertDialog(properties = DialogProperties(
            dismissOnBackPress = false,
            dismissOnClickOutside = false,
        ),
            onDismissRequest = {
                viewModel.showLocationDialog.value = false
            },
            confirmButton = {
                TextButton(onClick = {
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    context.startActivity(intent)
                    (context as Activity).finish()
                    viewModel.showLocationDialog.value = false
                    viewModel.askForLocationPermission.value = false
                }) { Text(text = "Settings") }
            },
            dismissButton = {
                TextButton(onClick = {
                    viewModel.showLocationDialog.value = false
                    (context as Activity).finish()
                }) { Text(text = "Cancel") }
            },
            title = { Text(text = "GPS is settings") },
            text = { Text(text = "GPS is not enabled. Do you want to go to settings menu?") })
    }
}



