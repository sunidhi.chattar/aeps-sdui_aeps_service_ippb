package com.aeps.aeps_ippb.common

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Typeface
import android.graphics.pdf.PdfDocument
import android.graphics.pdf.PdfDocument.PageInfo
import android.net.Uri
import android.os.Environment
import android.widget.Toast
import androidx.compose.ui.graphics.toArgb
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.graphics.alpha
import com.aeps.aeps_ippb.domain.model.response.TransferResponseDomain
import com.aeps.aeps_ippb.ui.theme.yellowColor
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


fun createPdf(
    context: Context,
    msList: List<TransferResponseDomain.MiniStatementDomain?>?,
    printMiniStatement: Boolean? = false,
    transactionAmount: String,
    status: String,
    createdDate: String,
    transactionMode: String,
    transactionID: String,
    aadhaarNo: String,
    bankName: String,
    referenceNo: String,
    balanceAmount: String
) {
    val balanceAmt=if(balanceAmount.isNotEmpty() && balanceAmount != "N/A"){
        "Rs. $balanceAmount"
    }else{
        balanceAmount.ifEmpty { "N/A" }
    }
    val pageHeight = 1120
    val pageWidth = 792
    val pdfDocument = PdfDocument()
    val faildStatus = Paint()
    val refundStatus = Paint()
    val keys = Paint()
    val value = Paint()
    val amount = Paint()
    val centerData = Paint()
    val rightData = Paint()
    val successStatus = Paint()
    val txnDetails = Paint()
    val pageInfo = PageInfo.Builder(pageWidth, pageHeight, 1).create()
    val myPage = pdfDocument.startPage(pageInfo)
    //it is used for design pdf
    val canvas: Canvas = myPage.canvas
    /* below line is used for adding typeface for
         our text which we will be adding in our PDF file.*/
    faildStatus.typeface = Typeface.create(
        Typeface.DEFAULT, Typeface.BOLD
    )
    refundStatus.typeface= Typeface.create(
        Typeface.DEFAULT, Typeface.BOLD
    )
    refundStatus.textSize = 40f
    refundStatus.color = yellowColor.toArgb()
    refundStatus.textAlign = Paint.Align.CENTER
    keys.typeface = Typeface.create(Typeface.DEFAULT, Typeface.BOLD)
    value.typeface = Typeface.create(Typeface.DEFAULT, Typeface.NORMAL)
    amount.typeface = Typeface.create(Typeface.DEFAULT, Typeface.BOLD)
    successStatus.typeface = Typeface.create(Typeface.DEFAULT, Typeface.BOLD)
    txnDetails.typeface = Typeface.create(Typeface.DEFAULT, Typeface.BOLD)
    /* below line is used for setting text size
         which we will be displaying in our PDF file.*/
    faildStatus.textSize = 40f
    faildStatus.color = Color.RED
    faildStatus.textAlign = Paint.Align.CENTER
    keys.textSize = 26f
    keys.textAlign = Paint.Align.LEFT
    successStatus.textSize = 40f
    successStatus.color = Color.GREEN
    successStatus.textAlign = Paint.Align.CENTER
    centerData.textSize = 36f
    centerData.textAlign = Paint.Align.CENTER
    rightData.textSize = 26f
    rightData.textAlign = Paint.Align.RIGHT
    canvas.drawText(SdkConstants.SHOP_NAME, 400f, 100f, centerData)
    canvas.drawText("Receipt", 400f, 150f, centerData)
    if (status == "FAILED" || status=="-1") {
        canvas.drawText("FAILED", 400f, 200f, faildStatus)
    } else if(status == "REFUNDED"){
        canvas.drawText("REFUNDED", 400f, 200f, refundStatus)
    }else if(status == "SUCCESS"){
        canvas.drawText("SUCCESS", 400f, 200f, successStatus)
    }else {
        status.let { canvas.drawText(it, 400f, 200f, refundStatus) }
    }
    canvas.drawText("Date/Time : ${createdDate.ifEmpty { "N/A" }}", 50f, 280f, keys)
    canvas.drawText("Operation Performed : ${transactionMode.ifEmpty { "N/A" }}", 50f, 330f, keys)
    canvas.drawText("Transaction Details", 400f, 400f, centerData)
    val rightX = 50f
    val leftX = 700f
    var startY = 450f
    if (printMiniStatement == true) {
        msList?.forEach { d ->
            if (d != null) {
                canvas.drawText("${d.txnDesc}", rightX, startY, keys)
                canvas.drawText("${d.txntype}", rightX, startY + 25f, keys)
                canvas.drawText("${d.amount}", leftX, startY, rightData)
                canvas.drawText("${d.date}", leftX, startY + 25f, rightData)
                startY += 55f
            }
        }
    } else {
        canvas.drawText("Transaction ID: ${transactionID.ifEmpty { "N/A" }}", rightX, startY + 30f, keys)
        canvas.drawText("Aadhaar Number: ${aadhaarNo.ifEmpty { "N/A" }}", rightX, startY + 80f, keys)
        canvas.drawText("Bank Name: ${bankName.ifEmpty { "N/A" }}", rightX, startY + 130f, keys)
        canvas.drawText("RRN: ${referenceNo.ifEmpty { "N/A" }}", rightX, startY + 180f, keys)
        if(SdkConstants.transactionType != "4"){
            canvas.drawText("Balance Amount: ${balanceAmt.ifEmpty { "N/A" }}", rightX, startY + 230f, keys)
        }

        if (SdkConstants.transactionType == "1" || SdkConstants.transactionType == "3"|| SdkConstants.transactionType == "4") {
            canvas.drawText(
                "Transaction Amount: Rs.$transactionAmount",
                rightX,
                startY + 280f,
                keys
            )
        } else {
//            canvas.drawText("Transaction Amount: " + "N/A", rightX, startY + 280f, keys)
            startY -= 50f
        }
        canvas.drawText("Transaction Type: ${transactionMode.ifEmpty { "N/A" }}", rightX, startY + 330f, keys)
    }
    canvas.drawText("Thank You", leftX, startY + 430f, rightData)
    canvas.drawText(SdkConstants.BRAND_NAME, leftX, startY + 480f, rightData)
    pdfDocument.finishPage(myPage)
    // below line is used to set the name of PDF file and its path.
    var save: String
    var num = 0
    var folder = File(
        Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
            .toString() + "/Unifier_Aeps_Report.pdf"
    )
    while (folder.exists()) {
        save = "Transaction Report" + num++ + ".pdf"
        folder = File(
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), save
        )
    }
    try {
        pdfDocument.writeTo(FileOutputStream(folder))
        Toast.makeText(context, "PDF file generated successfully.", Toast.LENGTH_SHORT).show()
    } catch (e: IOException) {
        e.printStackTrace()
    }
    pdfDocument.close()
    val path: Uri = FileProvider.getUriForFile(
        context, context.applicationContext.packageName + ".provider", folder
    )
    val pdfOpenIntent = Intent(Intent.ACTION_VIEW)
    pdfOpenIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
    pdfOpenIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
    pdfOpenIntent.setDataAndType(path, "application/pdf")
    try {
        context.startActivity(pdfOpenIntent)
    } catch (e: ActivityNotFoundException) {
        e.printStackTrace()
    }
}