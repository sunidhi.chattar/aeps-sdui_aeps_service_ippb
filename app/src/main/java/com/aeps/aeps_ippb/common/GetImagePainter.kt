package com.aeps.aeps_ippb.common

import android.content.Context
import androidx.compose.runtime.Composable
import coil.compose.AsyncImagePainter
import coil.compose.rememberAsyncImagePainter
import coil.decode.SvgDecoder
import coil.request.ImageRequest

@Composable
fun getImagePainter(
    context: Context,
    imageUrl: String,
): AsyncImagePainter {
    return rememberAsyncImagePainter(
        model = ImageRequest.Builder(context = context)
            .data(imageUrl)
            .decoderFactory(SvgDecoder.Factory())
            .build()
    )
}
