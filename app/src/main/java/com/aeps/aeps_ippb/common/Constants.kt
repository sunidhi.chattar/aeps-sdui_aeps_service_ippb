package com.aeps.aeps_ippb.common

object Constants {
    var BIO_AUTH_BASE_URL = ""/*"https://vpn.iserveu.tech/"*/
    var ADDRESS_BASE_URL =
        ""/*"https://us-central1-iserveustaging.cloudfunctions.net/pincodeFetch/api/v1/getCitystateAndroid"*/
    var UNIFIED_AEPS_BASE_URL = ""/* "https://unifiedaepsbeta.iserveu.tech/"*/
    var baseurlSdkUser = ""/*"https://aeps-prod-gateway-as1-5pwajhaz.ts.gateway.dev/"*/
    var AADHAAR_PAY_BASE_URL = ""
    var TRANSACTION_ENCODED_URL_CORE_BE = ""
    var TRANSACTION_ENCODED_URL_CORE_CW = ""
    var TRANSACTION_ENCODED_URL_CORE_MS = ""
    var TRANSACTION_ENCODED_URL_CORE_AP = ""
    var TRANSACTION_ENCODED_URL_CORE_CD = ""
    var CHECK_BIO_AUTH_URL = ""
    var SUBMIT_BIO_AUTH_URL = ""

    var fType=""
    var timeout=""
    var env=""
    var wadh=""

}