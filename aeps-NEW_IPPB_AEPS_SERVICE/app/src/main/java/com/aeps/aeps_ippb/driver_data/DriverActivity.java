package com.aeps.aeps_ippb.driver_data;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.aeps.aeps_ippb.R;
import com.aeps.aeps_ippb.common.Constants;
import com.aeps.aeps_ippb.common.SdkConstants;
import com.aeps.aeps_ippb.driver_data.fingerprintmodel.DeviceInfo;
import com.aeps.aeps_ippb.driver_data.fingerprintmodel.Opts;
import com.aeps.aeps_ippb.driver_data.fingerprintmodel.PidData;
import com.aeps.aeps_ippb.driver_data.fingerprintmodel.PidOptions;

import org.json.JSONException;
import org.json.JSONObject;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;


public class DriverActivity extends AppCompatActivity implements ConnectionLostCallback {
    boolean usbconnted = false;
    String deviceSerialNumber = "0";
    String mantradeviceid = "MANTRA";
    String morphodeviceid = "SAGEM SA";
    String morphoe2device = "Morpho";
    String precisiondeviceid = "Mvsilicon";
    String fmDeviceId = "Startek Eng-Inc.";
    String fmDeviceId2 = "Startek Eng-Inc.\u0000";
    String fmDeviceId3 = "Startek Eng. Inc.";
    String fmDeviceId4 = "Startek";
    String TATVIK = "TATVIK";
    UsbManager musbManager;
    String freshnessFactor = "";
    String aadharNo = "";
    PlugInControlReceiver usbReceiver;
    private UsbDevice usbDevice;
    private Serializer serializer;
    private ArrayList<String> positions;
    private String driverFlag = "";
    String errorMessage = "";
    String Secugen = "SecuGen Corp.";
    private String productName = "";
    ActivityResultLauncher<Intent> launcher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
        if (result.getResultCode() == Activity.RESULT_OK) {
            try {
                Intent data = result.getData();
                if (data != null) {
                    String results = data.getStringExtra("PID_DATA");
                    byte[] dataPid = results.getBytes(StandardCharsets.UTF_8);
                    String base64pidData = Base64.encodeToString(dataPid, Base64.DEFAULT);
                    PidData pidData = serializer.read(PidData.class, results);
                    errorMessage = pidData._Resp.errInfo;
                    DeviceInfo info = pidData._DeviceInfo;
                    SdkConstants.Companion.setRECEIVE_DRIVER_DATA("");
                    try {
                        JSONObject obj = new JSONObject();
                        obj.put("CI", pidData._Skey.ci);
                        obj.put("DC", info.dc);
                        obj.put("DPID", info.dpId);
                        obj.put("DATAVALUE", pidData._Data.value);
                        obj.put("HMAC", pidData._Hmac);
                        obj.put("MC", info.mc);
                        obj.put("MI", info.mi);
                        obj.put("RDSID", info.rdsId);
                        obj.put("RDSVER", info.rdsVer);
                        obj.put("value", pidData._Skey.value);
                        obj.put("srno", pidData._DeviceInfo.add_info.params.get(0).value);

                        if ((usbDevice != null && (usbDevice.getManufacturerName().equalsIgnoreCase(TATVIK) || usbDevice.getManufacturerName().equalsIgnoreCase(precisiondeviceid))) || SdkConstants.Companion.getBluetoothname().equalsIgnoreCase("EVOLUTE") || SdkConstants.Companion.getBluetoothname().equalsIgnoreCase("BLUPRINT")) {
                            obj.put("pidata_qscore", pidData._Resp.nmPoints);

                            if ((Integer.parseInt(pidData._Resp.nmPoints)) >= 1 && (Integer.parseInt(pidData._Resp.nmPoints) <= 10)) {
                                int score = (Integer.parseInt(pidData._Resp.nmPoints)) + 60;
                                obj.put("pidata_qscore", String.valueOf(score));
                            } else if ((Integer.parseInt(pidData._Resp.nmPoints)) >= 11 && (Integer.parseInt(pidData._Resp.nmPoints) <= 30)) {
                                int score = (Integer.parseInt(pidData._Resp.nmPoints)) * 2 + 20;
                                obj.put("pidata_qscore", String.valueOf(score));
                            } else if ((Integer.parseInt(pidData._Resp.nmPoints)) >= 31 && (Integer.parseInt(pidData._Resp.nmPoints) <= 50)) {
                                int score = (Integer.parseInt(pidData._Resp.nmPoints)) * 2;
                                obj.put("pidata_qscore", String.valueOf(score));
                            } else {
                                obj.put("pidata_qscore", pidData._Resp.nmPoints);
                            }

                        } else if (usbDevice != null && (usbDevice.getManufacturerName().equalsIgnoreCase(morphodeviceid) || usbDevice.getManufacturerName().equalsIgnoreCase(morphoe2device) ||
                                usbDevice.getManufacturerName().equalsIgnoreCase(fmDeviceId) || usbDevice.getManufacturerName().equalsIgnoreCase(fmDeviceId2)
                                || usbDevice.getManufacturerName().equalsIgnoreCase(fmDeviceId3) || usbDevice.getManufacturerName().equalsIgnoreCase(fmDeviceId4))) {
                            if ((Integer.parseInt(pidData._Resp.qScore)) >= 1 && (Integer.parseInt(pidData._Resp.qScore) <= 10)) {
                                int score = (Integer.parseInt(pidData._Resp.qScore)) + 60;
                                obj.put("pidata_qscore", String.valueOf(score));
                            } else if ((Integer.parseInt(pidData._Resp.qScore)) >= 11 && (Integer.parseInt(pidData._Resp.qScore) <= 30)) {
                                int score = (Integer.parseInt(pidData._Resp.qScore)) * 2 + 20;
                                obj.put("pidata_qscore", String.valueOf(score));
                            } else if ((Integer.parseInt(pidData._Resp.qScore)) >= 31 && (Integer.parseInt(pidData._Resp.qScore) <= 50)) {
                                int score = (Integer.parseInt(pidData._Resp.qScore)) * 2;
                                obj.put("pidata_qscore", String.valueOf(score));
                            } else {
                                obj.put("pidata_qscore", pidData._Resp.qScore);
                            }
                        } else {
                            obj.put("pidata_qscore", pidData._Resp.qScore);
                        }

                        obj.put("base64pidData", base64pidData);
                        SdkConstants.Companion.setRECEIVE_DRIVER_DATA(obj.toString());
                        try {
                            onBackPressed();
                        } catch (Exception e) {
                            e.printStackTrace();
                            onBackPressed();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            } catch (Exception e) {
                if (!errorMessage.equals("")) {
                    showAlertMessage(errorMessage);
                } else {
                    showAlertMessage("Please connect your device  properly. ");
                }
            }
        }
    });


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//           setContentView(R.layout.activity_driver);
        serializer = new Persister();
        SdkConstants.Companion.setRECEIVE_DRIVER_DATA("");
        usbReceiver = new PlugInControlReceiver(DriverActivity.this, DriverActivity.this);
        if (getIntent().hasExtra("driverFlag")) {
            driverFlag = getIntent().getStringExtra("driverFlag");
        }
        if (getIntent().hasExtra("freshnesFactor")) {
            freshnessFactor = getIntent().getStringExtra("freshnesFactor");
        }
        if (getIntent().hasExtra("AadharNo")) {
            aadharNo = getIntent().getStringExtra("AadharNo");
        }
        positions = new ArrayList<>();
        musbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        if (SdkConstants.Companion.getBluetoothConnector()) {
            if (SdkConstants.Companion.getBluetoothname().equalsIgnoreCase("EVOLUTE")) {
                installationCheck("com.evolute.rdservice", "Evolute RD Service", "Unable to find Evolute Service app. Please install it from playstore.");
            } else if (SdkConstants.Companion.getBluetoothname().equalsIgnoreCase("BLUPRINT")) {
                installationCheck("com.nextbiometrics.onetouchrdservice", "Bluprint Biometric RD Service", "Unable to find Bluprint Biometric Service app. Please install it from playstore.");
            }
        } else if (SdkConstants.Companion.getInternalFPName().equalsIgnoreCase("wiseasy")) {
            boolean appInstall = appInstalledOrNot("co.aratek.afour_ngms.rdservice");
            if (appInstall) {
                aratekWiseasyCapture();
            } else {
                Toast.makeText(this, "App Not Installed", Toast.LENGTH_SHORT).show();
            }
        } else {
            callRdService();
        }
    }

    private void aratekWiseasyCapture() {
        try {
            String fpInputData = getFpInputData();
            Intent intent = new Intent();
            intent.setAction("in.gov.uidai.rdservice.fp.CAPTURE");
            intent.putExtra("PID_OPTIONS", fpInputData);
            launcher.launch(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getFpInputData() {
        Log.d("WiseasyDriverActivity", "startCaptureRegistered");
        String envType = "P";
        String fCount = "1";
        String inputXml = String.format("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + "<PidOptions ver=\"" + "1.0\">" + "<Opts " + "fCount=\"" + fCount + "\" " + "fType=\"" + "2" + "\" " + "format=\"" + "0" + "\" " + "pidVer=\"" + "2.0" + "\" " + "timeout=\"" + "15000" + "\" " + "otp=\"" + "" + "\" " + "wadh=\"" + "" + "\" " + "posh=\"" + "" + "\" " + "env=\"" + envType + "\" " + "/>" + "</PidOptions>");
        Log.d("WiseasyDriverActivity", "inputxml " + inputXml);

        return inputXml;
    }

    protected void onStart() {
        super.onStart();
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(usbReceiver, filter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (usbReceiver != null) {
            unregisterReceiver(usbReceiver);
        }
    }


    public void callRdService() {
        if (biometricDeviceConnect()) {
            musbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
            biometricDeviceCheck();
        }
    }

    private Boolean biometricDeviceConnect() {
        HashMap<String, UsbDevice> connectedDevices = musbManager.getDeviceList();
        usbDevice = null;
        if (connectedDevices.isEmpty()) {
            deviceConnectMessgae();
            return false;
        } else {
            for (UsbDevice device : connectedDevices.values()) {
                if (device != null && device.getManufacturerName() != null) {
                    usbDevice = device;
                    driverFlag = usbDevice.getManufacturerName();
                    productName = usbDevice.getProductName();
                    return true;
                }
            }
        }
        return false;
    }

    private void updateDeviceList() {
        HashMap<String, UsbDevice> connectedDevices = musbManager.getDeviceList();
        usbDevice = null;
        if (connectedDevices.isEmpty()) {
            usbconnted = false;
            deviceConnectMessgae();
        } else {
            for (UsbDevice device : connectedDevices.values()) {
                usbconnted = true;
                if (device != null && device.getManufacturerName() != null) {
                    usbDevice = device;
                    deviceSerialNumber = usbDevice.getManufacturerName();
                }

            }
            biometricDeviceCheck();
        }
    }

    private void biometricDeviceCheck() {
        if (usbDevice != null) {
            if (driverFlag.equalsIgnoreCase(mantradeviceid)) {
                if (productName.equalsIgnoreCase("MFS100")) {
                    installationCheck("com.mantra.rdservice", getResources().getString(R.string.mantra_install), getResources().getString(R.string.mantra_rd_service));
                } else if (productName.equalsIgnoreCase("MIS100V2")) {
                    installationCheck("com.mantra.mis100v2.rdservice", "MANTRA RD Service", "Unable to find MANTRA Service app. Please install it from playstore.");
                } else if (productName.equalsIgnoreCase("MFS110")) {
                    installationCheck("com.mantra.mfs110.rdservice", "MANTRA RD Service", "Unable to find MANTRA Service app. Please install it from playstore.");
                }
            } else if (driverFlag.equalsIgnoreCase(morphodeviceid) || driverFlag.equalsIgnoreCase(morphoe2device)) {
                installationCheck("com.scl.rdservice", getResources().getString(R.string.morpho), getResources().getString(R.string.install_morpho_message));
            } else if (driverFlag.equalsIgnoreCase(precisiondeviceid)) {
                installationCheck("com.precision.pb510.rdservice", "Precision RD Service", "Unable to find Precision Service app. Please install it from playstore.");
            } else if (driverFlag.equalsIgnoreCase(fmDeviceId) || driverFlag.contains(fmDeviceId4) || driverFlag.equalsIgnoreCase(fmDeviceId2) || driverFlag.equalsIgnoreCase(fmDeviceId3)) {
                installationCheck("com.acpl.registersdk", getResources().getString(R.string.Fm220U_install), getResources().getString(R.string.fm220U_service));
            } else if (driverFlag.equalsIgnoreCase(TATVIK)) {
                installationCheck("com.tatvik.bio.tmf20", "Tatvik RD Service", "Unable to find Tatvik Service app. Please install it from playstore.");
            } else if (driverFlag.trim().contains(Secugen)) {
                installationCheck("com.secugen.rdservice", "SecuGen RD Service", "Unable to find SecuGen Service app. Please install it from playstore.");
            } else {
                deviceConnectMessgae();
            }
        } else {
            updateDeviceList();
        }
    }


    private void installationCheck(String packageName, String dialogTitle, String dialogMessage) {
        boolean isAppInstalled = appInstalledOrNot(packageName);
        if (isAppInstalled) {
            if (productName.equalsIgnoreCase("MIS100V2")) {
                captureIris(packageName);
            } else {
                captureFingerprint(packageName);
            }
        } else {
            installationMessage(packageName, dialogTitle, dialogMessage);
        }
    }

    private void installationMessage(String packageName, String dialogTitle, String dialogMessage) {
        android.app.AlertDialog.Builder builder;
        builder = new android.app.AlertDialog.Builder(DriverActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
        builder.setCancelable(false);
        builder.setTitle(dialogTitle).setMessage(dialogMessage).setPositiveButton(getResources().getString(R.string.ok_error), (dialog, which) -> {
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)));
                finish();
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + packageName)));
                finish();
            }
        }).show();
    }

    private void captureFingerprint(String packageName) {
        try {
            String pidOption = getPIDOptions();
            if (pidOption != null) {
                Intent intent = new Intent("in.gov.uidai.rdservice.fp.CAPTURE");
                intent.setPackage(packageName);
                intent.putExtra("PID_OPTIONS", pidOption);
                launcher.launch(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    private void captureIris(String packageName) {
        try {
            String pidOption = getPIDOptionsIris();
            if (pidOption != null) {
                Intent intent = new Intent("in.gov.uidai.rdservice.iris.CAPTURE");
                intent.setPackage(packageName);
                intent.putExtra("PID_OPTIONS", pidOption);
                launcher.launch(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }


    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }

    private void deviceConnectMessgae() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(DriverActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
        builder.setCancelable(false);
        builder.setTitle(getResources().getString(R.string.device_connect)).setMessage(getResources().getString(R.string.setting_device)).setPositiveButton(getResources().getString(R.string.ok_error), (dialog, which) -> {
            dialog.dismiss();
            finish();
        });
        builder.show();
    }

    private String getPIDOptions() {
        try {
            String posh = "UNKNOWN";
            if (positions.size() > 0) {
                posh = positions.toString().replace("[", "").replace("]", "").replaceAll("[\\s+]", "");
            }
            Opts opts = new Opts();
            opts.fCount = "1";
            opts.fType = Constants.INSTANCE.getFType();
            opts.iCount = "0";
            opts.iType = "0";
            opts.pCount = "0";
            opts.pType = "0";
            opts.format = "0";
            opts.pidVer = "2.0";
            opts.timeout = Constants.INSTANCE.getTimeout();
            opts.posh = posh;
            opts.env = Constants.INSTANCE.getEnv();
            opts.otp = "";
            opts.wadh = Constants.INSTANCE.getWadh();
            PidOptions pidOptions = new PidOptions();
            pidOptions.ver = "1.0";
            pidOptions.Opts = opts;
            Serializer serializer = new Persister();
            StringWriter writer = new StringWriter();
            serializer.write(pidOptions, writer);
            return writer.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private String getPIDOptionsIris() {
        try {
            String posh = "UNKNOWN";
            if (positions.size() > 0) {
                posh = positions.toString().replace("[", "").replace("]", "").replaceAll("[\\s+]", "");
            }
            Opts opts = new Opts();
            opts.fCount = "0";
            opts.fType = "0";
            opts.iCount = "1";
            opts.iType = "0";
            opts.pCount = "0";
            opts.pType = "0";
            opts.format = "0";
            opts.pidVer = "2.0";
            opts.timeout = "20000";
            opts.posh = posh;
            opts.env = Constants.INSTANCE.getEnv();
            opts.otp = "";

            opts.wadh = Constants.INSTANCE.getWadh();
            PidOptions pidOptions = new PidOptions();
            pidOptions.ver = "1.0";
            pidOptions.Opts = opts;
            Serializer serializer = new Persister();
            StringWriter writer = new StringWriter();
            serializer.write(pidOptions, writer);
            return writer.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public void connectionLost() {
    }

    public void showAlertMessage(String message) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(DriverActivity.this);
            builder.setTitle("");
            builder.setMessage(message);
            builder.setPositiveButton("OK", (dialog, which) -> {
                dialog.dismiss();
                finish();
            });
            AlertDialog dialog = builder.create();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
