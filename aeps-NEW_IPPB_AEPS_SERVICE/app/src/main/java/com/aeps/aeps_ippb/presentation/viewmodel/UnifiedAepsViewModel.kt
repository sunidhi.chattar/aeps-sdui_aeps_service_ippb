package com.aeps.aeps_ippb.presentation.viewmodel

import android.content.Context
import android.widget.Toast
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.navigation.NavHostController
import com.aeps.aeps_ippb.common.Constants
import com.aeps.aeps_ippb.common.Constants.baseurlSdkUser
import com.aeps.aeps_ippb.common.SdkConstants
import com.aeps.aeps_ippb.common.TransactionType
import com.aeps.aeps_ippb.common.handleFlow
import com.aeps.aeps_ippb.data.remote.dto.response.ui_data.FetchUiData
import com.aeps.aeps_ippb.domain.model.request.SubmitBioAuthReqDomain
import com.aeps.aeps_ippb.domain.model.request.TransferRequestDomain
import com.aeps.aeps_ippb.domain.model.response.CheckBioAuthDomain
import com.aeps.aeps_ippb.domain.model.response.TransferResponseDomain
import com.aeps.aeps_ippb.domain.usecase.UseCases
import com.aeps.aeps_ippb.presentation.navgraph.Destinations

class UnifiedAepsViewModel constructor(
    private val useCases: UseCases
) : ViewModel() {
    var apiResponse: MutableState<TransferResponseDomain?> = mutableStateOf(null)
        private set
    var uiResponse: MutableState<FetchUiData?> = mutableStateOf(null)
        private set
    var scoreStr: MutableState<String> = mutableStateOf(value = "0")
        private set
    var amount: MutableState<String> = mutableStateOf("")
        private set
    var aadhaarNo: MutableState<String> = mutableStateOf("")
        private set
    var bankName: MutableState<String> = mutableStateOf("")
        private set
    var latLong: MutableState<String> = mutableStateOf("")
        private set
    var pidData: MutableState<String> = mutableStateOf("")
        private set
    var showDialog: MutableState<Boolean> = mutableStateOf(value = false)
        private set
    var isChecked: MutableState<Boolean> = mutableStateOf(value = false)
        private set
    var isCaptured: MutableState<Boolean> = mutableStateOf(value = false)
        private set
    var showDeviceTypeDialog: MutableState<Boolean> = mutableStateOf(value = true)
        private set
    var showDeviceTypeDialogBio: MutableState<Boolean> = mutableStateOf(value = true)
        private set
    var showLocationDialog: MutableState<Boolean> = mutableStateOf(value = false)
        private set
    private var isDataFetched: MutableState<Boolean> = mutableStateOf(value = false)

    var askForLocationPermission: MutableState<Boolean> = mutableStateOf(value = false)
        private set
    var isFingerClicked: MutableState<Boolean> = mutableStateOf(false)
        private set
    var isFingerCaptureFailed: MutableState<Boolean> = mutableStateOf(false)
        private set
    var isFingerEnabled: MutableState<Boolean> = mutableStateOf(false)
        private set
    var isLoading: MutableState<Boolean> = mutableStateOf(false)
        private set
    var isStartPrinting: MutableState<Boolean> = mutableStateOf(false)
        private set
    var showPrinterDialogue: MutableState<Boolean> = mutableStateOf(value = false)
        private set
    private var isSL: MutableState<Boolean> = mutableStateOf(false)
    var bioAuthVisible: MutableState<Boolean> = mutableStateOf(false)
        private set
    var showLoader: MutableState<Boolean> = mutableStateOf(false)
        private set
    var postalCode: MutableState<String?> = mutableStateOf("")
        private set
    var errorMessage: MutableState<String?> = mutableStateOf("")
        private set
    private var gatewayPriority: MutableState<Int?> = mutableStateOf(0)
    var terminalId: MutableState<String?> = mutableStateOf(null)
        private set


    //UI Data Fetch
    fun fetchUiData(navHostController: NavHostController) {
        if (!isDataFetched.value) {
            isDataFetched.value = true
            val res = useCases.fetchUiUseCase.invoke(adminName = SdkConstants.adminName)
            handleFlow(response = res, onLoading = {
                isLoading.value = it
            }, onFailure = {
                showDialog.value = true
                errorMessage.value = it
            }, onSuccess = {
                uiResponse.value = it
                Constants.BIO_AUTH_BASE_URL = it.data?.uiJson?.baseUrl?.bioAuthBaseUrl ?: ""
                Constants.UNIFIED_AEPS_BASE_URL = it.data?.uiJson?.baseUrl?.unifiedAepsBaseUrl ?: ""
                baseurlSdkUser = it.data?.uiJson?.baseUrl?.baseurlSdkUser ?: ""
                Constants.ADDRESS_BASE_URL = it.data?.uiJson?.baseUrl?.addressBaseUrl ?: ""
                Constants.AADHAAR_PAY_BASE_URL = it.data?.uiJson?.baseUrl?.aadhaarPayBaseUrl ?: ""
                Constants.TRANSACTION_ENCODED_URL_CORE_BE =
                    it.data?.uiJson?.baseUrl?.transactionEncodedUrlCoreBE ?: ""
                Constants.TRANSACTION_ENCODED_URL_CORE_MS =
                    it.data?.uiJson?.baseUrl?.transactionEncodedUrlCoreMS ?: ""
                Constants.TRANSACTION_ENCODED_URL_CORE_CW =
                    it.data?.uiJson?.baseUrl?.transactionEncodedUrlCoreCW ?: ""
                Constants.TRANSACTION_ENCODED_URL_CORE_AP =
                    it.data?.uiJson?.baseUrl?.transactionEncodedUrlCoreAP ?: ""
                Constants.TRANSACTION_ENCODED_URL_CORE_CD =
                    it.data?.uiJson?.baseUrl?.transactionEncodedUrlCoreCD ?: ""
                Constants.CHECK_BIO_AUTH_URL =
                    it.data?.uiJson?.baseUrl?.checkBioAuthUrl ?: ""
                Constants.SUBMIT_BIO_AUTH_URL =
                    it.data?.uiJson?.baseUrl?.submitBioAuthUrl ?: ""
                Constants.fType = it.data?.uiJson?.driverDate?.fType ?: "2"
                Constants.timeout = it.data?.uiJson?.driverDate?.timeout ?: "30000"
                Constants.env = it.data?.uiJson?.driverDate?.env ?: "P"
                Constants.wadh = it.data?.uiJson?.driverDate?.wadh ?: ""
                if (navHostController.currentDestination!!.route.toString() == Destinations.BIO_AUTH_SCREEN) {
                    checkBioAuthStatus {
                        navHostController.navigate(Destinations.FINGERPRINT_CAPTURE_ROUTE) {
                            this.popUpTo(Destinations.BIO_AUTH_SCREEN) {
                                inclusive = true
                            }
                        }
                    }
                }
            })
        }
    }

    // Checking the bioauth status
    private fun checkBioAuthStatus(onComplete: () -> Unit) {
        if (Constants.CHECK_BIO_AUTH_URL != "") {
            if (SdkConstants.tokenFromCoreApp != null) {
                val res = useCases.checkBioAuthUseCase(
                    Constants.CHECK_BIO_AUTH_URL,
                    SdkConstants.tokenFromCoreApp!!
                )
                handleFlow(response = res, onLoading = {
                    isLoading.value = it
                }, onFailure = {
                    showDialog.value = true
                    errorMessage.value = it
                }, onSuccess = {
                    val bioAuthStatus: Int? = it.status
                    if (bioAuthStatus == 1) {
                        bioAuthVisible.value = false
                        onComplete()
                    } else {
                        bioAuthVisible.value = true
                    }
                })
            } else {
                showDialog.value = true
                errorMessage.value = "Token Missing"
            }
        } else {
            showDialog.value = true
            errorMessage.value = "URL Missing"
        }
    }

    // Submit BioAuth
    fun onSubmitBioAuth(context: Context, onComplete: (CheckBioAuthDomain) -> Unit) {
        if (scoreStr.value == "0") {
            Toast.makeText(context, "Please do biometric verification", Toast.LENGTH_SHORT).show()
        } else {
            val submitBioAuthReqDomain =
                SubmitBioAuthReqDomain(latLong = latLong.value, pidData = pidData.value)
            submitBioAuth(submitBioAuthReqDomain) {
                onComplete(it)
            }
        }
    }

    // Submit BioAuth
    private fun submitBioAuth(
        submitBioAuthReqDomain: SubmitBioAuthReqDomain,
        onComplete: (CheckBioAuthDomain) -> Unit
    ) {
        if (Constants.SUBMIT_BIO_AUTH_URL != "") {
            if (SdkConstants.tokenFromCoreApp != null) {
                val res = useCases.submitBioAuthUseCase.invoke(
                    Constants.SUBMIT_BIO_AUTH_URL,
                    submitBioAuthReqDomain,
                    SdkConstants.tokenFromCoreApp!!
                )
                handleFlow(response = res,
                    onLoading = {
                        showLoader.value = it
                    }, onFailure = {
                        showDialog.value = true
                        errorMessage.value = it
                    }, onSuccess = {
                        val status = it.status
                        if (status != null) {
                            onComplete(it)
                        } else {
                            showDialog.value = true
                            errorMessage.value = "Something Went Wrong"
                        }
                    })
            } else {
                showDialog.value = true
                errorMessage.value = "Token Missing"
            }
        } else {
            showDialog.value = true
            errorMessage.value = "Url Missing"
        }
    }

    // Submit Transaction
    fun onSubmit(context: Context, onComplete: () -> Unit) {
        val transactionRequest: TransferRequestDomain
        if (SdkConstants.transactionType == TransactionType.CashWithdraw.name || SdkConstants.transactionType == TransactionType.AadhaarPay.name || SdkConstants.transactionType == TransactionType.CashDeposit.name) {
            transactionRequest = TransferRequestDomain(
                aadharNo = SdkConstants.aadhaarNo,
                iin = SdkConstants.bankIIN,
                amount = SdkConstants.transactionAmount.toInt(),
                latLong = latLong.value,
                bankName = SdkConstants.bankName,
                mobileNumber = SdkConstants.mobileNo,
                pidData = pidData.value,
                apiUser = "ANDROIDUSER",
                paramA = SdkConstants.paramA,
                paramB = SdkConstants.paramB,
                paramC = SdkConstants.paramC,
                retailer = SdkConstants.userNameFromCoreApp,
                gatewayPriority = gatewayPriority.value,
                apiUserName = SdkConstants.API_USER_NAME_VALUE,
                isSL = if (SdkConstants.transactionType == TransactionType.AadhaarPay.name) isSL.value else null
            )
        } else {
            transactionRequest = TransferRequestDomain(
                aadharNo = SdkConstants.aadhaarNo,
                iin = SdkConstants.bankIIN,
                latLong = latLong.value,
                bankName = SdkConstants.bankName,
                mobileNumber = SdkConstants.mobileNo,
                pidData = pidData.value,
                apiUser = "ANDROIDUSER",
                paramA = SdkConstants.paramA,
                paramB = SdkConstants.paramB,
                paramC = SdkConstants.paramC,
                retailer = SdkConstants.userNameFromCoreApp,
                gatewayPriority = gatewayPriority.value,
                apiUserName = SdkConstants.API_USER_NAME_VALUE
            )
        }
        val encodedUrl: String
        if (SdkConstants.transactionType == TransactionType.AadhaarPay.name) {
            doTransaction(
                Constants.TRANSACTION_ENCODED_URL_CORE_AP,
                transactionRequest,
                context,
                onComplete
            )
            return
        } else {
            if ((SdkConstants.transactionType == TransactionType.BalanceEnquiry.name) && (Constants.TRANSACTION_ENCODED_URL_CORE_BE.isNotEmpty())) {
                encodedUrl = Constants.TRANSACTION_ENCODED_URL_CORE_BE
            } else if ((SdkConstants.transactionType == TransactionType.MiniStatement.name) && (Constants.TRANSACTION_ENCODED_URL_CORE_MS.isNotEmpty())) {
                encodedUrl = Constants.TRANSACTION_ENCODED_URL_CORE_MS
            } else if ((SdkConstants.transactionType == TransactionType.CashWithdraw.name) && (Constants.TRANSACTION_ENCODED_URL_CORE_CW.isNotEmpty())) {
                encodedUrl = Constants.TRANSACTION_ENCODED_URL_CORE_CW
            } else if ((SdkConstants.transactionType == TransactionType.CashDeposit.name) && (Constants.TRANSACTION_ENCODED_URL_CORE_CD.isNotEmpty())) {
                encodedUrl = Constants.TRANSACTION_ENCODED_URL_CORE_CD
            } else {
                showDialog.value = true
                errorMessage.value = "Url is empty"
                return
            }
            doTransaction(encodedUrl, transactionRequest, context, onComplete)
        }
    }

    private fun doTransaction(
        encodedUrl: String,
        transactionRequest: TransferRequestDomain,
        context: Context,
        onComplete: () -> Unit
    ) {
        if (SdkConstants.tokenFromCoreApp != null) {
            val res = useCases.transferUseCase.invoke(
                encodedUrl, SdkConstants.tokenFromCoreApp!!, transactionRequest
            )
            handleFlow(response = res, onLoading = {
                showLoader.value = it
            }, onFailure = {
                showDialog.value = true
                errorMessage.value = it
            }, onSuccess = {
                apiResponse.value = it
                onComplete()
            })
        } else {
            Toast.makeText(context, "Missing Authorization Token", Toast.LENGTH_SHORT).show()
        }
    }
}