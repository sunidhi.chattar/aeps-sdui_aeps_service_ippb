package com.aeps.aeps_ippb

import android.app.Activity
import android.app.Application
import android.content.Context
import android.os.Bundle
import com.aeps.aeps_ippb.di.aepsModule
import com.google.firebase.FirebaseApp
import com.iserveu.inappdistribution.ISUInAppUpdateSDK
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin


class MyApplication : Application(), Application.ActivityLifecycleCallbacks {
    override fun onCreate() {
        super.onCreate()
        registerActivityLifecycleCallbacks(this)
        startKoin {
            androidLogger()
            androidContext(this@MyApplication)
            modules(aepsModule)
        }
    }

    override fun onActivityCreated(p0: Activity, p1: Bundle?) {

    }

    override fun onActivityStarted(p0: Activity) {
    }

    override fun onActivityResumed(p0: Activity) {
        FirebaseApp.initializeApp(p0)
//        ISUInAppUpdateSDK.begin(p0 as Context)
    }

    override fun onActivityPaused(p0: Activity) {

    }

    override fun onActivityStopped(p0: Activity) {

    }

    override fun onActivitySaveInstanceState(p0: Activity, p1: Bundle) {

    }

    override fun onActivityDestroyed(p0: Activity) {

    }
}