package com.aeps.aeps_ippb.presentation.finger_capture_screen

import android.app.Activity
import android.content.Intent
import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.AlertDialog
import androidx.compose.material.BottomSheetScaffold
import androidx.compose.material.BottomSheetValue
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Checkbox
import androidx.compose.material.CheckboxDefaults
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.material.rememberBottomSheetScaffoldState
import androidx.compose.material.rememberBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.DialogProperties
import androidx.navigation.NavHostController
import com.aeps.aeps_ippb.R
import com.aeps.aeps_ippb.common.SdkConstants
import com.aeps.aeps_ippb.common.getImagePainter
import com.aeps.aeps_ippb.driver_data.DriverActivity
import com.aeps.aeps_ippb.presentation.components.CustomLoader
import com.aeps.aeps_ippb.presentation.components.TopActionBar
import com.aeps.aeps_ippb.presentation.navgraph.Destinations
import com.aeps.aeps_ippb.presentation.unified_bioauth.ShowDialogToChooseDeviceType
import com.aeps.aeps_ippb.presentation.viewmodel.UnifiedAepsViewModel
import com.aeps.aeps_ippb.ui.theme.ConcentTextColor
import com.aeps.aeps_ippb.ui.theme.HeadingColor
import com.aeps.aeps_ippb.ui.theme.fingerGrey
import com.aeps.aeps_ippb.ui.theme.msTextColor
import com.aeps.aeps_ippb.ui.theme.primaryColor
import com.aeps.aeps_ippb.ui.theme.txnBkgbkgblue
import com.aeps.aeps_ippb.ui.theme.txnBkglightblue
import com.aeps.aeps_ippb.ui.theme.txnBkgtextColor
import com.aeps.aeps_ippb.ui.theme.txnTextColor
import kotlinx.coroutines.launch

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun FingerCaptureScreen(navHostController: NavHostController, viewModel: UnifiedAepsViewModel) {
    val context = LocalContext.current

    val isPlaying = remember {
        mutableStateOf(false)
    }
    val sheetState = rememberBottomSheetState(initialValue = BottomSheetValue.Collapsed)
    val scaffoldState = rememberBottomSheetScaffoldState(bottomSheetState = sheetState)

    viewModel.isFingerEnabled.value = sheetState.isCollapsed && viewModel.isChecked.value

    viewModel.isCaptured.value = sheetState.isExpanded
    val uiData = viewModel.uiResponse
    val scope = rememberCoroutineScope()
    LaunchedEffect(key1 = true) {
        viewModel.fetchUiData(navHostController)
    }
    if (viewModel.isFingerClicked.value) {
        if (viewModel.scoreStr.value != "0") {
            if (viewModel.scoreStr.value.contains(",")) {
                Toast.makeText(
                    context,
                    "Invalid Fingerprint Data",
                    Toast.LENGTH_SHORT
                )
                    .show()
            } else {
                isPlaying.value = true
                LaunchedEffect(key1 = !viewModel.isFingerEnabled.value) {
                    if (sheetState.isCollapsed) {
                        sheetState.expand()
                    }
                }
            }
        } else if (viewModel.isFingerCaptureFailed.value) {
            isPlaying.value = false
            LaunchedEffect(key1 = !viewModel.isFingerEnabled.value) {
                sheetState.collapse()
            }
        }
    }

    if (viewModel.showDialog.value) {
        AlertDialog(
            modifier = Modifier.border(
                width = 2.dp,
                shape = RoundedCornerShape(20.dp),
                color = primaryColor
            ),
            properties = DialogProperties(
                dismissOnBackPress = false,
                dismissOnClickOutside = false
            ),
            onDismissRequest = { viewModel.showDialog.value = false },
            confirmButton = {
                TextButton(
                    modifier = Modifier.padding(horizontal = 10.dp, vertical = 5.dp),
                    colors = ButtonDefaults.textButtonColors(
                        backgroundColor = primaryColor
                    ),
                    elevation = ButtonDefaults.elevation(10.dp),
                    onClick = {
                        (context as Activity).finish()
                    }
                ) {
                    Text(
                        "Ok",
                        color = Color.White,
                        fontFamily = FontFamily(Font(R.font.poppins_regular))
                    )
                }
            },
            title = {
                Text(
                    text = "Alert", color = Color.Black, fontWeight = FontWeight.Bold,
                    fontFamily = FontFamily(Font(R.font.poppins_semibold))
                )
            },
            text = {
                if (viewModel.errorMessage.value?.isNotEmpty() == true) {
                    Text(
                        viewModel.errorMessage.value ?: "", color = Color.Black,
                        fontFamily = FontFamily(Font(R.font.poppins_regular))
                    )
                } else {
                    Text(
                        "Something went wrong", color = Color.Black,
                        fontFamily = FontFamily(Font(R.font.poppins_regular))
                    )
                }
            },
            backgroundColor = Color.White,
            shape = RoundedCornerShape(20.dp),
            contentColor = Color.White
        )
    }


    BottomSheetScaffold(
        topBar = {
            TopActionBar(context)
        },
        scaffoldState = scaffoldState,
        sheetContent = {
            SheetContent(
                viewModel=viewModel,
                isFingerCaptureFailed = viewModel.isFingerCaptureFailed,
                isPlaying = isPlaying,
                progressScore = viewModel.scoreStr.value.toFloat(),
                proceedOnClick = {
                    viewModel.onSubmit(context) {
                        when (SdkConstants.transactionType) {
                            "2" -> {
                                viewModel.apiResponse.value?.let {
                                    if (it.status.equals("SUCCESS")) {
                                        navHostController.navigate(Destinations.MINISTATEMENT_ROUTE)
                                    } else {
                                        navHostController.navigate(Destinations.TRANSACTION_STATUS_ROUTE)
                                    }
                                }
                            }

                            else -> {
                                navHostController.navigate(Destinations.TRANSACTION_STATUS_ROUTE)
                            }
                        }
                    }
                }
            ) {
                scope.launch {
                    sheetState.collapse()
                    viewModel.scoreStr.value = "0"
                    isPlaying.value = false
                    try {
                        viewModel.isLoading.value = true
                        val intent =
                            Intent(
                                context,
                                DriverActivity::class.java
                            )
                        context.startActivity(intent)
                    } catch (e: Exception) {
                        Toast
                            .makeText(
                                context,
                                e.localizedMessage?.toString()
                                    ?: "Activity Not Found",
                                Toast.LENGTH_SHORT
                            )
                            .show()
                    }
                    viewModel.isFingerClicked.value = true
                }
            }
        },
        sheetShape = RoundedCornerShape(topStart = 40.dp, topEnd = 40.dp),
        sheetBackgroundColor = Color.White,
        sheetPeekHeight = 0.dp,
        sheetElevation = 20.dp,
        sheetGesturesEnabled = false
    ) {
        if (uiData.value == null) {
            CustomLoader()
        } else {
            uiData.value?.data?.uiJson?.screens?.fingerCaptureScreen?.let { ui ->
                uiData.value?.data?.uiJson?.appColor?.let {
                    primaryColor = Color(android.graphics.Color.parseColor(it.primaryColor))
                    txnBkgtextColor =
                        Color(android.graphics.Color.parseColor(it.txnBkgtextColor))
                    txnBkglightblue =
                        Color(android.graphics.Color.parseColor(it.txnBkglightblue))
                    txnBkgbkgblue = Color(android.graphics.Color.parseColor(it.txnBkgbkgblue))
                    msTextColor = Color(android.graphics.Color.parseColor(it.msTextColor))
                    txnTextColor = Color(android.graphics.Color.parseColor(it.txnTextColor))
                    fingerGrey = Color(android.graphics.Color.parseColor(it.fingerGrey))
                }
                Box(
                    modifier = Modifier
                        .fillMaxSize()
                        .background(
                            color = if (viewModel.isCaptured.value) Color.Gray else Color.Transparent
                        )
                ) {
                    if (viewModel.showDeviceTypeDialog.value) {
                        ShowDialogToChooseDeviceType(viewModel.showDeviceTypeDialog, context)
                    }
                    Column(
                        modifier = Modifier
                            .padding(horizontal = 33.dp)
                            .verticalScroll(rememberScrollState()),
                        horizontalAlignment = Alignment.Start
                    ) {
                        Spacer(modifier = Modifier.height(it.calculateTopPadding() + 45.dp))
                        Text(
                            modifier = Modifier.fillMaxWidth(),
                            textAlign = TextAlign.Start,
                            text = "Transact Now!",
                            color = HeadingColor,
                            fontSize = 20.sp,
                            fontFamily = FontFamily(Font(R.font.poppins_semibold))
                        )
                        Spacer(modifier = Modifier.height(13.dp))
                        Row(
                            modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.Top
                        ) {
                            Checkbox(
                                checked = viewModel.isChecked.value, onCheckedChange = {
                                    viewModel.isChecked.value = it
                                }, colors = CheckboxDefaults.colors(
                                    checkedColor = ConcentTextColor
                                )
                            )
                            Text(
                                fontFamily = FontFamily(Font(R.font.poppins_semibold)),
                                text = ui.checkBox?.text?.value
                                    ?: "",
                                fontWeight = FontWeight.SemiBold,
                                modifier = Modifier.padding(top = 10.dp)
                            )
                        }
                        Spacer(modifier = Modifier.height(23.dp))
                        Image(
                            modifier = Modifier
                                .size(
                                    width = (ui.fingerPrintImage?.image?.modifier?.width
                                        ?: 100).dp,
                                    height = (ui.fingerPrintImage?.image?.modifier?.height
                                        ?: 100).dp
                                )
                                .align(Alignment.CenterHorizontally),
                            painter = getImagePainter(
                                context = context,
                                ui.fingerPrintImage?.image?.imageUrl
                                    ?: ""
                            ),
                            contentDescription = "",
                            colorFilter = if (viewModel.isFingerEnabled.value) ColorFilter.tint(
                                primaryColor
                            ) else ColorFilter.tint(
                                fingerGrey
                            )
                        )
                        Spacer(modifier = Modifier.height(23.dp))
                        if (!viewModel.isCaptured.value) {
                            Button(
                                onClick = {
                                    viewModel.isCaptured.value = true
                                    if (viewModel.isFingerEnabled.value) {
                                        viewModel.isFingerClicked.value =
                                            true
                                        try {
                                            viewModel.isLoading.value = true
                                            val intent =
                                                Intent(
                                                    context,
                                                    DriverActivity::class.java
                                                )
                                            context.startActivity(intent)
                                        } catch (e: Exception) {
                                            Toast
                                                .makeText(
                                                    context,
                                                    e.localizedMessage?.toString()
                                                        ?: "Activity Not Found",
                                                    Toast.LENGTH_SHORT
                                                )
                                                .show()
                                        }
                                    }
                                    viewModel.isFingerEnabled.value = false
                                },
                                modifier = Modifier.fillMaxWidth(),
                                colors = ButtonDefaults.buttonColors(
                                    backgroundColor = primaryColor
                                ),
                                shape = RoundedCornerShape(10.dp),
                                enabled = viewModel.isFingerEnabled.value
                            ) {
                                Text(
                                    text = "CAPTURE",
                                    fontFamily = FontFamily(Font(R.font.poppins_semibold)),
                                    fontSize = 16.sp,
                                    color = Color.White
                                )
                            }
                            Spacer(modifier = Modifier.height(23.dp))
                        }
                    }
                    if (viewModel.isLoading.value && !viewModel.showLoader.value) {
                        CustomLoader()
                    }
                }
            }
        }
    }
}

