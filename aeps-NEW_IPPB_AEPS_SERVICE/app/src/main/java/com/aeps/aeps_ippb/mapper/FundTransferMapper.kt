package com.aeps.aeps_ippb.mapper

import com.aeps.aeps_ippb.common.Mapper
import com.aeps.aeps_ippb.data.remote.dto.request.TransferRequestDto
import com.aeps.aeps_ippb.data.remote.dto.response.TransferResponseDto
import com.aeps.aeps_ippb.domain.model.request.TransferRequestDomain
import com.aeps.aeps_ippb.domain.model.response.TransferResponseDomain


class FundTransferResponseMapper : Mapper<TransferResponseDto, TransferResponseDomain> {
    override fun mapToDto(domain: TransferResponseDomain): TransferResponseDto {
        return TransferResponseDto(
            transactionMode = domain.transactionMode,
            txId = domain.txId,
            bankName = domain.bankName,
            updatedDate = domain.updatedDate,
            iin = domain.iin,
            originIdentifier = domain.originIdentifier,
            createdDate = domain.createdDate,
            apiTid = domain.apiTid,
            balance = domain.balance,
            isRetriable = domain.isRetriable,
            ministatement = domain.ministatement?.let { mapMiniStatementDomainToDto(it) },
            apiComment = domain.apiComment,
            gateway = domain.gateway,
            errors = domain.errors,
            status = domain.status,
            authCode = domain.authCode,
            uidRefId = domain.uidRefId,
            depositId = domain.depositId
        )
    }

    override fun mapToDomain(dto: TransferResponseDto): TransferResponseDomain {
        return TransferResponseDomain(
            transactionMode = dto.transactionMode,
            txId = dto.txId,
            bankName = dto.bankName,
            updatedDate = dto.updatedDate,
            iin = dto.iin,
            originIdentifier = dto.originIdentifier,
            createdDate = dto.createdDate,
            apiTid = dto.apiTid,
            balance = dto.balance,
            isRetriable = dto.isRetriable,
            ministatement = dto.ministatement?.let { mapMiniStatementDtoToDomain(it) },
            apiComment = dto.apiComment,
            gateway = dto.gateway,
            errors = dto.errors,
            status = dto.status,
            authCode = dto.authCode,
            uidRefId = dto.uidRefId,
            depositId = dto.depositId
        )
    }


    private fun mapMiniStatementDtoToDomain(list: List<TransferResponseDto.MiniStatementDto?>): List<TransferResponseDomain.MiniStatementDomain> {
        val returnList: MutableList<TransferResponseDomain.MiniStatementDomain> =
            mutableListOf()

        for (item in list) {
            if (item != null) returnList.add(
                TransferResponseDomain.MiniStatementDomain(
                    date = item.date,
                    amount = item.amount,
                    txntype = item.txntype,
                    txnDesc = item.txnDesc,
                )
            )
        }
        return returnList
    }

    private fun mapMiniStatementDomainToDto(list: List<TransferResponseDomain.MiniStatementDomain?>): List<TransferResponseDto.MiniStatementDto> {
        val returnList: MutableList<TransferResponseDto.MiniStatementDto> = mutableListOf()

        for (item in list) {
            if (item != null) returnList.add(
                TransferResponseDto.MiniStatementDto(
                    date = item.date,
                    amount = item.amount,
                    txntype = item.txntype,
                    txnDesc = item.txnDesc,
                )
            )
        }
        return returnList
    }

}

class FundTransferRequestMapper : Mapper<TransferRequestDto, TransferRequestDomain> {
    override fun mapToDto(domain: TransferRequestDomain): TransferRequestDto {
        return TransferRequestDto(
            amount = domain.amount,
            latLong = domain.latLong,
            mobileNumber = domain.mobileNumber,
            bankName = domain.bankName,
            aadharNo = domain.aadharNo,
            pidData = domain.pidData,
            apiUser = domain.apiUser,
            iin = domain.iin,
            ipAddress = domain.ipAddress,
            shakey = domain.shakey,
            paramA = domain.paramA,
            paramB = domain.paramB,
            paramC = domain.paramC,
            retailer = domain.retailer,
            gatewayPriority = domain.gatewayPriority,
            apiUserName = domain.apiUserName
        )
    }

    override fun mapToDomain(dto: TransferRequestDto): TransferRequestDomain {
        return TransferRequestDomain(
            amount = dto.amount,
            latLong = dto.latLong,
            mobileNumber = dto.mobileNumber,
            bankName = dto.bankName,
            aadharNo = dto.aadharNo,
            pidData = dto.pidData,
            apiUser = dto.apiUser,
            iin = dto.iin,
            ipAddress = dto.ipAddress,
            shakey = dto.shakey,
            paramA = dto.paramA,
            paramB = dto.paramB,
            paramC = dto.paramC,
            retailer = dto.retailer,
            gatewayPriority = dto.gatewayPriority,
            apiUserName = dto.apiUserName
        )
    }

}