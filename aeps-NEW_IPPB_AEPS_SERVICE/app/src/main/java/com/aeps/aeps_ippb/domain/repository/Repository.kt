package com.aeps.aeps_ippb.domain.repository

import com.aeps.aeps_ippb.common.NetworkResource
import com.aeps.aeps_ippb.data.remote.dto.response.ui_data.FetchUiData
import com.aeps.aeps_ippb.domain.model.request.*
import com.aeps.aeps_ippb.domain.model.response.*
import kotlinx.coroutines.flow.Flow

interface Repository {

    fun transfer(
        endPoint: String, token: String, body: TransferRequestDomain
    ): Flow<NetworkResource<TransferResponseDomain>>

    fun fetchUIData(adminName: String): Flow<NetworkResource<FetchUiData>>

    fun checkBioAuthStatus(url: String,token:String):Flow<NetworkResource<CheckBioAuthDomain>>

    fun submitBioAuth(url: String,submitBioAuthReqDomain: SubmitBioAuthReqDomain,token:String):Flow<NetworkResource<CheckBioAuthDomain>>


}