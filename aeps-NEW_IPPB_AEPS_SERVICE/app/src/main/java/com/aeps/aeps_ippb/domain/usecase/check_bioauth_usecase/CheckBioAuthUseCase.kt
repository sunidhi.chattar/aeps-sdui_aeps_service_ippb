package com.aeps.aeps_ippb.domain.usecase.check_bioauth_usecase

import com.aeps.aeps_ippb.common.NetworkResource
import com.aeps.aeps_ippb.domain.model.response.CheckBioAuthDomain
import com.aeps.aeps_ippb.domain.repository.Repository
import kotlinx.coroutines.flow.Flow

class CheckBioAuthUseCase constructor(private val repository: Repository) {
    operator fun invoke(url: String, token: String): Flow<NetworkResource<CheckBioAuthDomain>> {
        return repository.checkBioAuthStatus(url, token)
    }
}