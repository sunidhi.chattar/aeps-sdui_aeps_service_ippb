package com.aeps.aeps_ippb.domain.usecase.fetchui_usecase

import com.aeps.aeps_ippb.common.NetworkResource
import com.aeps.aeps_ippb.data.remote.dto.response.ui_data.FetchUiData
import com.aeps.aeps_ippb.domain.repository.Repository
import kotlinx.coroutines.flow.Flow

class FetchUiUseCase constructor(
    private val repository: Repository
) {
    fun invoke(adminName: String): Flow<NetworkResource<FetchUiData>> {
        return repository.fetchUIData(adminName)
    }
}