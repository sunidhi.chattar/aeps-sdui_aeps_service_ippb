package com.aeps.aeps_ippb.domain.usecase.submit_bioauth_usecase

import com.aeps.aeps_ippb.common.NetworkResource
import com.aeps.aeps_ippb.domain.model.request.SubmitBioAuthReqDomain
import com.aeps.aeps_ippb.domain.model.response.CheckBioAuthDomain
import com.aeps.aeps_ippb.domain.repository.Repository
import kotlinx.coroutines.flow.Flow

class SubmitBioAuthUseCase(
    private val repository: Repository
) {
    fun invoke(
        url: String,
        submitBioAuthReqDomain: SubmitBioAuthReqDomain,
        token: String
        ): Flow<NetworkResource<CheckBioAuthDomain>> {
        return repository.submitBioAuth(url, submitBioAuthReqDomain, token)
    }
}