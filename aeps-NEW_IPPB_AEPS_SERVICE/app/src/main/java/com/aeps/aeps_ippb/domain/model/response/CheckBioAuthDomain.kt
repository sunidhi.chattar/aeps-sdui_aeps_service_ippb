package com.aeps.aeps_ippb.domain.model.response

data class CheckBioAuthDomain(
	val statusDesc: String? = null,

	val txnId: Any? = null,

	val status: Int? = null
)
