package com.aeps.aeps_ippb.presentation.finger_capture_screen

import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Divider
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.aeps.aeps_ippb.R
import com.aeps.aeps_ippb.presentation.viewmodel.UnifiedAepsViewModel
import com.aeps.aeps_ippb.ui.theme.primaryColor
import com.airbnb.lottie.compose.LottieAnimation
import com.airbnb.lottie.compose.LottieCompositionSpec
import com.airbnb.lottie.compose.animateLottieCompositionAsState
import com.airbnb.lottie.compose.rememberLottieComposition
import kotlin.math.roundToInt

@Composable
fun SheetContent(
    viewModel: UnifiedAepsViewModel,
    isFingerCaptureFailed: MutableState<Boolean>,
    isPlaying: MutableState<Boolean>,
    progressScore: Float,
    proceedOnClick: () -> Unit,
    fingerRecapture: () -> Unit
) {

// for speed
    val speed = remember {
        mutableStateOf(1f)
    }

    val composition = rememberLottieComposition(spec = LottieCompositionSpec.RawRes(R.raw.success))

    val progress = animateLottieCompositionAsState(
        composition = composition.value,
        iterations = 1,
        isPlaying = isPlaying.value,
        speed = speed.value,
        restartOnPlay = true
    )
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentHeight(), contentAlignment = Alignment.Center
    ) {
        Column(
            modifier = Modifier
                .wrapContentHeight()
                .padding(horizontal = 33.dp)
        ) {
            Spacer(modifier = Modifier.height(20.dp))
            Divider(
                modifier = Modifier
                    .fillMaxWidth(.3f)
                    .align(Alignment.CenterHorizontally)
            )
            Spacer(modifier = Modifier.height(20.dp))
            Text(
                text = if (isFingerCaptureFailed.value) "Capture Failed" else "Capture Success",
                color = if (isFingerCaptureFailed.value) Color.Red else Color.Green,
                modifier = Modifier.align(Alignment.CenterHorizontally),
                fontSize = 20.sp,
                fontFamily = FontFamily(Font(R.font.poppins_semibold))
            )
            Spacer(modifier = Modifier.height(23.dp))
            LottieAnimation(
                composition = composition.value,
                modifier = Modifier
                    .size(100.dp)
                    .align(Alignment.CenterHorizontally),
                progress = progress.value
            )
            Spacer(modifier = Modifier.height(23.dp))

            if (!isFingerCaptureFailed.value) {
                val scoreAnimate = animateFloatAsState(
                    targetValue = progressScore,
                    animationSpec =
                    tween(durationMillis = 2000)
                )
                val annotatedString =
                    AnnotatedString.Builder().apply {
                        // Add the ** symbol with red color
                        pushStyle(
                            SpanStyle(
                                color = Color.Black,
                                fontWeight = FontWeight.Bold
                            )
                        )
                        append("Fingerprint strength is ")
                        pop()
                        // Add the message text with black color
                        pushStyle(SpanStyle(color = Color.Green, fontSize = 18.sp))
                        append(scoreAnimate.value.roundToInt().toString() + "%")
                        pop()
                    }.toAnnotatedString()

                Text(
                    text = annotatedString,
                    color = Color.Green,
                    modifier = Modifier.align(Alignment.CenterHorizontally),
                    fontSize = 14.sp,
                    fontFamily = FontFamily(Font(R.font.poppins_semibold))
                )
                Spacer(modifier = Modifier.height(23.dp))
            }
            Row(modifier = Modifier.fillMaxWidth()) {
                Button(
                    onClick = {
                        fingerRecapture.invoke()
                    },
                    modifier = Modifier
                        .fillMaxWidth()
                        .weight(1f),
                    colors = ButtonDefaults.buttonColors(
                        backgroundColor = primaryColor
                    ),
                    shape = RoundedCornerShape(10.dp)
                ) {
                    Text(
                        text = "ReCapture",
                        fontFamily = FontFamily(Font(R.font.poppins_semibold)),
                        color = Color.White,
                        fontSize = 16.sp
                    )
                }
                if (!isFingerCaptureFailed.value) {
                    Spacer(modifier = Modifier.width(10.dp))
                    Button(
                        onClick = {
                            viewModel.showLoader.value = true
                            proceedOnClick.invoke()
                        },
                        modifier = Modifier
                            .fillMaxWidth()
                            .weight(1f),
                        colors = ButtonDefaults.buttonColors(
                            backgroundColor = primaryColor
                        ),
                        shape = RoundedCornerShape(10.dp)
                    ) {
                        if (!viewModel.showLoader.value) {
                            Text(
                                text = "Proceed",
                                fontFamily = FontFamily(Font(R.font.poppins_semibold)),
                                color = Color.White,
                                fontSize = 16.sp
                            )
                        } else {
                            CircularProgressIndicator(
                                modifier = Modifier.size(25.dp),
                                color = Color.White
                            )
                        }
                    }
                }
            }

            Spacer(modifier = Modifier.height(23.dp))
        }
    }
}