package com.aeps.aeps_ippb.presentation.navgraph

import androidx.compose.animation.*
import androidx.compose.animation.core.tween
import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import com.aeps.aeps_ippb.common.SdkConstants
import com.aeps.aeps_ippb.presentation.finger_capture_screen.FingerCaptureScreen
import com.aeps.aeps_ippb.presentation.ministatement_screen.MiniStatementScreen
import com.aeps.aeps_ippb.presentation.transaction_status.TransactionStatusScreen
import com.aeps.aeps_ippb.presentation.unified_bioauth.BioAuthFingerCaptureScreen
import com.aeps.aeps_ippb.presentation.viewmodel.UnifiedAepsViewModel
import com.google.accompanist.navigation.animation.AnimatedNavHost
import com.google.accompanist.navigation.animation.composable

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun SetUpNavGraph(
    navController: NavHostController,
    viewModel: UnifiedAepsViewModel,
) {
    AnimatedNavHost(
        navController = navController,
        startDestination = SdkConstants.START_DESTINATION
    ) {
        composable(
            route = Destinations.TRANSACTION_STATUS_ROUTE,
            enterTransition = {
                enterTransition()
            },
            exitTransition = {
                exitTransition()
            },
            popEnterTransition = {
                popEnterTransition()
            }
        ) {
            TransactionStatusScreen(
                navHostController = navController,
                viewModel)
        }
        composable(
            route = Destinations.MINISTATEMENT_ROUTE,
            enterTransition = {
                enterTransition()
            },
            exitTransition = {
                exitTransition()
            },
            popEnterTransition = {
                popEnterTransition()
            }
        ) {
            MiniStatementScreen(
                viewModel
            )
        }
        composable(route = Destinations.FINGERPRINT_CAPTURE_ROUTE,
            enterTransition = {
                enterTransition()
            },
            exitTransition = {
                exitTransition()
            },
            popEnterTransition = {
                popEnterTransition()
            }) {
            FingerCaptureScreen(
                navHostController = navController,
                viewModel = viewModel
            )
        }
        composable(route = Destinations.BIO_AUTH_SCREEN,
            enterTransition = {
                enterTransition()
            },
            exitTransition = {
                exitTransition()
            },
            popEnterTransition = {
                popEnterTransition()
            }) {
            BioAuthFingerCaptureScreen(
                navHostController = navController,
                viewModel = viewModel
            )
        }
    }
}

private fun popEnterTransition() =
    slideInHorizontally(
        initialOffsetX = { -300 },
        animationSpec = tween(300)
    ) + fadeIn(animationSpec = tween(300))


private fun exitTransition() =
    slideOutHorizontally(
        targetOffsetX = { -300 },
        animationSpec = tween(300)
    ) + fadeOut(animationSpec = tween(300))


private fun enterTransition() =
    slideInHorizontally(
        initialOffsetX = { 300 },
        animationSpec = tween(300)
    ) + fadeIn(animationSpec = tween(300))

