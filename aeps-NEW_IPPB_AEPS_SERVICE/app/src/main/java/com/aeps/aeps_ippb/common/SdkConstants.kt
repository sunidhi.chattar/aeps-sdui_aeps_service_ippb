package com.aeps.aeps_ippb.common

import android.Manifest
import android.bluetooth.BluetoothDevice
import com.aeps.aeps_ippb.presentation.navgraph.Destinations

class SdkConstants {
    companion object {
        var START_DESTINATION = Destinations.FINGERPRINT_CAPTURE_ROUTE
        var adminName = ""


        var RECEIVE_DRIVER_DATA: String = ""
        const val cashWithdrawal = "1"
        const val aadhaarPay = "3"
        var transactionAmount = "0"
        var transactionType = ""
        var paramA = ""
        var paramB = ""
        var paramC = ""
        var API_USER_NAME_VALUE = ""
        var applicationType = ""
        var userNameFromCoreApp = ""
        var tokenFromCoreApp: String? = ""
        var isSl = false
        var BRAND_NAME = ""
        var SHOP_NAME = ""
        var bankName = ""
        var bankIIN = ""
        var aadhaarNo = ""
        var mobileNo = ""
        var internalFPName = ""
        var Bluetoothname = ""
        var bluetoothConnector = false
        var agent = ""
        var location = ""
        var isMantraIris = false
        val permissions12 = arrayOf(
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_FINE_LOCATION,
            "android.permission.BLUETOOTH_SCAN",
            "android.permission.BLUETOOTH_CONNECT"
        )
        val permissionsList = arrayOf(
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_FINE_LOCATION,
            "android.permission.BLUETOOTH_SCAN",
            "android.permission.BLUETOOTH_CONNECT"
        )
        val permissionsList1 = arrayOf(
            Manifest.permission.READ_MEDIA_AUDIO,
            Manifest.permission.READ_MEDIA_IMAGES,
            Manifest.permission.READ_MEDIA_VIDEO,
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_FINE_LOCATION,
            "android.permission.BLUETOOTH_SCAN",
            "android.permission.BLUETOOTH_CONNECT"
        )
        var selected_btdevice: BluetoothDevice? = null
        var selected_fingerPrint: BluetoothDevice? = null


    }
}