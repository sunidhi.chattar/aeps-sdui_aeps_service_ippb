package com.aeps.aeps_ippb.common

interface Mapper<I, O> {
    fun mapToDto(domain: O): I
    fun mapToDomain(dto: I): O
}
