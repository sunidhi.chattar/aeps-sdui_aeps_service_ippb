package com.aeps.aeps_ippb.domain.usecase.transfer

import com.aeps.aeps_ippb.common.NetworkResource
import com.aeps.aeps_ippb.domain.model.request.TransferRequestDomain
import com.aeps.aeps_ippb.domain.model.response.TransferResponseDomain
import com.aeps.aeps_ippb.domain.repository.Repository
import kotlinx.coroutines.flow.Flow

class TransferUseCase(
    private val repository: Repository
) {
    fun invoke(
        url: String,
        token: String,
        body: TransferRequestDomain
    ): Flow<NetworkResource<TransferResponseDomain>> {
        return repository.transfer(url,token, body)
    }
}