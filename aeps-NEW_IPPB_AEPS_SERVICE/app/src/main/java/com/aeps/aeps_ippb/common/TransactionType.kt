package com.aeps.aeps_ippb.common

sealed class TransactionType(val name: String) {
    object CashWithdraw : TransactionType("1")
    object BalanceEnquiry : TransactionType("0")
    object MiniStatement : TransactionType("2")
    object AadhaarPay : TransactionType("3")
    object CashDeposit : TransactionType("4")

}
