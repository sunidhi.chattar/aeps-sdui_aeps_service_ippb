package com.aeps.aeps_ippb.data.remote.dto.response

import com.google.gson.annotations.SerializedName

data class TransferResponseDto(

	@field:SerializedName("transactionMode")
	val transactionMode: String? = null,

	@field:SerializedName("txId")
	val txId: String? = null,

	@field:SerializedName("bankName")
	val bankName: String? = null,

	@field:SerializedName("updatedDate")
	val updatedDate: String? = null,

	@field:SerializedName("iin")
	val iin: String? = null,

	@field:SerializedName("origin_identifier")
	val originIdentifier: String? = null,

	@field:SerializedName("createdDate")
	val createdDate: String? = null,

	@field:SerializedName("apiTid")
	val apiTid: String? = null,

	@field:SerializedName("balance")
	val balance: String? = null,

	@field:SerializedName("isRetriable")
	val isRetriable: Boolean? = null,

	@field:SerializedName("ministatement")
	val ministatement: List<MiniStatementDto?>? = null,

	@field:SerializedName("apiComment")
	val apiComment: String? = null,

	@field:SerializedName("gateway")
	val gateway: Int? = null,

	@field:SerializedName("errors")
	val errors: Any? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("authCode")
	val authCode: String? = null,

	@field:SerializedName("uidRefId")
	val uidRefId: String? = null,

	@field:SerializedName("depositId")
	val depositId: String? = null
){
	data class MiniStatementDto(

		@field:SerializedName("Date")
		val date: String? = null,

		@field:SerializedName("Amount")
		val amount: String? = null,

		@field:SerializedName("DebitCredit")
		val txntype: String? = null,

		@field:SerializedName("Type")
		val txnDesc: String? = null,
	)
}
