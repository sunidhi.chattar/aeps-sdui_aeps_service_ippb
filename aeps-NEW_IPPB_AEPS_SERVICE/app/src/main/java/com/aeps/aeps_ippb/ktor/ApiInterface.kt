package com.aeps.aeps_ippb.ktor

import io.ktor.client.HttpClient
import io.ktor.client.request.get

data class ApiInterface(val users: List<User>)
suspend fun getUsers(): List<User>{
    val client = HttpClient()
    return client.get("https://www.example.com")
}