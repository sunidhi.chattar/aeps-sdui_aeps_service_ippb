package com.aeps.aeps_ippb.presentation

class DataModel() {
    var transactionAmount: String? = null
    var transactionType: String? = null
    var paramA: String? = null
    var paramB: String? = null
    var paramC: String? = null
    var applicationType: String? = null
    var tokenFromCoreApp: String? = null
    var userNameFromCoreApp: String? = null
    var API_USER_NAME_VALUE: String? = null
    var DRIVER_ACTIVITY: String? = null
    var BRAND_NAME: String? = null
    var SHOP_NAME: String? = null
    var skipReceipt: Boolean? = null
    var internalFPName: String? = null
    var latlong:String?=null

}

