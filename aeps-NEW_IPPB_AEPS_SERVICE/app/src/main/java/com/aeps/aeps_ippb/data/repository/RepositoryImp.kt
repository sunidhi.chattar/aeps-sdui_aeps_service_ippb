package com.aeps.aeps_ippb.data.repository

import com.aeps.aeps_ippb.common.NetworkResource
import com.aeps.aeps_ippb.common.handleFlowResponse
import com.aeps.aeps_ippb.data.remote.ApiServices
import com.aeps.aeps_ippb.data.remote.dto.request.FetchUiDataReq
import com.aeps.aeps_ippb.data.remote.dto.response.ui_data.FetchUiData
import com.aeps.aeps_ippb.domain.model.request.SubmitBioAuthReqDomain
import com.aeps.aeps_ippb.domain.model.request.TransferRequestDomain
import com.aeps.aeps_ippb.domain.model.response.CheckBioAuthDomain
import com.aeps.aeps_ippb.domain.model.response.TransferResponseDomain
import com.aeps.aeps_ippb.domain.repository.Repository
import com.aeps.aeps_ippb.mapper.CheckBioAuthMapper
import com.aeps.aeps_ippb.mapper.FundTransferRequestMapper
import com.aeps.aeps_ippb.mapper.FundTransferResponseMapper
import com.aeps.aeps_ippb.mapper.SubmitBioAuthReqMapper
import kotlinx.coroutines.flow.Flow


class RepositoryImp constructor(
    private val apiService: ApiServices,
) : Repository {


    override fun transfer(
        endPoint: String, token: String, body: TransferRequestDomain
    ): Flow<NetworkResource<TransferResponseDomain>> {
        return handleFlowResponse(call = {
            apiService.transfer(
                endPoint,
                token,
                FundTransferRequestMapper().mapToDto(body)
            )
        },
            mapFun = { FundTransferResponseMapper().mapToDomain(it) })
    }

    override fun fetchUIData(adminName: String): Flow<NetworkResource<FetchUiData>> {
        val fetchUiDataReq = FetchUiDataReq(userName = adminName)
        return handleFlowResponse(
            call = {
                apiService.fetchUiData(fetchUiDataReq = fetchUiDataReq)
            },
            mapFun = {
                it
            }
        )
    }

    override fun submitBioAuth(
        url: String,
        submitBioAuthReqDomain: SubmitBioAuthReqDomain,
        token: String
    ): Flow<NetworkResource<CheckBioAuthDomain>> {
        return handleFlowResponse(
            call = {
                apiService.submitBioAuthStatus(
                    url,
                    SubmitBioAuthReqMapper().mapToDto(submitBioAuthReqDomain),
                    token
                )
            },
            mapFun = {
                CheckBioAuthMapper().mapToDomain(it)
            }
        )
    }

    override fun checkBioAuthStatus(
        url: String,
        token: String
    ): Flow<NetworkResource<CheckBioAuthDomain>> {
        return handleFlowResponse(
            call = {
                apiService.checkBioAuthStatus(url, token)
            },
            mapFun = {
                CheckBioAuthMapper().mapToDomain(it)
            }
        )
    }
}

