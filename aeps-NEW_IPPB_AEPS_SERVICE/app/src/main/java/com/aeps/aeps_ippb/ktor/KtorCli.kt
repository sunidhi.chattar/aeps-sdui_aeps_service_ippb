package com.aeps.aeps_ippb.ktor

import io.ktor.client.HttpClient
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.request.*
import io.ktor.client.features.json.serializer.*
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.json.Json

fun main() = runBlocking {
    try {
        val users = getUsers()
        users.forEach { user ->
            println("User ${user.id}: ${user.name}, Email: ${user.email}")
        }
    } catch (e: Exception) {
        println("Error occurred: ${e.message}")
    }
    finally {
        client.close()
    }
}