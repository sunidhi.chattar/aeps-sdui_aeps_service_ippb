package com.aeps.aeps_ippb

import android.app.Activity
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.aeps.aeps_ippb.common.GetPosConnectedPrinter
import com.aeps.aeps_ippb.common.SdkConstants
import com.aeps.aeps_ippb.presentation.navgraph.SetUpNavGraph
import com.aeps.aeps_ippb.common.GetLocation
import com.aeps.aeps_ippb.common.Util
import com.aeps.aeps_ippb.presentation.viewmodel.UnifiedAepsViewModel
import com.aeps.aeps_ippb.presentation.components.BluetoothDeviceDialog
import com.aeps.aeps_ippb.presentation.components.RootView
import com.aeps.aeps_ippb.ui.theme.primaryColor
import com.google.accompanist.navigation.animation.rememberAnimatedNavController
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.iserveu.inappdistribution.ISUInAppUpdateSDK
import com.isu.printer_library.vriddhi.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.json.JSONObject
import org.koin.androidx.compose.getViewModel
import java.io.IOException
import java.net.SocketException
import java.util.*


class MainActivity : ComponentActivity(), IAemCardScanner, IAemScrybe,
    GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private lateinit var dataObject: String
    private lateinit var rootNavController: NavHostController
    private lateinit var viewModel: UnifiedAepsViewModel
    private var `object`: JSONObject = JSONObject()
    private var printerList: ArrayList<String>? = null
    private var mAemscrybedevice: AEMScrybeDevice? = null
    private var mAemprinter: AEMPrinter? = null
    private var mCardreader: CardReader? = null
    private var creditData: String? = null
    private var tempdata: String? = null
    private var replacedData: String? = null
    private var responseString: String? = null
    private var response: String? = null
    private var cardTrackType: CardReader.CARD_TRACK? = null
    private var responseArray = arrayOfNulls<String>(1)
    private var mylocation: Location? = null
    private var errorMessage = ""

    @OptIn(ExperimentalAnimationApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ISUInAppUpdateSDK.forceUpdateActivity = this.localClassName
        setContent {
            RootView {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    val showAlert = remember {
                        mutableStateOf(false)
                    }
                    val context = LocalContext.current
                    viewModel = getViewModel()
                    bluetoothBroadcastReceiver()
                    try {
                        if (intent.hasExtra("dataToService")) {
                            dataObject = intent.getStringExtra("dataToService").toString()
                            `object` = JSONObject(dataObject)
                            SdkConstants.START_DESTINATION =
                                `object`.getString("startDestination")
                            SdkConstants.adminName =
                                `object`.getString("adminName")
                            SdkConstants.transactionType = `object`.getString("transactionType")
                            SdkConstants.tokenFromCoreApp =
                                `object`.getString("tokenFromCoreApp")
                            SdkConstants.applicationType =
                                `object`.getString("applicationType")
                            SdkConstants.userNameFromCoreApp =
                                `object`.getString("userNameFromCoreApp")
                            SdkConstants.transactionAmount = `object`.getString("transactionAmount")
                            viewModel.amount.value = SdkConstants.transactionAmount
                            if (SdkConstants.transactionType == "1" || SdkConstants.transactionType == "3" || SdkConstants.transactionType == "4") {
                                try {
                                    val amount = SdkConstants.transactionAmount.toInt()
                                    if (amount == 0) {
                                        showAlert.value = true
                                        errorMessage = "Amount can't be 0"
                                    }
                                } catch (e: Exception) {
                                    showAlert.value = true
                                    errorMessage = "Invalid Transaction Amount"
                                }
                            }
                            SdkConstants.paramA = `object`.getString("paramA")
                            SdkConstants.paramB = `object`.getString("paramB")
                            SdkConstants.paramC = `object`.getString("paramC")
                            SdkConstants.API_USER_NAME_VALUE =
                                `object`.getString("API_USER_NAME_VALUE")
                            SdkConstants.aadhaarNo = `object`.getString("aadhaarNo")
                            SdkConstants.bankName = `object`.getString("bankName")
                            SdkConstants.bankIIN = `object`.getString("bankIIN")
                            SdkConstants.mobileNo = `object`.getString("mobileNo")
                            if (`object`.has("SHOP_NAME")) {
                                SdkConstants.SHOP_NAME =
                                    `object`.getString("SHOP_NAME").toString()
                            }
                            if (`object`.has("BRAND_NAME")) {
                                SdkConstants.BRAND_NAME =
                                    `object`.getString("BRAND_NAME").toString()
                            }
                            if (`object`.has("internalFPName")) {
                                SdkConstants.internalFPName = `object`.getString("internalFPName")
                            }
                            if (`object`.has("location")) {
                                SdkConstants.location = `object`.getString("location")
                            }
                            if (`object`.has("agent")) {
                                SdkConstants.agent = `object`.getString("agent")
                                SdkConstants.agent =
                                    if (SdkConstants.agent != "" || SdkConstants.agent.isNotEmpty() || SdkConstants.agent != "null") {
                                        SdkConstants.agent
                                    } else {
                                        "N/A"
                                    }
                            }
                            if (dataEmpty()) {
                                showAlert.value = true
                            }
                        } else {
                            showAlert.value = true
                        }
                    } catch (e: Exception) {
                        showAlert.value = true
                        errorMessage = e.message ?: ""
                    }
                    if (showAlert.value) {
                        AlertDialogComponent(showAlert, errorMessage)
                    } else {
                        GetLocation(context, viewModel)
                        rootNavController = rememberAnimatedNavController()
                        SetUpNavGraph(navController = rootNavController, viewModel)
                    }
                }
            }
        }
    }

    private fun bluetoothBroadcastReceiver() {
        val filter = IntentFilter()
        val broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
            var device: BluetoothDevice? = null
            override fun onReceive(context: Context, intent: Intent) {
                val action = intent.action
                device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE)
                if (BluetoothDevice.ACTION_ACL_CONNECTED == action) {
                } else if (BluetoothDevice.ACTION_ACL_DISCONNECTED == action) {
                    GetPosConnectedPrinter.aemPrinter = null
                    Toast.makeText(applicationContext, "Device is disconnected", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED)
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED)
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED)
        this.registerReceiver(broadcastReceiver, filter)
    }

    private fun dataEmpty(): Boolean {
        if (SdkConstants.START_DESTINATION.isEmpty()) {
            errorMessage = "Start destination can't be empty"
            return true
        } else if (SdkConstants.adminName.isEmpty()) {
            errorMessage = "Admin name can't be empty"
            return true
        } else if (SdkConstants.transactionType.isEmpty()) {
            errorMessage = "Transaction type can't be empty"
            return true
        } else if (SdkConstants.userNameFromCoreApp.isEmpty()) {
            errorMessage = "User name can't be empty"
            return true
        } else if (SdkConstants.tokenFromCoreApp.toString().isEmpty()) {
            errorMessage = "Token can't be empty"
            return true
        } else if (SdkConstants.aadhaarNo.isEmpty()) {
            errorMessage = "Aadhaar no can't be empty"
            return true
        } else if (!validateAadhaarOrVid()) {
            errorMessage = "Invalid Aadhaar No"
            return true
        } else if (SdkConstants.bankName.isEmpty()) {
            errorMessage = "Bank name can't be empty"
            return true
        } else if (SdkConstants.bankIIN.isEmpty()) {
            errorMessage = "Bank IIN can't be empty"
            return true
        } else if (SdkConstants.mobileNo.isEmpty()) {
            errorMessage = "Mobile No can't be empty"
            return true
        } else if (!Util.isValidMobile(SdkConstants.mobileNo)) {
            errorMessage = "Invalid mobile no"
            return true
        } else if (SdkConstants.transactionType == "1" || SdkConstants.transactionType == "3" || SdkConstants.transactionType == "4") {
            try {
                val amount = SdkConstants.transactionAmount.toInt()
                if (amount == 0) {
                    errorMessage = "Amount can't be 0"
                    return true
                }
            } catch (e: Exception) {
                errorMessage = "Invalid Transaction Amount"
                return true
            }
        }
        return false
    }
    private fun validateAadhaarOrVid(): Boolean {
        val length = SdkConstants.aadhaarNo.length
        if (length == 12) {
            return Util.validateAadharNumber(SdkConstants.aadhaarNo)
        } else if (length == 16) {
            return Util.validateAadharVID(SdkConstants.aadhaarNo)
        }
        return false
    }


    @Composable
    fun AlertDialogComponent(openDialog: MutableState<Boolean>, errorMessage: String) {
        val context = LocalContext.current
        if (openDialog.value) {
            AlertDialog(
                modifier = Modifier.border(
                    width = 2.dp,
                    shape = RoundedCornerShape(20.dp),
                    color = primaryColor
                ),
                onDismissRequest = { openDialog.value = false },
                title = {
                    Text(
                        text = "Alert", color = Color.Black, fontWeight = FontWeight.Bold,
                        fontFamily = FontFamily(Font(R.font.poppins_semibold))
                    )
                },
                text = {
                    if (errorMessage.isNotEmpty()) {
                        Text(
                            errorMessage, color = Color.Black,
                            fontFamily = FontFamily(Font(R.font.poppins_regular))
                        )
                    } else {
                        Text(
                            "Require Data Missing", color = Color.Black,
                            fontFamily = FontFamily(Font(R.font.poppins_regular))
                        )
                    }
                },
                confirmButton = {
                    TextButton(
                        modifier = Modifier.padding(horizontal = 10.dp, vertical = 5.dp),
                        colors = ButtonDefaults.textButtonColors(
                            backgroundColor = primaryColor
                        ),
                        elevation = ButtonDefaults.elevation(10.dp),
                        onClick = {
                            openDialog.value = false
                            (context as Activity).finish()
                        }
                    ) {
                        Text(
                            "Ok",
                            color = Color.White,
                            fontFamily = FontFamily(Font(R.font.poppins_regular))
                        )
                    }
                },
                backgroundColor = Color.White,
                shape = RoundedCornerShape(20.dp),
                contentColor = Color.White
            )
        }
    }

    @Composable
    fun ShowBluetoothDevices(
        context: Context,
        viewModel: UnifiedAepsViewModel,
        printClicked: MutableState<Boolean>,
        onClick: () -> Unit
    ) {
        val scope = rememberCoroutineScope()
        printerList = ArrayList()
        mAemscrybedevice = AEMScrybeDevice(this)
        if (GetPosConnectedPrinter.aemPrinter == null) {
            printerList = mAemscrybedevice!!.pairedPrinters
            printerList?.let { list ->
                if (list.size > 0) {
                    BluetoothDeviceDialog(
                        printerName = list,
                        viewModel = viewModel
                    ) {
                        scope.launch(Dispatchers.Main) {
                            try {
                                mAemscrybedevice!!.connectToPrinter(it)
                                mCardreader =
                                    mAemscrybedevice!!.getCardReader(this@MainActivity)
                                mAemprinter = mAemscrybedevice!!.aemPrinter
                                GetPosConnectedPrinter.aemPrinter = mAemprinter
                                Toast.makeText(
                                    context,
                                    "Connected with $it",
                                    Toast.LENGTH_SHORT
                                ).show()
                                viewModel.isStartPrinting.value =
                                    !viewModel.isStartPrinting.value
                                printClicked.value = !printClicked.value

                            } catch (e: IOException) {
                                viewModel.isStartPrinting.value =
                                    !viewModel.isStartPrinting.value
                                if (e.message!!.contains("Service discovery failed")) {
                                    Toast.makeText(
                                        context,
                                        "Not Connected\n$it is unreachable or off otherwise it is connected with other device",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                } else if (e.message!!.contains("Device or resource busy")) {
                                    Toast.makeText(
                                        context,
                                        "the device is already connected",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                } else {
                                    Toast.makeText(
                                        context,
                                        "Unable to connect",
                                        Toast.LENGTH_SHORT
                                    ).show()

                                }
                                printClicked.value = !printClicked.value
                            } catch (e: Exception) {
                                viewModel.isStartPrinting.value =
                                    !viewModel.isStartPrinting.value
                                Toast.makeText(
                                    context,
                                    "Unable to connect",
                                    Toast.LENGTH_SHORT
                                ).show()
                                printClicked.value = !printClicked.value
                            } catch (e: SocketException) {
                                viewModel.isStartPrinting.value =
                                    !viewModel.isStartPrinting.value
                                Toast.makeText(
                                    context,
                                    "Unable to connect",
                                    Toast.LENGTH_SHORT
                                ).show()
                                printClicked.value = !printClicked.value
                            }
                        }
                    }
                } else {
                    viewModel.showPrinterDialogue.value = true
                }
            }
        } else {
            onClick.invoke()
        }

    }

    override fun onResume() {
        super.onResume()
        try {
            viewModel.isLoading.value = false
            if (SdkConstants.RECEIVE_DRIVER_DATA == "") {
                viewModel.scoreStr.value = "0"
                viewModel.isFingerCaptureFailed.value = true

            } else {
                viewModel.isFingerCaptureFailed.value = false
                val respObj = JSONObject(SdkConstants.RECEIVE_DRIVER_DATA)
                var scoreString = respObj.getString("pidata_qscore")
                var pidData = respObj.getString("base64pidData")
                pidData = if (SdkConstants.internalFPName.equals("wiseasy", ignoreCase = true)) {
                    pidData.replace("\\n+".toRegex(), "").trim()
                } else {
                    pidData.replace("\\R+".toRegex(), "").trim()
                }
                val scoreList: Array<String> = scoreString.split(",").toTypedArray()
                scoreString = scoreList[0]
                viewModel.pidData.value = pidData
                viewModel.scoreStr.value = scoreString
                if (respObj.has("srno")) {
                    viewModel.terminalId.value = respObj.getString("srno")
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 201) {
            if (!SdkConstants.bluetoothConnector) {
                viewModel.showDeviceTypeDialog.value = true
                viewModel.showDeviceTypeDialogBio.value = true
            }
        }
    }


    override fun onScanMSR(buffer: String?, cardtrack: CardReader.CARD_TRACK?) {
        cardTrackType = cardtrack
        creditData = buffer
        runOnUiThread {
        }
    }

    override fun onScanDLCard(buffer: String?) {
        val dlCardData: CardReader.DLCardData = mCardreader!!.decodeDLData(buffer)
        val name = """
            NAME:${dlCardData.NAME}
            
            """.trimIndent()
        val SWD = """
            SWD Of: ${dlCardData.SWD_OF}
            
            """.trimIndent()
        val dob = """
            DOB: ${dlCardData.DOB}
            
            """.trimIndent()
        val dlNum = """
            DLNUM: ${dlCardData.DL_NUM}
            
            """.trimIndent()
        val issAuth = """
            ISS AUTH: ${dlCardData.ISS_AUTH}
            
            """.trimIndent()
        val doi = """
            DOI: ${dlCardData.DOI}
            
            """.trimIndent()
        val tp = """
            VALID TP: ${dlCardData.VALID_TP}
            
            """.trimIndent()
        val ntp = """
            VALID NTP: ${dlCardData.VALID_NTP}
            
            """.trimIndent()
    }

    override fun onScanRCCard(buffer: String?) {
        val rcCardData: CardReader.RCCardData = mCardreader!!.decodeRCData(buffer)
        val regNum = """
            REG NUM: ${rcCardData.REG_NUM}
            
            """.trimIndent()
        val regName = """
            REG NAME: ${rcCardData.REG_NAME}
            
            """.trimIndent()
        val regUpto = """
            REG UPTO: ${rcCardData.REG_UPTO}
            
            """.trimIndent()

        val data = regNum + regName + regUpto

        runOnUiThread {
            //                editText.setText(data);
        }
    }

    override fun onScanRFD(buffer: String?) {
        val stringBuffer = StringBuffer()
        stringBuffer.append(buffer)
        var temp = ""
        try {
            temp = stringBuffer.deleteCharAt(8).toString()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        val data = temp

        runOnUiThread { //rfText.setText("RF ID:   " + data);
            //                editText.setText("ID " + data);
            try {
                mAemprinter!!.print(data)
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    override fun onScanPacket(buffer: String?) {
        if (buffer == "PRINTEROK") {
            val stringBuffer = StringBuffer()
            stringBuffer.append(buffer)
            var temp = ""
            try {
                temp = stringBuffer.toString()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            tempdata = temp
            val strData: String = tempdata!!.replace("|", "&")
            //Log.e("BufferData",data);
            val formattedData = arrayOf(strData.split("&".toRegex(), 3).toTypedArray())
            // Log.e("Response Data",formattedData[2]);
            responseString = formattedData[0][2]
            responseArray[0] = responseString!!.replace("^", "")
            Log.e("Response Array", responseArray[0]!!)
            runOnUiThread {
                replacedData = tempdata!!.replace("|", "&")
                formattedData[0] = replacedData!!.split("&".toRegex(), 3).toTypedArray()
                response = formattedData[0][2]

            }
        } else {
            val stringBuffer = StringBuffer()
            stringBuffer.append(buffer)
            var temp = ""
            try {
                temp = stringBuffer.toString()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            tempdata = temp
            val strData: String = tempdata!!.replace("|", "&")
            //Log.e("BufferData",data);
            val formattedData = arrayOf(strData.split("&".toRegex(), 3).toTypedArray())
            // Log.e("Response Data",formattedData[2]);
            responseString = formattedData[0][2]
            responseArray[0] = responseString!!.replace("^", "")
            Log.e("Response Array", responseArray[0]!!)
            runOnUiThread {
                replacedData = tempdata!!.replace("|", "&")
                formattedData[0] = replacedData!!.split("&".toRegex(), 3).toTypedArray()
                response = formattedData[0][2]

            }
        }
    }

    override fun onDiscoveryComplete(aemPrinterList: ArrayList<String>?) {
        printerList = aemPrinterList
        for (i in aemPrinterList!!.indices) {
            val deviceName = aemPrinterList[i]
            val status = mAemscrybedevice!!.pairPrinter(deviceName)
            Log.e("STATUS", status)
        }
    }

    override fun onConnected(p0: Bundle?) {}

    override fun onConnectionSuspended(p0: Int) {}

    override fun onConnectionFailed(p0: ConnectionResult) {}

    override fun onLocationChanged(location: Location) {
        mylocation = location
        if (mylocation != null) {
            val addresses: List<Address>?
            val geocoder = Geocoder(this, Locale.getDefault())
            val latitude = location.latitude
            val longitude = location.longitude
            Log.e("latitude", "latitude--$latitude")
            try {
                Log.e("latitude", "inside latitude--$latitude")
                addresses = geocoder.getFromLocation(latitude, longitude, 1)
                if (!addresses.isNullOrEmpty()) {
                    val address = addresses[0].getAddressLine(0)
                    val city = addresses[0].locality
                    val state = addresses[0].adminArea
                    val country = addresses[0].countryName
                    viewModel.postalCode.value = addresses[0].postalCode
                    if (viewModel.postalCode.value == null) {
                        viewModel.postalCode.value = "751012"
                    }
                    getPin()
                }
            } catch (e: IOException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }
        }
    }

    private fun getPin() {
        if (mylocation != null) {
            // updateLocationInterface.onLocationUpdate(location);
            val addresses: List<Address>?
            val geocoder: Geocoder = Geocoder(this, Locale.getDefault())
            val latitude = mylocation!!.latitude
            val longitude = mylocation!!.longitude
            Log.e("latitude", "latitude--$latitude")
            try {
                Log.e("latitude", "inside latitude--$latitude")
                addresses = geocoder.getFromLocation(latitude, longitude, 1)
                if (!addresses.isNullOrEmpty()) {
                    viewModel.postalCode.value = addresses[0].postalCode
                    if (viewModel.postalCode.value == null) {
                        viewModel.postalCode.value = "751017"
                    }
                }
            } catch (e: IOException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }
        }
    }

}
