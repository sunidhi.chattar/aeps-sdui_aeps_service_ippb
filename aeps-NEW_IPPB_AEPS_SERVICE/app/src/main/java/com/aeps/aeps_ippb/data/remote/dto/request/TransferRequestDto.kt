package com.aeps.aeps_ippb.data.remote.dto.request

import com.google.gson.annotations.SerializedName

data class TransferRequestDto(
    @field:SerializedName("amount")
    val amount: Int? = null,

    @field:SerializedName("latLong")
    val latLong: String? = null,

    @field:SerializedName("mobileNumber")
    val mobileNumber: String? = null,

    @field:SerializedName("bankName")
    val bankName: String? = null,

    @field:SerializedName("aadharNo")
    val aadharNo: String? = null,

    @field:SerializedName("pidData")
    val pidData: String? = null,

    @field:SerializedName("apiUser")
    val apiUser: String? = null,

    @field:SerializedName("iin")
    val iin: String? = null,
    @field:SerializedName("ipAddress")
    val ipAddress: String? = null,
    @field:SerializedName("shakey")
    val shakey: String? = null,
    @field:SerializedName("paramA")
    var paramA: String? = null,
    @field:SerializedName("paramB")
    var paramB: String? = null,
    @field:SerializedName("paramC")
    var paramC: String? = null,
    @field:SerializedName("retailer")
    val retailer: String? = null,
    @field:SerializedName("gatewayPriority")
    val gatewayPriority: Int? = null,
    @field:SerializedName("apiUserName")
    val apiUserName: String? = null
)
