package com.aeps.aeps_ippb.data.remote.dto.request

import com.google.gson.annotations.SerializedName

data class FetchUiDataReq(

	@field:SerializedName("user_name")
	val userName: String? = "demoisu",
)
