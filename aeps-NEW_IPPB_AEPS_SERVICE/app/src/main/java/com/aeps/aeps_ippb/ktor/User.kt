package com.aeps.aeps_ippb.ktor

import io.ktor.client.HttpClient
import io.ktor.client.engine.okhttp.OkHttp
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.serializer.KotlinxSerializer
import kotlinx.serialization.Serializable
import kotlin.jvm.internal.Intrinsics.Kotlin

@Serializable
data class User(val id: Int, val name:String, val email: String)
val client = HttpClient(OkHttp){
    install(JsonFeature){
        serializer = KotlinxSerializer(kotlinx.serialization.json.Json {
            prettyPrint = true
        isLenient = true
        ignoreUnknownKeys = true})
    }
}