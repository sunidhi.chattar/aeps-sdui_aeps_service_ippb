package com.aeps.aeps_ippb.di


import com.aeps.aeps_ippb.data.remote.ApiServices
import com.aeps.aeps_ippb.data.repository.RepositoryImp
import com.aeps.aeps_ippb.domain.repository.Repository
import com.aeps.aeps_ippb.domain.usecase.UseCases
import com.aeps.aeps_ippb.domain.usecase.check_bioauth_usecase.CheckBioAuthUseCase
import com.aeps.aeps_ippb.domain.usecase.fetchui_usecase.FetchUiUseCase
import com.aeps.aeps_ippb.domain.usecase.submit_bioauth_usecase.SubmitBioAuthUseCase
import com.aeps.aeps_ippb.domain.usecase.transfer.TransferUseCase
import com.aeps.aeps_ippb.presentation.viewmodel.UnifiedAepsViewModel
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit

val aepsModule = module {
    //retrofit
    single {
        val loggingInterceptor =
            HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
        val client: OkHttpClient = OkHttpClient.Builder().connectTimeout(1, TimeUnit.MINUTES)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .retryOnConnectionFailure(true)
            .addInterceptor(loggingInterceptor)
            .retryOnConnectionFailure(true)
            .build()
        Retrofit.Builder()
            .baseUrl("https://unifiedaepsbeta.iserveu.tech/") // random url
            .addConverterFactory(GsonConverterFactory.create())
            .addConverterFactory(ScalarsConverterFactory.create())
            .client(client)
            .build()
            .create(ApiServices::class.java)
    }
    //repository
    single<Repository> { RepositoryImp(get()) }

    //useCase
    single { TransferUseCase(get()) }
    single { FetchUiUseCase(get()) }
    single { SubmitBioAuthUseCase(get()) }
    single { CheckBioAuthUseCase(get()) }
    single {
        UseCases(
            get(),
            get(),
            get(),
            get()
        )
    }

    //viewModel
    viewModel { UnifiedAepsViewModel(get()) }
}